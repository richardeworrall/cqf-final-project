﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Simulation
{
    public interface IParallelizationStrategy
    {
        IEnumerable<uint> ItersPerCore(uint nTotalIters);
    }

    public static class ThreadingStrategy
    {
        public static IParallelizationStrategy SingleThreaded =>
            new SingleThreadedStrategy();

        public static IParallelizationStrategy MultiThreaded =>
            new MultithreadedStrategy();

        private static void ReportThreadsInUse(uint nThreads)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Splitting across {nThreads} threads");
            Console.ResetColor();
        }

        private class SingleThreadedStrategy : IParallelizationStrategy
        {
            public IEnumerable<uint> ItersPerCore(uint nTotalIters)
            {
                ReportThreadsInUse(1);

                return new[] { nTotalIters };
            }
        }

        private class MultithreadedStrategy : IParallelizationStrategy
        {
            public IEnumerable<uint> ItersPerCore(uint nTotalIters)
            {
                var nCores = (uint) Environment.ProcessorCount;

                uint threadsToUse;
                if (nCores < 4) threadsToUse = nCores;
                else threadsToUse = nCores - 1;

                ReportThreadsInUse(threadsToUse);

                return DivideEvenly(nTotalIters, threadsToUse).ToArray();
            }

            private static IEnumerable<uint> DivideEvenly(uint numerator, uint denominator)
            {
                var div = Math.DivRem(numerator, denominator, out var rem);

                for (var i = 0; i < denominator; i++)
                {
                    yield return i < rem ? (uint)div + 1u : (uint)div;
                }
            }
        }
    }
}