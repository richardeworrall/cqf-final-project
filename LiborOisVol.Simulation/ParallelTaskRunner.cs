﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace LiborOisVol.Simulation
{
    public delegate Action SimulationTaskFactory(uint offset);

    public class ParallelTaskRunner
    {
        private readonly HashSet<WaitCallback> _allTasks;
        private readonly HashSet<ManualResetEvent> _resetEvents;

        public ParallelTaskRunner(
            SimulationTaskFactory taskFactory, 
            uint totalIters, IParallelizationStrategy strategy,
            IProgessReporter progressReporter = null)
        {
            _allTasks = new HashSet<WaitCallback>();

            _resetEvents = new HashSet<ManualResetEvent>();

            var itersPerCore = strategy.ItersPerCore(totalIters);
            
            progressReporter?.Reset(totalIters);

            uint offsetCounter = 0;

            foreach (var iterCount in itersPerCore)
            {
                var task = taskFactory(offsetCounter);

                offsetCounter += iterCount;

                var iterCountCopy = iterCount;
                var doneEvt = new ManualResetEvent(false);
                
                _allTasks.Add(threadContext =>
                {
                    var totalReported = 0u;

                    for (var q = 0; q < iterCountCopy; q++)
                    {
                        task();

                        if (q - totalReported > 100)
                        {
                            progressReporter?.IncrementBy(100u);
                            totalReported += 100u;
                        }
                    }

                    progressReporter?.IncrementBy(iterCountCopy - totalReported);

                    doneEvt.Set();
                });

                _resetEvents.Add(doneEvt);
            }
        }

        public void RunAndAwaitAll()
        {
            foreach (var task in _allTasks) ThreadPool.QueueUserWorkItem(task);

            foreach (var e in _resetEvents) e.WaitOne();
        }
    }
}