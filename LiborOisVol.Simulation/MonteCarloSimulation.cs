﻿using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Core;
using LiborOisVol.Model;
using LiborOisVol.Model.Instruments;

namespace LiborOisVol.Simulation
{
    public class MonteCarloSimulation
    {
        private readonly uint _nIters;

        private readonly MarketModelFactory _modelFactory;
        private readonly IContract[] _contracts;
        private readonly IProgessReporter _reporter;
        private readonly IParallelizationStrategy _strategy;

        public MonteCarloSimulation(uint nIters, 
            MarketModelFactory modelFactory,
            IContract[] contracts,
            IParallelizationStrategy strategy = null,
            IProgessReporter reporter = null)
        {
            _nIters = nIters;
            _modelFactory = modelFactory;
            _contracts = contracts;
            _reporter = reporter;
            _strategy = strategy ?? ThreadingStrategy.SingleThreaded;
        }

        public MeanVarianceTracker[] Run()
        {
            var allResults = new List<MeanVarianceTracker[]>();

            SimulationTaskFactory simulationTaskFactory = offset =>
            {
                var model = _modelFactory(offset);

                var thisThreadResults = _contracts
                                            .Select(x => new MeanVarianceTracker(x.Description))
                                            .ToArray();

                allResults.Add(thisThreadResults);

                return () =>
                {
                    model.Simulate();

                    for (var i = 0; i < _contracts.Length; i++)
                    {
                        var npv = model.NPV(_contracts[i]);

                        thisThreadResults[i].Add(npv);
                    }
                };
            };

            var runner = new ParallelTaskRunner(
                simulationTaskFactory, _nIters, _strategy, _reporter);

            runner.RunAndAwaitAll();

            var combinedResults = allResults.Aggregate(
                _contracts.Select(x => MeanVarianceTracker.Empty).ToArray(),
                (acc, a) => acc.Zip(a, (x, y) => MeanVarianceTracker.Combine(x, y)).ToArray());

            return combinedResults;
        }
    }
}