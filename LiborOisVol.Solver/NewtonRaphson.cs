﻿using System;
using System.Linq.Expressions;

namespace LiborOisVol.Solver
{
    public static class NewtonRaphson
    {
        private const double H = 1E-13;
        private const double TwoH = 2.0 * H;

        public static double Solve(Func<double, double> f, double initialGuess, 
            double tolerance = 1E-10, double maxIters = 1000)
        {
            var x = initialGuess;

            Func<double, double> dfdx = u => (f(u + H) - f(u - H)) / TwoH;

            for (var i = 0; i < maxIters; i++)
            {
                var fX = f(x);
                if (Math.Abs(fX) < tolerance) return x;
                x = x - fX / dfdx(x);
            }

            return x;
        }
    }
}