﻿using System;

namespace LiborOisVol.Solver
{
    namespace LiborOisVol.Solver
    {
        public static class IntervalBisection
        {
            public static double Solve(Func<double, double> f, double lowerBound, double upperBound, 
                double tolerance = 1E-10, double maxIters = 1000)
            {
                var x = 0.0;

                for (var i = 0; i < maxIters; i++)
                {
                    x = 0.5 * (upperBound + lowerBound);
                    var fX = f(x);
                    if (Math.Abs(fX) < tolerance) return x;
                    if (fX < 0) lowerBound = x;
                    else upperBound = x;
                }

                return x;
            }
        }
    }
}