﻿using System.Text;
using LiborOisVol.Core;

namespace LiborOisVol.Utils
{
    public static class HumanReadableHelper
    {
        public static string PrettyPrint(this double[] A)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < A.Length; i++)
            {
                sb.Append(A[i]).Append("\t");
            }

            return sb.ToString();
        }

        public static string PrettyPrint(this double[,] A)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < A.Width(); i++)
            {
                for (var j = 0; j < A.Height(); j++)
                {
                    sb.Append(A[j, i]).Append("\t");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
