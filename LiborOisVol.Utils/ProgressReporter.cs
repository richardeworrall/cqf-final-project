﻿using System;

namespace LiborOisVol.Simulation
{
    public interface IProgessReporter
    {
        void Reset(uint nTotal);
        void IncrementBy(uint nFinished);
    }
    
    public class ConsoleProgressReporter : IProgessReporter
    {
        private uint _nTotal;
        private uint _n;

        private readonly object _lockObj = new object();

        public void Reset(uint nTotal)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Preparing to run {nTotal} simulations");
            Console.ResetColor();

            _nTotal = nTotal;
        }

        public void IncrementBy(uint nFinished)
        {
            lock (_lockObj)
            {
                var previousPercent = (int)(_n / (float) _nTotal * 100);

                _n += nFinished;

                var currentPercent = (int)(_n / (float) _nTotal * 100);

                if (_n == _nTotal - 1 || _n == _nTotal)
                {
                    ReportFinished();
                }

                if (previousPercent != currentPercent)
                {
                    Report(currentPercent);
                }
            }
        }

        private static void Report(int percent)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\rWorking: {0}%  ", percent);
            Console.ResetColor();
        }

        private static void ReportFinished()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("\rWorking: {0}%  ", 100);
            Console.ResetColor();
        }
    }
}