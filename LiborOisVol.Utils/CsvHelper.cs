﻿using System;
using System.IO;
using LiborOisVol.Core;

namespace LiborOisVol.Utils
{
    public class CsvHelper
    {
        public void WriteToCsv(double[,] result, string path)
        {
            string uuid = Guid.NewGuid().ToString().Substring(0, 8);
            using (var writer = new StreamWriter(path + $"LMM-{uuid}.csv"))
            {
                for (var i = 0; i < result.Height(); i++)
                {
                    for (var j = 0; j < result.Width(); j++)
                    {
                        writer.Write($"{result[i, j]},");
                    }
                    writer.Write(writer.NewLine);
                }
            }
        }

    }
}