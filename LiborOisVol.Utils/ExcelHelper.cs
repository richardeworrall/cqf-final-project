﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using LiborOisVol.Core;
using Microsoft.Office.Interop.Excel;

namespace LiborOisVol.Utils
{
    // From https://web.archive.org/web/20100420164139/http://blogs.officezealot.com/whitechapel/archive/2005/04/10/4514.aspx

    public static class ExcelHelper
    {
        [DllImport("Oleacc.dll")]
        private static extern int AccessibleObjectFromWindow(int hwnd, uint dwObjectID, byte[] riid, ref Window ptr);

        public delegate bool EnumChildCallback(int hwnd, ref int lParam);

        [DllImport("User32.dll")]
        public static extern bool EnumChildWindows(int hWndParent, EnumChildCallback lpEnumFunc, ref int lParam);

        [DllImport("User32.dll")]
        public static extern int GetClassName(int hWnd, StringBuilder lpClassName, int nMaxCount);

        public static bool EnumChildProc(int hwndChild, ref int lParam)
        {
            var buf = new StringBuilder(128);
            GetClassName(hwndChild, buf, 128);
            if (buf.ToString() == "EXCEL7")
            {
                lParam = hwndChild;
                return false;
            }
            return true;
        }

        public static Worksheet GetFreshWorksheet(string fileName)
        {
            return GetFreshWorksheet(GetWorkbook(fileName));
        }

        public static Worksheet GetFreshWorksheet(Workbook wb)
        {
            var ws = (Worksheet)wb.Worksheets.Add();
            ws.Name = Guid.NewGuid().ToString().ToUpperInvariant().Substring(0, 8);
            return ws;
        }

        public static Workbook GetWorkbook(string fileName)
        {
            Application app = null;
            Workbook wb = null;

            var excelHwnds = Process.GetProcessesByName("EXCEL").Select(x => (int)x.MainWindowHandle);

            foreach (var hwnd in excelHwnds)
            {
                Application xl = null;

                if (hwnd != 0)
                {
                    var hwndChild = 0;
                    var cb = new EnumChildCallback(EnumChildProc);
                    EnumChildWindows(hwnd, cb, ref hwndChild);

                    if (hwndChild != 0)
                    {
                        Window ptr = null;

                        var hr = AccessibleObjectFromWindow(hwndChild, 0xFFFFFFF0, 
                            new Guid("{00020400-0000-0000-C000-000000000046}").ToByteArray(), ref ptr);

                        if (hr >= 0) xl = ptr.Application;
                    }

                    if (xl != null)
                    {
                        try
                        {
                            wb = xl.Workbooks[fileName];
                            app = xl;
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                }
            }

            if (app == null)
            {
                app = new Application();
                app.Visible = true;
                app.DisplayAlerts = true;

                try
                {
                    wb = app.Workbooks.Open(fileName);
                }
                catch (Exception e)
                {
                    Console.Write(e);
                    wb = app.Workbooks.Add(Missing.Value);
                    wb.SaveAs(fileName);
                }
            }

            return wb;
        }

        public static void Create3DSurfacePlot(Range range)
        {
            var ws = range.Worksheet;

            var chartObjs = (ChartObjects)ws.ChartObjects(Type.Missing);
            var chartObj = chartObjs.Add(100, 20, 800, 450);
            var xlChart = chartObj.Chart;

            xlChart.SetSourceData(range, Type.Missing);
            xlChart.ChartType = XlChartType.xlSurface;

            xlChart.Rotation = 30;
            xlChart.Elevation = 20;

            var xAxis = (Axis)xlChart.Axes(XlAxisType.xlCategory);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "X Axis";

            var yAxis = (Axis)xlChart.Axes(XlAxisType.xlSeriesAxis);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Y Axis";

            var zAxis = (Axis)xlChart.Axes(XlAxisType.xlValue);
            zAxis.HasTitle = true;
            zAxis.AxisTitle.Text = "Z Axis";

            xlChart.HasTitle = true;
            xlChart.ChartTitle.Text = "A Nice Chart";

            // Remove legend:
            xlChart.HasLegend = false;
        }

        public static void PlotResult(double[,] result, string fileName)
        {
            var ws = GetFreshWorksheet(fileName);

            var c1 = (Range)ws.Cells[1, 1];
            var c2 = (Range)ws.Cells[1 + result.Height() - 1, result.Width()];
            var range = ws.Range[c1, c2];

            range.Value = result;

            Create3DSurfacePlot(range);
        }
    }
}