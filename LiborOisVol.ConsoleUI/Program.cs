﻿using System;
using LiborOisVol.Core;
using static LiborOisVol.Test.TestData.TestEnvironments;
using LiborOisVol.Model;
using System.Linq;
using LiborOisVol.Core.Sobol;
using LiborOisVol.Core.Stats;

namespace LiborOisVol.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Get Test Data
            var env = July10Environment;

            // 2. Strip Caplets from Caps
            var capletQuotes = CapletBuilder.Build(env,
                July10CapVolCurve, July10_LIBOR_6M_PSEUDO_DF_Curve)
                .Take(39).ToArray();

            // 3. Fit ABCD Volatility Model
            var volatilityModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            // 4. Find k_i adjustment factors
            var kk = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, volatilityModel)
                .Take(10).ToArray();

            var correlationModel = new ABCDModelCorrelation();

            var resetDates = env.ResetDatesBetween("6M", "54M");

            // TT is the year fraction vector
            var TT = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate, resetDates,
                env.Parameters.Calendar, env.Parameters.DCC);

            var CC = MatrixBuilder.BuildCovarianceMatrices(TT, volatilityModel, correlationModel, kk);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var nDraws = TT.Length * (TT.Length + 1) / 2;

            var random = SobolFactory.Build((uint)nDraws, "Sobol/sobol-direction-numbers.dat");

            var terminalDate = env.AddTenorAdjusted(resetDates.Last(), env.Parameters.LiborTenor);

            var resetsPlusFinalMaturity = resetDates.ToArray().WithAppendedValue(terminalDate);
            
            // TTX (TT-extended) is the start date, all reset dates, plus the final maturity date
            var TTX = env.YearFractionsFromStartFor(resetsPlusFinalMaturity).WithPrependedValue(0);

            // f0 is the initial forward rate vector as seen on the pricing date
            var f0 = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, July10_LIBOR_6M_PSEUDO_DF_Curve, env.Parameters.Calendar, env.Parameters.DCC);

            var oisRates = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var liborOisSpreads = f0.Zip(oisRates, (libor, ois) => libor - ois).ToArray();

            var P = CommonCalculations.BuildDiscountFactorVector(resetsPlusFinalMaturity, env.DiscountCurve);

            // tau is the vector of year fractions of the periods between resets 
            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);
            
            // calculate the par swap rate of the IR swap so it has MTM=0 at inception
            var sr = CommonCalculations.SwapRate(TT, tau, P, f0);

            // 5. Set up the LMM
            var liborMarketModel = new LiborMarketModel(CC, AA, random, tau, f0);

            // 65,536 iterations
            var nSims = 1 << 16;            
            
            var exposureCalculator = new InterestRateSwapMTMCalculator(tau, sr, liborOisSpreads);

            var buckets = new Buckets(TTX.Length);

            // 6. Run the simulations
            for (var i = 0; i < nSims; i++)
            {
                liborMarketModel.Simulate();

                var mtm = exposureCalculator.Calculate_MTM_TimeSeries(liborMarketModel.F);

                buckets.Add(mtm);
            }
            
            // 7. Calculate EE = max(MTM, 0) for each reset
            var expectedExposure = buckets.Data
                .Select(b => b.Select(d => Math.Max(d, 0)).Average())
                .ToArray();

            var recoveryRate = 0.4;

            // 8. Bootstrap survival curve from CDS spreads
            var survivalCurve = CreditCalculations.GetSurvivalProbabilityCurve(
                recoveryRate, July10_LLOY_CDS_MID_Curve, July10_OIS_DF_Curve, resetsPlusFinalMaturity, tau);

            var allDates = resetsPlusFinalMaturity.WithPrependedValue(env.Parameters.PricingDate);

            // 9. Calculate CVA
            var cva = CreditCalculations.CalculateCVA(
                            recoveryRate, allDates, expectedExposure, survivalCurve, July10_OIS_DF_Curve);

            Console.WriteLine($"CVA for 5Y Interest Rate Swap: {(1E4 * cva):0.000} bps");

            Console.Read();
        }
    }
}