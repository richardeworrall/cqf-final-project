﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LiborOisVol.ConsoleUI
{
    public class MultiFormApplicationContext : ApplicationContext
    {
        private void OnFormClosed(object sender, EventArgs e)
        {
            if (Application.OpenForms.Count == 0)
            {
                ExitThread();
            }
        }

        public void ShowForm(Form form)
        {
            form.FormClosed += OnFormClosed;
            form.Show();
        }

        public MultiFormApplicationContext(IEnumerable<Form> forms)
        {
            foreach (var form in forms) ShowForm(form);
        }
    }
}