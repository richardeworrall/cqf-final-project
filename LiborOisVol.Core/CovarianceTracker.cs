﻿using System;

namespace LiborOisVol.Core
{
    public class CovarianceTracker
    {
        // Using https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm

        private double C;

        private uint _n;

        private MeanVarianceTracker X = new MeanVarianceTracker();
        private MeanVarianceTracker Y = new MeanVarianceTracker();

        private readonly object _lockObj = new object();

        public static CovarianceTracker Empty => new CovarianceTracker();

        public CovarianceTracker(string id = null)
        {
            ID = id;
        }

        public void Add(double x, double y)
        {
            _n++;
            // Note the order here is deliberate
            X.Add(x);
            C += (x - X.Mean) * (y - Y.Mean);
            Y.Add(y);
        }

        public static CovarianceTracker Combine(CovarianceTracker a, CovarianceTracker b, string newId = null)
        {
            var newIdOrDefault = string.IsNullOrWhiteSpace(newId)
                ? a.ID ?? b.ID
                : newId;

            return new CovarianceTracker(newIdOrDefault)
            {
                X = MeanVarianceTracker.Combine(a.X, b.X),
                Y = MeanVarianceTracker.Combine(a.Y, b.Y),
                C = a.C + b.C + (a.X.Mean + b.X.Mean) * (a.Y.Mean - b.Y.Mean) * (a._n * b._n) / (a._n + b._n)
            };
        }

        public string ID { get; }
        public double Covariance => C / _n;
        public double Correlation => Covariance / (X.StdDev * Y.StdDev);
    }
}