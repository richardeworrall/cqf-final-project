﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Core
{
    public static class ArrayHelper
    {
        public static T[] WithoutFirstElement<T>(this T[] arr)
        {
            return arr.WithoutFirstNElements(1);
        }

        public static T[] WithoutLastElement<T>(this T[] arr)
        {
            return arr.WithoutLastNElements(1);
        }

        public static T[] WithoutFirstNElements<T>(this T[] arr, uint n)
        {
            return arr.Skip((int)Math.Min(arr.Length, n)).ToArray();
        }

        public static T[] WithoutLastNElements<T>(this T[] arr, uint n)
        {
            return arr.Take((int) Math.Max(arr.Length - n, 0)).ToArray();
        }

        public static T[] WithPrependedValue<T>(this T[] arr, T item)
        {
            T[] newValues = new T[arr.Length + 1];
            newValues[0] = item;
            Array.Copy(arr, 0, newValues, 1, arr.Length);
            return newValues;
        }

        public static T[] WithAppendedValue<T>(this T[] arr, T item)
        {
            T[] newValues = new T[arr.Length + 1];
            newValues[arr.Length] = item;
            Array.Copy(arr, 0, newValues, 0, arr.Length);
            return newValues;
        }

        public static IEnumerable<T> SliceRow<T>(this T[,] array, int col)
        {
            for (var i = 0; i < array.GetLength(0); i++)
            {
                yield return array[i, col];
            }
        }

        public static IEnumerable<T> SliceColumn<T>(this T[,] array, int row)
        {
            for (var i = 0; i < array.GetLength(1); i++)
            {
                yield return array[row, i];
            }
        }

        public static void FillWith<T>(this T[] arr, Func<int, T> valueFactory)
        {
            for (var i = 0; i < arr.Length; i++)
            {
                var j = i;
                arr[i] = valueFactory(j);
            }
        }
    }
}
