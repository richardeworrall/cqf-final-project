﻿using System;

namespace LiborOisVol.Core
{
    public static class MatrixHelper
    {
        public static double[] Multiply(double[,] A, double[] b)
        {
            if (A.Width() != b.Length) throw new ArgumentException(
                $"Dimension mismatch; A is {A.Height()}x{A.Width()} but b is {b.Length}x1");

            var result = new double[A.Height()];

            for(var i = 0; i < A.Height(); i++)
            {
                for (var j = 0; j < b.Length; j++)
                {
                    result[i] += A[i, j] * b[j];
                }
            }

            return result;
        }

        public static double[,] Multiply(double[,] A, double[,] B)
        {
            if (A.Width() != B.Height()) throw new ArgumentException(
                $"Dimension mismatch; A is {A.Height()}x{A.Width()} but b is {B.Height()}x{B.Width()}");

            var result = new double[A.Height(), B.Width()];

            for (var i = 0; i < A.Height(); i++)
            {
                for (var j = 0; j < B.Width(); j++)
                {
                    for(var k = 0; k < A.Width(); k++)
                    {
                        result[i, j] += A[i, k] * B[k, j];
                    }
                }
            }

            return result;
        }

        public static void HalfMultiply(double[,] A, double[] b, double[] target)
        {
            for (var i = 0; i < A.Height(); i++)
            {
                target[i] = 0;

                for (var j = 0; j <= i; j++)
                {
                    target[i] += A[i, j] * b[j];
                }
            }
        }

        public static double[,] Transpose(double[,] A)
        {
            var result = new double[A.Height(), A.Width()];

            for (var i = 0; i < A.Height(); i++)
            {
                for (var j = 0; j < A.Width(); j++)
                {
                    result[i, j] += A[j, i];
                }
            }

            return result;
        }

        public static double[,] CholeskyDecompose(this double[,] matrix)
        {
            var N = matrix.Height();

            var result = new double[N, N];

            result[0, 0] = 1.0;

            for (var i = 0; i < N; ++i)
            {
                for (var j = i; j < N; ++j)
                {
                    var sum = matrix[i, j];

                    for (var k = 0; k <= i - 1; ++k)
                    {
                        sum = sum - result[i, k] * result[j, k];
                    }

                    if (i == j)
                    {
                        if (sum <= 0)
                        {
                            result[i, i] = 0;
                        }
                        else
                        {
                            result[i, i] = Math.Sqrt(sum);
                        }
                    }

                    if (Math.Abs(result[i, i]) > 1E-9)
                    {
                        result[j, i] = sum / result[i, i];
                    }
                    else
                    {
                        result[j, i] = 0;
                    }
                }
            }

            return result;
        }

        public static double[,] HadamardProduct(this double[,] A, double[,] B)
        {
            var C = A.Copy();

            for (var i = 0; i < A.Height(); i++)
            {
                for (var j = 0; j < A.Width(); j++)
                {
                    C[i, j] *= B[i, j];
                }
            }

            return C;
        }

        public static int Height<T>(this T[,] A)
        {
            return A.GetLength(0);
        }

        public static int Width<T>(this T[,] A)
        {
            return A.GetLength(1);
        }

        public static T[,] Copy<T>(this T[,] A)
        {
            var newArr = new T[A.Height(), A.Width()];
            Array.Copy(A, 0, newArr, 0, A.Length);
            return newArr;
        }
    }
}