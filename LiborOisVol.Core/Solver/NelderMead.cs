﻿using System;
using LiborOisVol.Core.Sobol;

namespace LiborOisVol.Core.Solver
{
    public delegate double ObjectiveFunction(double[] p);
    public delegate void Constrain(double[] p);
    public delegate double[][] SeedGenerator(uint dimension);

    public static class SeedGenerators
    {
        public static SeedGenerator Sobol => dimension =>
        {
            var gen = SobolFactory.Build(dimension, @"Sobol/sobol-direction-numbers.dat");

            var points = new double[dimension + 1][];

            for (var i = 0; i < 1000; i++)
            {
                gen.DrawNext();
            }

            for (var i = 0; i < dimension + 1; i++)
            {
                points[i] = new double[dimension];

                gen.DrawNext();

                for (var j = 0; j < dimension; j++)
                {
                    points[i][j] = 2.0 * gen.CurrentDrawDoubles[j] - 1.0;
                }
            }

            return points;
        };

        public static SeedGenerator Random => dimension =>
        {
            var random = new System.Random();

            var points = new double[dimension + 1][];

            for (var i = 0; i < dimension + 1; i++)
            {
                points[i] = new double[dimension];

                for (var j = 0; j < dimension; j++)
                {
                    points[i][j] = 2.0 * random.NextDouble() - 1.0;
                }
            }

            return points;
        };
    }

    public static class Constraints
    {
        public static Constrain NoConstraint => p => { };
    }
    
    public class NelderMead
    {
        private readonly uint _dimension;
        private readonly ObjectiveFunction _fObj;
        private readonly Constrain _fConstraint;
        private readonly uint _maxIters;
        private readonly double _tolerance;

        private readonly double[][] _points;
        
        private readonly double[] _scores;
        private double _previousBestScore = double.MaxValue;
        
        private const double GrowFactor = 2.0;
        private const double ReflectFactor = 1.0;
        private const double ContractFactor = 0.5;
        private const double ShrinkFactor = 0.5;

        public NelderMead(uint dimension, ObjectiveFunction fObj, Constrain fConstraint = null, double[] scaling = null,
             uint maxIters = 100000, double tolerance = 1E-12, SeedGenerator seedGenerator = null)
        {
            _dimension = dimension;
            _fObj = fObj;
            _fConstraint = fConstraint;
            _maxIters = maxIters;
            _tolerance = tolerance;

            if (seedGenerator == null) seedGenerator = SeedGenerators.Random;
            _points = seedGenerator(dimension);

            if (scaling != null)
            {
                if (scaling.Length != dimension)
                    throw new ArgumentException("Scaling vector length must match dimension");

                for (var i = 0; i < dimension + 1; i++)
                {
                    for (var j = 0; j < dimension; j++)
                    {
                        _points[i][j] *= scaling[j];
                    }
                }
            }

            if (fConstraint == null) _fConstraint = Constraints.NoConstraint;
            for (var i = 0; i < dimension + 1; i++)
            {
                _fConstraint(_points[i]);
            }

            _scores = new double[_dimension + 1];
        }

        public void Solve(out double[] result, out double bestScore)
        {
            var iterCount = 0;

            doEval:

            EvaluateObjectiveFunction();

            for(var i=0; i<_scores.Length;i++)
            {
                if (double.IsNaN(_scores[i])) _scores[i] = double.MaxValue;
            }

            skipEval:

            SortPoints();

            if (iterCount > _maxIters || IsWithinTolerance())
            {
                bestScore = _scores[0];
                result = _points[0];
                return;
            }

            _previousBestScore = _scores[0];
            iterCount++;

            var centroid = CalculateCentroid();

            var reflected = Reflect(_points[_dimension], centroid);

            var reflectedScore = _fObj(reflected);

            if (reflectedScore < _scores[_dimension - 1])
            {
                if (reflectedScore >= _scores[0])
                {
                    _points[_dimension] = reflected;
                    _scores[_dimension] = reflectedScore;

                    goto skipEval;
                }

                var expanded = Expand(reflected, centroid);

                var expandedScore = _fObj(expanded);

                if (expandedScore < reflectedScore)
                {
                    _points[_dimension] = expanded;
                    _scores[_dimension] = expandedScore;

                    goto skipEval;
                }

                _points[_dimension] = reflected;
                _scores[_dimension] = reflectedScore;

                goto skipEval;
            }

            var contracted = Contract(_points[_dimension], centroid);

            var contractedScore = _fObj(contracted);

            if (contractedScore < _scores[_dimension])
            {
                _points[_dimension] = contracted;
                _scores[_dimension] = contractedScore;

                goto skipEval;
            }

            for (var i = 1; i < _dimension + 1; i++)
            {
                _points[i] = Shrink(_points[i], _points[0]);
            }

            goto doEval;
        }

        private bool IsWithinTolerance()
        {
            var tolerance = 0.0;

            for (var j = 0; j < _dimension; j++)
            {
                tolerance += Math.Abs(_points[0][j] - _points[_dimension][j]);
            }

            tolerance /= _dimension;

            return tolerance < _tolerance;
        }

        private double[] CalculateCentroid()
        {
            var result = new double[_dimension];

            for (var j = 0; j < _dimension; j++)
            {
                for (var i = 0; i < _dimension; i++)
                {
                    result[j] += _points[i][j];
                }

                result[j] /= _dimension;
            }

            return result;
        }

        private double[] Reflect(double[] point, double[] centroid)
        {
            var result = new double[_dimension];

            for (var i = 0; i < _dimension; i++)
            {
                result[i] = centroid[i] + ReflectFactor * (centroid[i] - point[i]);
            }

            // Apply constraints
            _fConstraint(result);

            return result;
        }

        private double[] Expand(double[] reflected, double[] centroid)
        {
            var result = new double[_dimension];

            for (var i = 0; i < _dimension; i++)
            {
                result[i] = centroid[i] + GrowFactor * (reflected[i] - centroid[i]);
            }

            // Apply constraints
            _fConstraint(result);

            return result;
        }

        private double[] Contract(double[] point, double[] centroid)
        {
            var result = new double[_dimension];

            for (var i = 0; i < _dimension; i++)
            {
                result[i] = centroid[i] + ContractFactor * (point[i] - centroid[i]);
            }

            // Apply constraints
            _fConstraint(result);

            return result;
        }

        private double[] Shrink(double[] point, double[] bestPoint)
        {
            var result = new double[_dimension];

            for (var i = 0; i < _dimension; i++)
            {
                result[i] = bestPoint[i] + ShrinkFactor * (point[i] - bestPoint[i]);
            }

            // Apply constraints
            _fConstraint(result);

            return result;
        }

        private void EvaluateObjectiveFunction()
        {
            for (var i = 0; i < _dimension + 1; i++)
            {
                _scores[i] = _fObj(_points[i]);
            }
        }

        private void SortPoints()
        {
            // Using an insertion sort here -- https://en.wikipedia.org/wiki/Insertion_sort

            for (var i = 0; i < _scores.Length - 1; i++)
            {
                for (var j = i + 1; j > 0; j--)
                {
                    if (_scores[j - 1] > _scores[j])
                    {
                        var tempScore = _scores[j - 1];
                        _scores[j - 1] = _scores[j];
                        _scores[j] = tempScore;

                        var tempPoint = _points[j - 1];
                        _points[j - 1] = _points[j];
                        _points[j] = tempPoint;
                    }
                }
            }
        }
    }
}