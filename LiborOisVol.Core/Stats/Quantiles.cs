﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Core.Stats
{
    public class Buckets
    {
        public List<double>[] Data { get; }
        
        public Buckets(int nBuckets)
        {
            Data = Enumerable
                        .Repeat(0, nBuckets)
                        .Select(_ => new List<double>()).ToArray();
        }

        public void Add(double[] data)
        {
            for (var i = 0; i < Data.Length; i++) Data[i].Add(data[i]);
        }

        public IEnumerable<double> Reduce(Func<IEnumerable<double>, double> f)
        {
            return Data.Select(f);
        }
    }

    public class Quantiles
    {
        private readonly double[] _dataSorted;

        public Quantiles(IEnumerable<double> data)
        {
            _dataSorted = data.OrderBy(x => x).ToArray();

            if (_dataSorted.Length == 0)
                throw new ArgumentException("No data was provided");
        }

        public double Get(double quantile)
        {
            if (quantile < 0 || quantile > 1.0)
                throw new ApplicationException("Can't get quantile outside [0, 1]");

            var q = quantile * _dataSorted.Length;

            if (Math.Abs(q % 1) <= double.Epsilon * 100)
            {
                return _dataSorted[(int)q - 1];
            }
            else
            {
                var dBelow = _dataSorted[(int) q - 1];
                var dAbove = _dataSorted[(int) q];

                return dBelow + (q - (int) q) * (dAbove - dBelow);
            }
        }
    }
}