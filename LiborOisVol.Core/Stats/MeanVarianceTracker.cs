﻿using System;
using System.Collections.Generic;

namespace LiborOisVol.Core
{
    public class MeanVarianceTracker
    {
        // Using https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm

        private uint _n;
        private double _mean;
        private double _m;

        private readonly object _lockObj = new object();
        
        private readonly string _id;

        public static MeanVarianceTracker Empty => new MeanVarianceTracker();

        public MeanVarianceTracker(string id = null)
        {
            _id = id;
        }

        public void AddRange(IEnumerable<double> values)
        {
            foreach (var value in values) Add(value);
        }

        public void Add(double value)
        {
            _n++;
            var delta = value - _mean;
            _mean += delta / _n;
            _m += delta * (value - _mean);
        }

        public static MeanVarianceTracker Combine(MeanVarianceTracker a, MeanVarianceTracker b, string newId = null)
        {
            var newIdOrDefault = string.IsNullOrWhiteSpace(newId)
                                    ? a.ID ?? b.ID
                                    : newId;

            var delta = b._mean - a._mean;

            return new MeanVarianceTracker(newIdOrDefault)
            {
                _n = a._n + b._n,
                _mean = (a._mean * a._n + b._mean * b._n) / (a._n + b._n),
                _m = a._m + b._m + delta * delta * (a._n * b._n) / (a._n + b._n)
            };
        }

        public string ID => _id;
        public double SimulationCount => _n;
        public double Mean => _mean;
        public double Variance => _m / _n;
        public double StdDev => Math.Sqrt(Variance);
    }
}