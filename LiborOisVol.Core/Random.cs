﻿using System;

namespace LiborOisVol.Core
{
    public delegate IUniformRandom UniformRandomFactory(uint dimension);

    public delegate UniformRandomFactory UniformRandomOffsetFactory(uint offset);
    
    public interface IUniformRandom
    {
        double[] CurrentDraw { get; }

        void DrawNext();
    }

    public class SimpleUniformRandom : IUniformRandom
    {
        public double[] CurrentDraw { get; }

        private readonly Random _random;

        public SimpleUniformRandom(uint dimensions)
        {
            CurrentDraw = new double[dimensions];
            _random = new Random();
        }

        public void DrawNext()
        {
            for (var i = 0; i < CurrentDraw.Length; i++)
            {
                CurrentDraw[i] = _random.NextDouble();
            }
        }
    }
}