﻿using System;
using System.IO;
using System.Linq;
using ZstdNet;

namespace LiborOisVol.Core.Sobol
{
    public static class SobolFactory
    {
        public static UniformRandomOffsetFactory GetFactory(string dataFilePath)
            => offset => dimensions => Build(dimensions, dataFilePath, offset);

        public static Sobol Build(uint dimensions, string dataFilePath, uint offset = 0)
        {
            var compressedBytes = File.ReadAllBytes(dataFilePath);

            byte[] rawBytes;

            using (var decompressor = new Decompressor())
            {
                rawBytes = decompressor.Unwrap(compressedBytes);
            }

            var data = DimensionSet.Parser.ParseFrom(rawBytes);

            if (dimensions > data.Dimensions.Count)
                throw new NotSupportedException(
                    $"Data file {Path.GetFileName(dataFilePath)} supports a maximum of " +
                    $"{data.Dimensions.Count} dimensions");

            var a = new ulong[dimensions];

            var m = new ulong[dimensions][];

            for (var i = 0; i < dimensions; i++)
            {
                a[i] = data.Dimensions[i].A;

                m[i] = data.Dimensions[i].M.ToArray();
            }

            return new Sobol(dimensions, a, m, offset);
        }
    }
}