﻿using System;
using System.Collections.Generic;

namespace LiborOisVol.Core.Sobol
{
    public class Sobol : IUniformRandom
    {
        public double[] CurrentDraw => CurrentDrawDoubles;

        public uint CurrentDrawNumber => _currentDrawNumber;

        public readonly double[] CurrentDrawDoubles;

        private readonly ulong[,] _v;
        
        private uint _currentDrawNumber;
        private readonly ulong[] _currentDraw;

        private readonly uint _d;
       
        public Sobol(uint d, ulong[] a, ulong[][] mInit, uint offset = 0)
        {
            _d = d;

            _v = new ulong[d, 64];

            _currentDraw = new ulong[d];
            CurrentDrawDoubles = new double[d];

            for (var k = 0; k < d; k++)
            {
                var m = new ulong[64];

                var sj = mInit[k].Length;

                Array.Copy(mInit[k], 0, m, 0, sj);

                for (var l = sj; l < 64; l++)
                {
                    ulong m_next = 0;

                    for (var j = 1; j < sj; j++)
                    {
                        if ((a[k] & (1ul << sj - 1 - j)) != 0) m_next ^= m[l - j] << j;
                    }

                    m_next ^= m[l - sj];

                    m_next ^= m[l - sj] << sj;

                    m[l] = m_next;
                }

                for (var i = 0; i < 64; i++) _v[k, i] = m[i] << (63 - i);
            }

            if (offset != 0) ApplyOffset(offset);
        }

        private void ApplyOffset(uint offset)
        {
            ulong g = offset ^ (offset >> 1);

            for (var k = 0; k < _d; k++)
            {
                for (var i = 0; i < 64; i++)
                {
                    if ((g & (1ul << i)) != 0) _currentDraw[k] ^= _v[k, i];
                }

                CurrentDrawDoubles[k] = _currentDraw[k] / (double) ulong.MaxValue;
            }

            _currentDrawNumber = offset;
        }

        public void DrawNext()
        {
            uint u = ~CurrentDrawNumber;

            uint c;
            if ((u & 0x1) != 0)
            {
                c = 0;
            }
            else
            {
                c = 1;

                if ((u & 0xffffffff) == 0)
                {
                    u >>= 32;
                    c += 32;
                }
                if ((u & 0xffff) == 0)
                {
                    u >>= 16;
                    c += 16;
                }
                if ((u & 0xff) == 0)
                {
                    u >>= 8;
                    c += 8;
                }
                if ((u & 0xf) == 0)
                {
                    u >>= 4;
                    c += 4;
                }
                if ((u & 0x3) == 0)
                {
                    u >>= 2;
                    c += 2;
                }

                c -= u & 0x1;
            }

            for (var k = 0; k < _d; k++)
            {
                _currentDraw[k] ^= _v[k, c];

                CurrentDrawDoubles[k] = _currentDraw[k] / (double) ulong.MaxValue;
            }

            _currentDrawNumber++;
        }
    }

    public static class SobolHelper
    {
        public static IEnumerable<double[]> TakeNext(this Sobol sobol, int n)
        {
            for (var i = 0; i < n; i++)
            {
                sobol.DrawNext();
                var arr = new double[sobol.CurrentDrawDoubles.Length];
                Array.Copy(sobol.CurrentDrawDoubles, arr, sobol.CurrentDrawDoubles.Length);
                yield return arr;
            }
        }
    }
}