﻿using System;
using System.IO;
using System.Linq;
using Google.Protobuf;
using ZstdNet;

namespace LiborOisVol.Core.Sobol
{
    public static class SobolDataFileBuilder
    {
        public static void BuildDatFileFromTxtFile(string path, string binaryPath)
        {
            var lines = File.ReadAllLines(path);

            var dimensions = lines.Select(l =>
            {
                var numbers = l.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(ulong.Parse).ToArray();

                var d = numbers[0];

                var s = numbers[1];

                var a = numbers[2];

                var m = numbers.Skip(3).ToArray();

                var dimension = new Dimension
                {
                    Dimension_ = (uint)d,
                    Order = (uint)s,
                    A = a
                };

                dimension.M.AddRange(m);

                return dimension;
            });

            var dimensionSet = new DimensionSet();
            dimensionSet.Dimensions.AddRange(dimensions);

            var data = dimensionSet.ToByteArray();

            byte[] compressedData;

            using (var compressor = new Compressor(new CompressionOptions(CompressionOptions.MaxCompressionLevel)))
            {
                compressedData = compressor.Wrap(data);
            }
            
            using (var fs = new FileStream(binaryPath, FileMode.Create, FileAccess.Write))
            {
                fs.Write(compressedData, 0, compressedData.Length);
            }
        }
    }
}