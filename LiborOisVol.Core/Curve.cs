﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Core
{
    public interface ICurve
    {
        double Get(DateTime date);
        DateTime FirstDate { get; }
        DateTime LastDate { get; }
        IInterpolator UnderlyingInterpolator { get; }
    }

    public class Curve<TInterpolator> : ICurve
        where TInterpolator : IInterpolator
    {
        public IInterpolator UnderlyingInterpolator => _underlyingInterpolator;

        private readonly TInterpolator _underlyingInterpolator;

        public DateTime FirstDate => DateTime.FromOADate(_underlyingInterpolator.LowerBound);
        public DateTime LastDate => DateTime.FromOADate(_underlyingInterpolator.UpperBound);

        public Curve(IDictionary<DateTime, double> knownValues)
        {
            _underlyingInterpolator =
                (TInterpolator) Activator.CreateInstance(typeof(TInterpolator),
                    knownValues.ToDictionary(x => x.Key.ToOADate(), x => x.Value));
        }
        
        public double Get(DateTime date)
        {
            return _underlyingInterpolator.Get(date.ToOADate());
        }
    }
}