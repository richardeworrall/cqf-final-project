﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Core
{
    public interface IInterpolator
    {
        double Get(double x);
        double LowerBound { get; }
        double UpperBound { get; }
    }

    public abstract class BaseInterpolator : IInterpolator
    {
        public double LowerBound => xx[0];
        public double UpperBound => xx[xx.Length-1];
        
        protected readonly double[] xx;
        protected readonly double[] yy;

        protected BaseInterpolator(IDictionary<double, double> knownValues)
        {
            var orderedKnownValues = new SortedDictionary<double, double>(knownValues);

            xx = orderedKnownValues.Keys.ToArray();
            yy = orderedKnownValues.Values.ToArray();
        }

        protected BaseInterpolator(double[] xx, double[] yy)
        {
            if (xx.Length != yy.Length)
                throw new ArgumentException("Need the same number of x and y data points");

            var pairs = xx.Zip(yy, Tuple.Create).OrderBy(xy => xy.Item1).ToArray();

            this.xx = pairs.Select(xy => xy.Item1).ToArray();
            this.yy = pairs.Select(xy => xy.Item2).ToArray();
        }

        public abstract double Get(double x);
        
        //  Hunt returns the index of the element with x-value equal to argument specified, or else the index 
        //  of the largest element with x-value below what was specified, using binary search.
        protected int Hunt(double x, out int index)
        {
            index = Array.BinarySearch(xx, x);

            if (index >= 0) return 0;
            
            index = ~index;

            // Case where index refers to smallest element that is larger than the requested value
            if (index < xx.Length) return -1;

            // Case where no element in the array is larger than the requested value
            return 1;
        }
    }

    public class LinearInterpolator : BaseInterpolator
    {
        public LinearInterpolator(IDictionary<double, double> knownValues) 
            : base(knownValues) { }

        public LinearInterpolator(double[] xx, double[] yy)
            : base(xx, yy) { }

        public override double Get(double x)
        {
            var result = Hunt(x, out var idx);
            
            switch(result)
            {
                case 0:
                    return yy[idx];
                case -1:
                    if (idx == 0) return yy[0];

                    var xBelow = xx[idx - 1]; var xAbove = xx[idx];
                    var yBelow = yy[idx - 1]; var yAbove = yy[idx];
                    
                    return yBelow + (x - xBelow) / (xAbove - xBelow) * (yAbove - yBelow);
                case 1:
                    return yy[idx-1];
                default:
                    throw new ApplicationException();
            }
        }
    }

    public class MonotoneCubicInterpolator : BaseInterpolator
    {
        // See: https://en.wikipedia.org/wiki/Monotone_cubic_interpolation

        private double[] _m;

        private static double H00(double t) { return (1 + 2 * t) * (1 - t) * (1 - t); }
        private static double H10(double t) { return t * (1 - t) * (1 - t); }
        private static double H01(double t) { return t * t * (3 - 2 * t); }
        private static double H11(double t) { return t * t * (t - 1); }

        public MonotoneCubicInterpolator(IDictionary<double, double> knownValues)
            : base(knownValues) { Initialize(); }

        public MonotoneCubicInterpolator(double[] xx, double[] yy)
            : base(xx, yy) { Initialize(); }

        private void Initialize()
        {
            var delta = new double[xx.Length - 1];

            for (var k = 0; k < xx.Length - 1; k++)
            {
                delta[k] = (yy[k + 1] - yy[k]) / (xx[k + 1] - xx[k]);
            }

            _m = new double[xx.Length];

            _m[0] = delta[0];
            _m[xx.Length - 1] = delta[xx.Length - 2];

            for (var k = 1; k < xx.Length - 1; k++)
            {
                if (Math.Sign(delta[k]) != Math.Sign(delta[k - 1]))
                {
                    _m[k] = 0;
                }
                else
                {
                    _m[k] = 0.5 * (delta[k] + delta[k - 1]);
                }
            }

            for (var k = 0; k < xx.Length - 1; k++)
            {
                if (Math.Abs(delta[k]) < 1E-10)
                {
                    _m[k] = 0;
                    _m[k + 1] = 0;
                }
                else
                {
                    var alpha = _m[k] / delta[k];
                    var beta = _m[k + 1] / delta[k];

                    if (alpha < 0) _m[k] = 0;
                    if (beta < 0) _m[k + 1] = 0;

                    if (alpha * alpha + beta * beta > 9)
                    {
                        var tau = 3.0 / Math.Sqrt(alpha * alpha + beta * beta);
                        _m[k] = tau * alpha * delta[k];
                        _m[k + 1] = tau * beta * delta[k];
                    }
                }
            }
        }
        
        public override double Get(double x)
        {
            var result = Hunt(x, out var idx);

            switch (result)
            {
                case 0:
                    return yy[idx];
                case -1:

                    if (idx == 0)
                    {
                        throw new ArgumentException(
                            $"{nameof(MonotoneCubicInterpolator)} cannot extrapolate data");
                    }

                    var xBelow = xx[idx - 1]; var xAbove = xx[idx];
                    var yBelow = yy[idx - 1]; var yAbove = yy[idx];
                    var mBelow = _m[idx-1]; var mAbove = _m[idx];

                    var h = xAbove - xBelow;
                    var t = (x - xBelow) / h;

                    return  yBelow * H00(t) 
                            + h * mBelow * H10(t) 
                            + yAbove * H01(t) 
                            + h * mAbove * H11(t);

                case 1:
                    throw new ArgumentException(
                        $"{nameof(MonotoneCubicInterpolator)} cannot extrapolate data");
                default:
                    throw new ApplicationException();
            }
        }
    }
}