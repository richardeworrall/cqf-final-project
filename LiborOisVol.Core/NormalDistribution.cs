﻿using System;

namespace LiborOisVol.Core
{
    public static class NormalDistribution
    {
        public static double PDF(double x)
        {
            return Math.Exp(-(x * x) / 2.0) / Math.Sqrt(2 * Math.PI);
        }

        /// <summary>
        /// Cumulative Normal Distribution Function
        /// Adapted from: https://lyle.smu.edu/~aleskovs/emis/sqc2/accuratecumnorm.pdf
        /// </summary>
        public static double CDF(double x)
        {
            if (Math.Abs(x) < 1E-12) return 0.5;

            if (x > 37) return 1.0;
            if (x < -37) return -1.0;

            var xAbs = Math.Abs(x);

            var e = Math.Exp(-0.5 * (xAbs * xAbs));

            double build;
            double result;

            if (xAbs < 7.07106781186547)
            {
                build = 3.52624965998911E-02 * xAbs + 0.700383064443688;
                build = build * xAbs + 6.37396220353165;
                build = build * xAbs + 33.912866078383;
                build = build * xAbs + 112.079291497871;
                build = build * xAbs + 221.213596169931;
                build = build * xAbs + 220.206867912376;

                result = e * build;

                build = 8.83883476483184E-02 * xAbs + 1.75566716318264;
                build = build * xAbs + 16.064177579207;
                build = build * xAbs + 86.7807322029461;
                build = build * xAbs + 296.564248779674;
                build = build * xAbs + 637.333633378831;
                build = build * xAbs + 793.826512519948;
                build = build * xAbs + 440.413735824752;

                result = result / build;
            }
            else
            {
                build = xAbs + 0.65;
                build = xAbs + 4 / build;
                build = xAbs + 3 / build;
                build = xAbs + 2 / build;
                build = xAbs + 1 / build;
                result = e / build / 2.506628274631;
            }

            return x > 0.0 ? 1.0 - result : result;
        }

        /// <summary>
        /// Inverse Cumulative Normal Distribution Function
        /// Adapted from: http://web.ics.purdue.edu/~pasupath/stat598/code/norminv.m
        /// </summary>
        public static double InverseCDF(double x)
        {
            const double a0 = 2.50662823884;
            const double a1 = -18.61500062529;
            const double a2 = 41.39119773534;
            const double a3 = -25.44106049637;

            const double b0 = -8.47351093090;
            const double b1 = 23.08336743743;
            const double b2 = -21.06224101826;
            const double b3 = 3.13082909833;

            const double c0 = 0.3374754822726147;
            const double c1 = 0.9761690190917186;
            const double c2 = 0.1607979714918209;
            const double c3 = 0.0276438810333863;
            const double c4 = 0.0038405729373609;
            const double c5 = 0.0003951896511919;
            const double c6 = 0.0000321767881768;
            const double c7 = 0.0000002888167364;
            const double c8 = 0.0000003960315187;

            double result;
            var build = 0.0;

            var y = x - 0.5;
            var r1 = Math.Abs(y);

            if (r1 < 0.42)
            {
                y = y * y;

                var acc = y;

                build += a0;
                build += acc * a1; acc *= y;
                build += acc * a2; acc *= y;
                build += acc * a3;

                result = r1 * build;

                build = 1.0; acc = y;
                build += acc * b0; acc *= y;
                build += acc * b1; acc *= y;
                build += acc * b2; acc *= y;
                build += acc * b3;

                result /= build;
            }
            else
            {
                y = Math.Min(x, 1.0 - x);
                y = Math.Log(-Math.Log(y));

                var acc = y;

                build += c0;
                build += acc * c1; acc *= y;
                build += acc * c2; acc *= y;
                build += acc * c3; acc *= y;
                build += acc * c4; acc *= y;
                build += acc * c5; acc *= y;
                build += acc * c6; acc *= y;
                build += acc * c7; acc *= y;
                build += acc * c8;

                result = build;
            }

            return x - 0.5 > 0 ? result : -result;
        }
    }
}