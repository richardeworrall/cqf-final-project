﻿using System;
using LiborOisVol.Core.Solver.LiborOisVol.Solver;
using static LiborOisVol.Core.NormalDistribution;

namespace LiborOisVol.Model
{
    public static class Black76
    {
        public static double SwaptionPrice(double forwardRate, double strike, double sigma,
            double deltaStartReset, double B)
        {
            var a = Math.Log(forwardRate / strike);
            var b = 0.5 * sigma * sigma * deltaStartReset;
            var c = sigma * Math.Sqrt(deltaStartReset);

            var d1 = (a + b) / c;
            var d2 = (a - b) / c;

            var price = B * (forwardRate * CDF(d1) - strike * CDF(d2));

            return price;
        }

        public static double CapletPrice(double forwardRate, double strike, double sigma, 
            double deltaStartReset, double deltaResetMaturity, double discountFactorToMaturity)
        {
            var a = Math.Log(forwardRate / strike);
            var b = 0.5 * sigma * sigma * deltaStartReset;
            var c = sigma * Math.Sqrt(deltaStartReset);

            var d1 = (a + b) / c;
            var d2 = (a - b) / c;

            var price = discountFactorToMaturity * deltaResetMaturity * (forwardRate * CDF(d1) - strike * CDF(d2));

            return price;
        }

        public static double CapletImpliedVol(double forwardRate, double strike, double price, 
            double deltaStartReset, double deltaResetMaturity, double discountFactorToMaturity)
        {
            Func<double, double> objectiveFunction = 
                vol => CapletPrice(forwardRate, strike, vol, 
                            deltaStartReset, deltaResetMaturity, discountFactorToMaturity) 
                        - price;

            return IntervalBisection.Solve(objectiveFunction, 0.0, 1.0);
        }
    }
}