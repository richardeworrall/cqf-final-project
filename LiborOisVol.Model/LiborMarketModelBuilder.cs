﻿using System;
using System.Linq;
using LiborOisVol.Core;

namespace LiborOisVol.Model
{
    public static class LiborMarketModelBuilder
    {
        public static MarketModelFactory GetFactory(PricingEnvironment env,
            DateTime[] resetDates, DateTime lastMaturityDate,
            IVolatilityModel volatilityModel, double[] adjustmentFactors, ICorrelationModel correlationModel,
            ICurve forwardCurve, UniformRandomOffsetFactory randomFactory)
        {
            return offset =>
            {
                var random = randomFactory(offset);

                return Build(env, resetDates, lastMaturityDate,
                    volatilityModel, adjustmentFactors, correlationModel,
                    forwardCurve, random);
            };
        }

        public static IMarketModel Build(PricingEnvironment env, 
            DateTime[] resetDates, DateTime terminalDate,
            IVolatilityModel volatilityModel, double[] adjustmentFactors, ICorrelationModel correlationModel,
            ICurve forwardCurve, UniformRandomFactory randomFactory)
        {
            var resetDatesWithFinalMaturity = resetDates.WithAppendedValue(terminalDate);

            var TT = CommonCalculations.BuildYearFractionVector(
                env.Parameters.PricingDate, resetDates, env.Parameters.Calendar, env.Parameters.DCC);

            var Tau = CommonCalculations.BuildTauVector(
                resetDatesWithFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var CC = MatrixBuilder.BuildCovarianceMatrices(TT, volatilityModel, correlationModel, adjustmentFactors);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var F0 = CommonCalculations.BuildForwardRateVector(
                resetDatesWithFinalMaturity, 
                forwardCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var dimension = (uint) (F0.Length * (F0.Length + 1)) / 2;

            var random = randomFactory(dimension);

            var lmm = new LiborMarketModel(CC, AA, random, Tau, F0);

            return new LiborMarketModelWrapper(lmm, env, resetDates, Tau, terminalDate);
        }
    }
}