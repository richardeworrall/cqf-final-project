﻿using System;

namespace LiborOisVol.Model
{
    public interface ICorrelationModel
    {
        double GetCorrelation(double Ti, double Tj);
    }

    public class ABCDModelCorrelation : ICorrelationModel
    {
        private readonly double _beta;

        public ABCDModelCorrelation(double beta = 0.1)
        {
            _beta = beta;
        }

        public double GetCorrelation(double Ti, double Tj)
        {
            return Math.Exp(-1.0 * _beta * Math.Abs(Ti - Tj));
        }
    }

    public class ABCDModelCorrelationLongCorrelation : ICorrelationModel
    {
        private readonly double _beta;
        private readonly double _gamma;

        public ABCDModelCorrelationLongCorrelation(double beta = 0.1, double gamma = 0.1)
        {
            _beta = beta;
            _gamma = gamma;
        }

        public double GetCorrelation(double Ti, double Tj)
        {
            return _gamma + (1.0 - _gamma) * Math.Exp(-1.0 * _beta * Math.Abs(Ti - Tj));
        }
    }
}
