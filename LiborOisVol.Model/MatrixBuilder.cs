﻿using System;
using System.Linq;
using LiborOisVol.Core;

namespace LiborOisVol.Model
{
    public static class MatrixBuilder
    {
        public static double[][,] BuildCovarianceMatrices(double[] TT, 
                                        IVolatilityModel volModel, ICorrelationModel correlationModel,
                                                    double[] adjustmentFactors = null)
        {
            #region Validation

            if (TT == null || TT.Length == 0)
                throw new ArgumentException("Need a TT array");
            
            if (!IsStrictlyMonotonicallyIncreasing(TT))
                throw new ArgumentException("TT should be strictly monotonically increasing");
            
            if (Math.Abs(TT[0]) > 1E-15)
                TT = TT.WithPrependedValue(0.0);

            if (adjustmentFactors == null)
            {
                adjustmentFactors = Enumerable.Repeat(1.0, TT.Length - 1).ToArray();
            }
            else if (adjustmentFactors.Length != TT.Length - 1)
                throw new ArgumentException(
                    $"TT has length {TT.Length} (including a possibly prepended start time = 0)," +
                    $"suggesting {TT.Length-1} forward rates, " +
                    $"but adjustment factors has length {adjustmentFactors.Length}, " +
                    $"suggesting {adjustmentFactors.Length} forward rates");

            #endregion

            var covarianceMatrices = new double[TT.Length - 1][,];

            for (var i = 0; i < TT.Length - 1; i++)
            {
                var covarianceMatrix = new double[TT.Length - 1 - i, TT.Length - 1 - i];
                
                var tStart = TT[i];
                var tEnd = TT[i+1];

                for (var j = i; j < TT.Length - 1; j++)
                {
                    var Tj = TT[j + 1];
                    var aj = adjustmentFactors[j];

                    for (var k = i; k < TT.Length - 1; k++)
                    {
                        var Tk = TT[k + 1];
                        var ak = adjustmentFactors[k];

                        var rho = correlationModel.GetCorrelation(Tj, Tk);

                        var covarianceIntegral =
                            aj * ak * rho * volModel.Integral_Sigma_i_Sigma_j_Definite(tStart, tEnd, Tj, Tk);

                        if (covarianceIntegral <= 0) throw new ApplicationException("Matrix should be positive definite");

                        covarianceMatrix[j-i, k-i] = covarianceIntegral;
                    }
                }

                covarianceMatrices[i] = covarianceMatrix;
            }

            return covarianceMatrices;
        }

        public static double[][,] BuildCorrelationRootMatrices(
            double[] TT, ICorrelationModel correlationModel)
        {
            if (TT == null || TT.Length == 0)
                throw new ArgumentException("Need a TT array");

            if (!IsStrictlyMonotonicallyIncreasing(TT))
                throw new ArgumentException("TT should be strictly monotonically increasing");

            if (Math.Abs(TT[0]) < 1E-15)
                TT = TT.WithoutFirstElement();

            var matrices = new double[TT.Length][,];

            for (uint i = 0; i < TT.Length; i++)
            {
                var TTTruncated = TT.WithoutFirstNElements(i);

                var correlationMatrix = BuildCorrelationMatrix(TTTruncated, correlationModel);

                var pseudoSquareRoot = correlationMatrix.CholeskyDecompose();

                matrices[i] = pseudoSquareRoot;
            }

            return matrices;
        }

        public static double[,] BuildCorrelationMatrix(double[] TT, ICorrelationModel correlationModel)
        {
            #region Validation

            if (TT == null || TT.Length == 0)
                throw new ArgumentException("Need a TT array");

            if (!IsStrictlyMonotonicallyIncreasing(TT))
                throw new ArgumentException("TT should be strictly monotonically increasing");

            if (Math.Abs(TT[0]) < 1E-15)
                TT = TT.WithoutFirstElement();

            #endregion Validation

            var correlations = new double[TT.Length, TT.Length];

            for(var i = 0; i < TT.Length; i++)
            {
                for (var j = 0; j < TT.Length; j++)
                {
                    correlations[i, j] = correlationModel.GetCorrelation(TT[i], TT[j]);
                }
            }

            return correlations;
        }


        private static bool IsStrictlyMonotonicallyIncreasing(double[] TT)
        {
            if (TT == null || TT.Length == 0) throw new ArgumentException("Need an array");

            if (TT.Length < 2) return true;

            for (var i = 1; i < TT.Length; i++)
            {
                if (TT[i] <= TT[i - 1]) return false;
            }

            return true;
        }
    }
}
