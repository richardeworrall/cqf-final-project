﻿using System;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Model.Quotes
{
    public class SwaptionQuote
    {
        public Tenor LiborTenor { get; set;}

        public DateTime ExpiryDate { get; set; }

        public DateTime MaturityDate { get; set; }

        public double Volatility { get; set; }
    }
}