﻿using System;

namespace LiborOisVol.Model.Quotes
{
    public class CapletQuote
    {
        public DateTime ResetDate { get; set; }

        public DateTime MaturityDate { get; set; }

        public double Strike { get; set; }

        public double Volatility { get; set; }
    }
}