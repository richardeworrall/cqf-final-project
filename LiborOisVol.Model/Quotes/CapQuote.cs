﻿using System;

namespace LiborOisVol.Model.Quotes
{
    public class CapQuote
    {
        public DateTime ResetDate { get; set; }

        public DateTime MaturityDate { get; set; }

        public double Strike { get; set; }

        public double Volatility { get; set; }
    }
}