﻿using System;
using System.Collections.Generic;
using LiborOisVol.Core;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Model
{
    public class PricingParameters
    {
        public DateTime PricingDate { get; set; }

        public Tenor LiborTenor { get; set; }

        public ICalendar Calendar { get; set; }

        public DayCountConvention DCC { get; set; }

        public BusinessDayConvention BDC { get; set; }
    }

    public static class PricingParametersExtensions
    {
        public static ICurve BuildDiscountCurve(
            this PricingParameters e, Dictionary<Tenor, double> forwardLiborRates)
            => CommonCalculations.BuildDiscountCurveFromLibors(
                e.LiborTenor, e.PricingDate, e.Calendar, e.BDC, e.DCC, forwardLiborRates);

        public static ICurve BuildDiscountCurve(
            this PricingParameters e, Dictionary<DateTime, double> forwardLiborRates)
            => CommonCalculations.BuildDiscountCurveFromLibors(
                e.LiborTenor, e.PricingDate, e.Calendar, e.BDC, e.DCC, forwardLiborRates);

        public static DateTime ResetDateAt(this PricingParameters e, Tenor tenor)
            => CommonCalculations.AddTenorAdjusted(e.PricingDate, tenor, e.Calendar, e.BDC);

        public static DateTime AddTenorAdjusted(this PricingParameters e, DateTime date, Tenor tenor)
            => CommonCalculations.AddTenorAdjusted(date, tenor, e.Calendar, e.BDC);
        
        public static DateTime[] ResetDatesBetween(
            this PricingParameters e, Tenor tenorToStart, Tenor tenorStartToEnd)
            => CommonCalculations.BuildResetDateArray(
                e.PricingDate, tenorToStart, tenorStartToEnd, e.LiborTenor, e.Calendar, e.BDC);

        public static double[] YearFractionsFromStartFor(
            this PricingParameters e, DateTime[] resetDates)
            => CommonCalculations.BuildYearFractionVector(
                e.PricingDate, resetDates, e.Calendar, e.DCC);

        public static double[] YearFractionsBetweenResetsFor(
            this PricingParameters e, DateTime[] resetDates)
            => CommonCalculations.BuildTauVector(resetDates, e.Calendar, e.DCC);

        public static double T(this PricingParameters e, DateTime date)
            => e.Calendar.YearFraction(e.PricingDate, date, e.DCC);

        public static double tau(
            this PricingParameters e, DateTime startDate, DateTime endDate)
            => e.Calendar.YearFraction(startDate, endDate, e.DCC);
    }
}