﻿using LiborOisVol.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model.Calendar;
using LiborOisVol.Model.Quotes;

namespace LiborOisVol.Model
{
    public static class CapletBuilder
    {
        public static List<CapletQuote> Build(PricingEnvironment e, ICurve capVolCurve, ICurve pseudoDiscountCurve)
        {
            var caplets = GenerateCapletDates(e.Parameters.PricingDate, capVolCurve.LastDate, 
                e.Parameters.LiborTenor, e.Parameters.Calendar, e.Parameters.BDC);

            var caps = GenerateCapsAndPopulateStrikes(e.Parameters.PricingDate, caplets, e.Parameters.Calendar,
                e.Parameters.DCC, e.DiscountCurve, pseudoDiscountCurve);

            PopulateCapletVolatilities(e.Parameters.PricingDate, caps, caplets, 
                capVolCurve, e.Parameters.Calendar, e.Parameters.DCC, e.DiscountCurve, pseudoDiscountCurve);

            return caplets;
        }
        
        private static List<CapletQuote> GenerateCapletDates(DateTime baseDate, DateTime maxDate, 
            Tenor tenor, ICalendar calendar, BusinessDayConvention bdc)
        {
            var caplets = new List<CapletQuote>();

            // First caplet starts 3M after the base date:

            var resetDate = baseDate.Add(tenor);
            var maturityDate = resetDate.Add(tenor);

            while (maturityDate <= maxDate)
            {
                caplets.Add(new CapletQuote
                {
                    ResetDate = calendar.GetBusinessDay(resetDate, bdc),
                    MaturityDate = calendar.GetBusinessDay(maturityDate, bdc)
                });

                resetDate = maturityDate;
                maturityDate = resetDate.Add(tenor);
            }
            
            return caplets;
        }

        private static List<CapQuote> GenerateCapsAndPopulateStrikes(DateTime baseDate, 
            IEnumerable<CapletQuote> caplets, 
            ICalendar calendar, DayCountConvention dcc,
            ICurve discountCurve, ICurve pseudoDiscountCurve = null)
        {
            if (pseudoDiscountCurve == null) pseudoDiscountCurve = discountCurve;

            var sortedCaplets = caplets.OrderBy(x => x.ResetDate).ToArray();
            
            var fixedAcc = 0.0;
            var floatAcc = 0.0;

            var firstResetDate = sortedCaplets[0].ResetDate;

            var caps = new List<CapQuote>();

            foreach (var caplet in sortedCaplets) {

                var delta = calendar.YearFraction(caplet.ResetDate, caplet.MaturityDate, dcc);

                var forwardRate = CommonCalculations.ForwardRate(
                    pseudoDiscountCurve, caplet.ResetDate, caplet.MaturityDate, calendar, dcc);

                caplet.Strike = forwardRate;

                var discountFactor = discountCurve.Get(caplet.MaturityDate) / discountCurve.Get(baseDate);

                fixedAcc += discountFactor * delta;
                floatAcc += discountFactor * forwardRate * delta;

                var atmSwapRate = floatAcc / fixedAcc;

                caps.Add(new CapQuote
                {
                    ResetDate = firstResetDate,
                    MaturityDate = caplet.MaturityDate,
                    Strike = atmSwapRate
                });
            }

            return caps;
        }

        private static void PopulateCapletVolatilities(DateTime baseDate, 
            IEnumerable<CapQuote> caps, IEnumerable<CapletQuote> caplets, ICurve capVolCurve,
            ICalendar calendar, DayCountConvention dcc,
            ICurve discountCurve, ICurve pseudoDiscountCurve = null)
        {
            var sortedCaps = caps.OrderBy(x => x.ResetDate).ToArray();
            var sortedCaplets = caplets.OrderBy(x => x.ResetDate).ToArray();
            
            foreach (var cap in sortedCaps)
            {
                var capCaplets = sortedCaplets.Where(
                    caplet => caplet.MaturityDate <= cap.MaturityDate).ToArray();

                var capPrice = 0.0;
                var capletTotalPrice = 0.0;

                foreach (var cpl in capCaplets)
                {
                    var discountToMaturity = discountCurve.Get(cpl.MaturityDate) / discountCurve.Get(baseDate);
                    var deltaStartReset = calendar.YearFraction(baseDate, cpl.ResetDate, dcc);
                    var deltaResetMaturity = calendar.YearFraction(cpl.ResetDate, cpl.MaturityDate, dcc);

                    var sigmaStartMaturity = capVolCurve.Get(cap.MaturityDate);
                        
                    var forwardRate = CommonCalculations.ForwardRate(
                        pseudoDiscountCurve, cpl.ResetDate, cpl.MaturityDate, calendar, dcc);

                    var cplPrice = Black76.CapletPrice(forwardRate, cap.Strike, sigmaStartMaturity,
                        deltaStartReset, deltaResetMaturity, discountToMaturity);
                    
                    capPrice += cplPrice;
                    
                    if (cpl == capCaplets[capCaplets.Length - 1])
                    {
                        var cplPriceDiff = capPrice - capletTotalPrice;

                        cpl.Volatility = Black76.CapletImpliedVol(forwardRate, cap.Strike, cplPriceDiff,
                            deltaStartReset, deltaResetMaturity, discountToMaturity);
                    }
                    else
                    {
                        capletTotalPrice += Black76.CapletPrice(forwardRate, cap.Strike, cpl.Volatility, 
                            deltaStartReset, deltaResetMaturity, discountToMaturity);
                    }
                }
            }
        }
    }
}