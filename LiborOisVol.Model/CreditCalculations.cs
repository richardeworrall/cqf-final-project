﻿using LiborOisVol.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Model
{
    public static class CreditCalculations
    {
        public static double CalculateCVA(
            double recoveryRate,
            DateTime[] resetDatesIncludingFinalMaturity,
            double[] expectedExposure,
            ICurve survivalCurve,
            ICurve discountCurve)
        {
            var acc = 0.0;

            for (var i = 0; i < resetDatesIncludingFinalMaturity.Length - 1; i++)
            {
                var date = resetDatesIncludingFinalMaturity[i];
                var nextDate = resetDatesIncludingFinalMaturity[i+1];

                var df = discountCurve.Get(date);

                var dP = survivalCurve.Get(date) 
                            - survivalCurve.Get(nextDate);

                var ee = expectedExposure[i];

                acc += ee * df * dP;
            }

            return (1.0 - recoveryRate) * acc;
        }

        public static ICurve GetSurvivalProbabilityCurve(
            double recoveryRate,
            ICurve cdsCurve,
            ICurve discountCurve,
            DateTime[] resetRatesPlusFinalMaturity,
            double[] tau)
        {
            var survivalProbabilities =
                GetSurvivalProbabilities(
                    recoveryRate,
                    cdsCurve,
                    discountCurve,
                    resetRatesPlusFinalMaturity,
                    tau);

            var data = resetRatesPlusFinalMaturity.Zip(
                survivalProbabilities, (t, p) => new KeyValuePair<DateTime, double>(t, p))
                .ToDictionary(x => x.Key, x => x.Value);

            return new Curve<LinearInterpolator>(data);
        }

        private static double[] GetSurvivalProbabilities(
        double recoveryRate, 
        ICurve cdsCurve,
        ICurve discountCurve,
        DateTime[] resetRatesPlusFinalMaturity,
        double[] tau)
        {
            var L = 1 - recoveryRate;

            if (tau.Length != resetRatesPlusFinalMaturity.Length - 1)
                throw new ApplicationException(
                    "Tau must have one less element than resetDatesPlusFinalMaturity");
            
            var survivalProbabilities = new double[resetRatesPlusFinalMaturity.Length];
            
            survivalProbabilities[0] = 1.0;

            for (var n = 1; n < resetRatesPlusFinalMaturity.Length; n++)
            {
                var S_N = cdsCurve.Get(resetRatesPlusFinalMaturity[n]);

                var tauN = tau[n - 1];

                var acc = 0.0;

                for (var i = 1; i < n; i++)
                {
                    var DF_i = discountCurve.Get(resetRatesPlusFinalMaturity[i]);
                    var tau_i = tau[i - 1];
                    var P_T_i = survivalProbabilities[i];
                    var P_T_i_minus_1 = survivalProbabilities[i - 1];

                    acc += DF_i * (L * P_T_i_minus_1 - (L + tau_i * S_N) * P_T_i);
                }

                var DF_N = discountCurve.Get(resetRatesPlusFinalMaturity[n]);

                var denominator = DF_N * (L + tauN * S_N);

                var A = acc / denominator;

                var P_T_N_minus_1 = survivalProbabilities[n - 1];

                var B = (P_T_N_minus_1 * L) / (L + tauN * S_N);

                survivalProbabilities[n] = A + B;
            }

            return survivalProbabilities;
        }
    }
}