﻿using System;
using System.Linq;
using LiborOisVol.Core.Solver;
using LiborOisVol.Model.Quotes;

namespace LiborOisVol.Model
{
    public static class VolatilityFitting
    {
        public class ABCDObjectiveFunction
        {
            private readonly double[] _resetYearFractions;
            private readonly double[] _volatilities;

            public ABCDObjectiveFunction(double[] resetYearFractions, double[] volatilities)
            {
                _resetYearFractions = resetYearFractions;
                _volatilities = volatilities;
            }

            private static readonly double H = 1E-4;

            public static void Constrain(double[] p)
            {
                p[2] = Math.Max(p[2], H); // c > 0
                p[3] = Math.Max(p[3], H); // d > 0
                
                if (p[0] + p[3] <= 0)
                {
                    p[0] = Math.Max(p[0], H - p[3]);
                }
            }

            public static double[][] SeedGenerator(uint dimension)
            {
                if (dimension != 4)
                    throw new ApplicationException(
                        $"Can only use {nameof(SeedGenerator)} for 4D search");

                var seeds = new double[5][];

                var a = 0.1;
                var b = 0.05;
                var c = 0.1;
                var d = 0.01;

                for (var i = 0; i < 5; i++)
                {
                    var p = new double[4];

                    p[0] = a *= 2;
                    p[1] = b *= 2;
                    p[2] = c *= 2;
                    p[3] = d *= 2;

                    seeds[i] = p;
                }

                return seeds;
            }

            public double Evaluate(double[] p)
            {
                var model = new ABCDModel(p[0], p[1], p[2], p[3]);

                var totalError = 0.0;

                for (var i = 0; i < _resetYearFractions.Length; i++)
                {
                    var target = _volatilities[i];

                    var Tn = _resetYearFractions[i];
                    
                    var integral = model.Integral_Sigma_i_Sigma_j_Definite(0, Tn, Tn, Tn);

                    var modelResult = Math.Sqrt(integral / Tn);

                    var diff = target - modelResult;

                    totalError += diff * diff;
                }

                return totalError;
            }
        }

        public static ABCDModel FitVolatilityModel(PricingEnvironment env, CapletQuote[] caplets)
        {
            var cal = env.Parameters.Calendar;
            var dcc = env.Parameters.DCC;
            var baseDate = env.Parameters.PricingDate;

            var resetYearFractions = caplets.Select(
                                        caplet => cal.YearFraction(baseDate, caplet.ResetDate, dcc))
                                        .ToArray();

            var volatilities = caplets.Select(x => x.Volatility).ToArray();

            var objFun = new ABCDObjectiveFunction(resetYearFractions, volatilities);

            var solver = new NelderMead(4, objFun.Evaluate, ABCDObjectiveFunction.Constrain, 
                 seedGenerator: ABCDObjectiveFunction.SeedGenerator);
            
            solver.Solve(out var result, out var error);
            
            return new ABCDModel(result[0], result[1], result[2], result[3]);
        }

        public static double[] GetCorrectionFactors(
            PricingEnvironment env, CapletQuote[] caplets, ABCDModel model)
        {
            var k = new double[caplets.Length];

            var cal = env.Parameters.Calendar;
            var dcc = env.Parameters.DCC;
            var baseDate = env.Parameters.PricingDate;

            var T = caplets.Select(caplet => cal.YearFraction(baseDate, caplet.ResetDate, dcc)).ToArray();

            var volatilities = caplets.Select(x => x.Volatility).ToArray();

            for (var i=0; i < volatilities.Length; i++)
            {
                var target = volatilities[i] * volatilities[i] * T[i];
                var modelResult = model.Integral_Sigma_i_Sigma_j_Definite(0, T[i], T[i], T[i]);

                k[i] = Math.Sqrt(target / modelResult);
            }

            return k;
        }
    }
}