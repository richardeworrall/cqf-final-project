﻿using System;
using LiborOisVol.Core;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Model
{
    public class PricingEnvironment
    {
        public PricingParameters Parameters { get; set; }

        public ICurve DiscountCurve { get; set; }
    }

    public static class PricingEnvironmentExtensions
    {
        public static DateTime AddTenorAdjusted(this PricingEnvironment e, DateTime date, Tenor tenor)
            => CommonCalculations.AddTenorAdjusted(date, tenor, e.Parameters.Calendar, e.Parameters.BDC);

        public static DateTime ResetDateAt(this PricingEnvironment e, Tenor tenor)
            => e.Parameters.ResetDateAt(tenor);

        public static DateTime[] ResetDatesBetween(
            this PricingEnvironment e, Tenor tenorToStart, Tenor tenorStartToEnd)
            => e.Parameters.ResetDatesBetween(tenorToStart, tenorStartToEnd);

        public static double[] YearFractionsFromStartFor(
            this PricingEnvironment e, DateTime[] resetDates)
            => e.Parameters.YearFractionsFromStartFor(resetDates);

        public static double[] YearFractionsBetweenResetsFor(
            this PricingEnvironment e, DateTime[] resetDates)
            => e.Parameters.YearFractionsBetweenResetsFor(resetDates);

        public static double T(this PricingEnvironment e, DateTime date)
            => e.Parameters.T(date);

        public static double tau(
            this PricingEnvironment e, DateTime startDate, DateTime endDate)
            => e.Parameters.tau(startDate, endDate);
    }
}