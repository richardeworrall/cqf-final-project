﻿using LiborOisVol.Model.Instruments;
using System;

namespace LiborOisVol.Model
{
    public delegate IMarketModel MarketModelFactory(uint offset);

    public interface IMarketModel
    {
        void Simulate();
        double GetRate(DateTime asOf, DateTime resetDate);
        double NPV(IContract contract, DateTime? pricingDate = null);
    }
}