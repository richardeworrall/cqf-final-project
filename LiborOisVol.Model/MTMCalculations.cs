﻿using LiborOisVol.Core;

namespace LiborOisVol.Model
{
    public class InterestRateSwapMTMCalculator
    {
        private readonly double[] _tau;
        private readonly double _rf;
        private readonly double[] _discountSpread;


        public InterestRateSwapMTMCalculator(double[] tau, double rf, double[] discountSpread)
        {
            _tau = tau;
            _rf = rf;
            _discountSpread = discountSpread;
        }

        public double[] Calculate_MTM_TimeSeries(double[,] F)
        {
            int RepeatFirst(int x)
            {
                if (x == 0) return 0;
                return x - 1;
            }

            var mtm = new double[F.Height() + 1];

            for (var i = 0; i < F.Height(); i++)
            {
                var k = RepeatFirst(i);

                var discountFactors = new double[F.Width()+1];

                discountFactors[k] = 1.0;
                
                for (var j = k; j < F.Width(); j++)
                {
                    var discountRate = F[i, j] - _discountSpread[j];

                    discountFactors[j+1] = discountFactors[j] / (1 + discountRate * _tau[j]);
                }

                var fixedLegAcc = 0.0;
                var floatLegAcc = 0.0;

                for (var j = F.Width() - 1; j >= k; j--)
                {
                    fixedLegAcc += _rf * _tau[j] * discountFactors[j];
                    floatLegAcc += F[i, j] * _tau[j] * discountFactors[j];
                }

                mtm[i] = floatLegAcc - fixedLegAcc;
            }

            return mtm;
        }
    }
}