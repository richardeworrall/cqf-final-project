﻿using System;
using LiborOisVol.Core;
using LiborOisVol.Model.Instruments;

namespace LiborOisVol.Model
{
    public class LiborMarketModelWrapper : IMarketModel
    {
        private readonly LiborMarketModel _lmm;

        private readonly double[,] L;

        private readonly DateTime[] _asOfDates;
        private readonly DateTime[] _resetDates;
        private readonly DateTime _terminalDate;

        private readonly double[] _tau;

        private readonly PricingEnvironment _environment;

        public LiborMarketModelWrapper(LiborMarketModel lmm, PricingEnvironment environment, 
            DateTime[] resetDates, double[] tau, DateTime terminalDate)
        {
            _lmm = lmm;

            L = lmm.F;

            _tau = tau;

            _environment = environment;

            _asOfDates = resetDates.WithPrependedValue(environment.Parameters.PricingDate);
            _resetDates = resetDates;
            _terminalDate = terminalDate;
        }

        public void Simulate() => _lmm.Simulate();

        public double NPV(IContract contract, DateTime? pricingDate = null)
        {
            if (pricingDate == null) pricingDate = _environment.Parameters.PricingDate;

            var cashflows = contract.GetCashflows(_environment, this);

            var cashflowsAtMaturity = 0.0;

            foreach(var cashflow in cashflows)
            {
                var cashflowValue = cashflow.Amount;

                if (cashflow.Date < _terminalDate)
                {
                    var asOfIdx = Array.IndexOf(_asOfDates, cashflow.Date);
                    var resetIdx = Array.IndexOf(_resetDates, cashflow.Date);
                    
                    for (var i = resetIdx; i < _resetDates.Length; i++)
                        cashflowValue *= 1 + _tau[i] * L[asOfIdx, i];
                }

                cashflowsAtMaturity += cashflowValue;
            }

            var npv = cashflowsAtMaturity
                        * _environment.DiscountCurve.Get(_terminalDate)
                        / _environment.DiscountCurve.Get(pricingDate.Value);

            return npv;
        }

        public double GetRate(DateTime asOf, DateTime resetDate)
        {
            var asOfIdx = Array.IndexOf(_asOfDates, asOf);
            var resetIdx = Array.IndexOf(_resetDates, resetDate);

            if (asOfIdx < 0 || resetIdx < 0)
                throw new NotSupportedException(
                    $"{nameof(LiborMarketModelWrapper)} cannot interpolate evolved rates");

            return L[asOfIdx, resetIdx];
        }
    }
}
