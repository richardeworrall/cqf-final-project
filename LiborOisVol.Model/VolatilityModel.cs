﻿using System;

namespace LiborOisVol.Model
{
    public interface IVolatilityModel
    {
        double GetInstantaneousVolatility(double t, double Ti);

        double Integral_Sigma_i_Sigma_j_Definite(double tFrom, double tUntil, double Ti, double Tj);
    }
    
    public class ABCDModel : IVolatilityModel
    {
        private readonly double _a, _b, _c, _d;
        
        public ABCDModel(double a, double b, double c, double d)
        {
            if (!(a + d > 0 && c > 0 && d > 0))
                throw new ArgumentException("Invalid volatility parameters");
            
            _a = a; _b = b; _c = c; _d = d;
        }

        public double GetInstantaneousVolatility(double t, double Ti)
        {
            return (_a + _b * (Ti - t)) * Math.Exp(-1.0 * _c * (Ti - t)) + _d;
        }

        public double Integral_Sigma_i_Sigma_j_Definite(double tFrom, double tUntil, double Ti, double Tj)
        {
            return (Integral_Sigma_i_Sigma_j(tUntil, Ti, Tj) 
                - Integral_Sigma_i_Sigma_j(tFrom, Ti, Tj)) 
                / _c / _c / _c;
        }
        
        private double Integral_Sigma_i_Sigma_j(double t, double Ti, double Tj)
        {
            var e1 = _c * (t - Ti);
            var e2 = _c * (t - Tj);

            var e = e1 + e2;

            var ee1 = Math.Exp(e1);
            var ee2 = Math.Exp(e2);

            var ac = _a * _c;

            var cd = _c * _d;
            
            var x = ac * cd * (ee1 + ee2) + _c * cd * cd * t;

            var y = _b * cd * (ee1 * (e1 - 1.0) + ee2 * (e2 - 1.0));

            var z = Math.Exp(e) * (ac * (ac + _b * (1 - e)) + _b * _b * (0.5 * (1.0 - e) + e1 * e2)) / 2.0;

            return x - y + z;
        }
    }
}