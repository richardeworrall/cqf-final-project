﻿using LiborOisVol.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model.Calendar;
using LiborOisVol.Model.Instruments;

namespace LiborOisVol.Model
{
    public static class CommonCalculations
    {
        public static double ForwardRate(ICurve discountFactorCurve, DateTime startDate, DateTime endDate,
            ICalendar calendar, DayCountConvention dcc)
        {
            var delta = calendar.YearFraction(startDate, endDate, dcc);

            return ForwardRate(discountFactorCurve.Get(startDate), discountFactorCurve.Get(endDate), delta);
        }

        public static double ForwardRate(double startDiscountFactor, double endDiscountFactor, double delta)
        {
            return (startDiscountFactor / endDiscountFactor - 1.0) / delta;
        }

        public static double SwapRate(double[] T, double[] tau, double[] P, double[] f)
        {
            var A = 0.0;
            var B = 0.0;
            for (var i = 0; i < T.Length; i++)
            {
                A += P[i] * tau[i] * f[i];
                B += P[i] * tau[i];
            }

            var swapRate = A / B;

            return swapRate;
        }

        public static double[] BuildYearFractionVector(DateTime baseDate, DateTime[] dates,
            ICalendar calendar, DayCountConvention dcc)
        {
            var T = new double[dates.Length];

            for (var i = 0; i < dates.Length; i++)
            {
                T[i] = calendar.YearFraction(baseDate, dates[i], dcc);
            }

            return T;
        }

        public static double[] BuildTauVector(DateTime[] resetDates,
            ICalendar calendar, DayCountConvention dcc)
        {
            var tau = new double[resetDates.Length - 1];

            for (var i = 0; i < resetDates.Length - 1; i++)
            {
                tau[i] = calendar.YearFraction(resetDates[i], resetDates[i + 1], dcc);
            }

            return tau;
        }

        public static double[] BuildForwardRateVector(DateTime[] resetDates, ICurve pseudoDiscountCurve,
            ICalendar calendar, DayCountConvention dcc)
        {
            var F = new double[resetDates.Length - 1];

            for (var i = 0; i < resetDates.Length - 1; i++)
            {
                F[i] = ForwardRate(pseudoDiscountCurve,
                    resetDates[i], resetDates[i + 1], calendar, dcc);
            }

            return F;
        }

        public static ICurve BuildDiscountCurveFromLibors(Tenor liborTenor, DateTime baseDate, ICalendar calendar,
            BusinessDayConvention bdc, DayCountConvention dcc, Dictionary<DateTime, double> forwardLiborRates)
        {
            var sortedResetDates = forwardLiborRates
                .Select(x => x.Key)
                .OrderBy(x => x)
                .ToArray();

            var finalCalculableDFDate = calendar.GetBusinessDay(sortedResetDates.Last().Add(liborTenor), bdc);

            sortedResetDates = sortedResetDates.WithAppendedValue(finalCalculableDFDate);

            var sortedRates = forwardLiborRates
                .OrderBy(x => x.Key)
                .Select(x => x.Value)
                .ToArray();

            // Assume first known libor rate applies all the way back to the base date
            if (!sortedResetDates.Contains(baseDate))
            {
                sortedResetDates = sortedResetDates.WithPrependedValue(baseDate);
                sortedRates = sortedRates.WithPrependedValue(sortedRates[0]);
            }

            var yearFractions = BuildTauVector(sortedResetDates, calendar, dcc);

            Dictionary<DateTime, double> discountFactors = new Dictionary<DateTime, double> {{baseDate, 1.0}};

            var currentDF = 1.0;
            for (var i = 0; i < sortedRates.Length; i++)
            {
                currentDF = currentDF * 1.0 / (1.0 + yearFractions[i] * sortedRates[i]);
                discountFactors.Add(sortedResetDates[i + 1], currentDF);
            }

            return new Curve<MonotoneCubicInterpolator>(discountFactors);
        }

        public static ICurve BuildDiscountCurveFromLibors(Tenor liborTenor, DateTime baseDate, ICalendar calendar,
            BusinessDayConvention bdc, DayCountConvention dcc, Dictionary<Tenor, double> forwardLiborRates)
        {
            var sortedResetDates = forwardLiborRates
                .Select(x => AddTenorAdjusted(baseDate, x.Key, calendar, bdc))
                .OrderBy(x => x)
                .ToArray();

            var finalCalculableDFDate = calendar.GetBusinessDay(sortedResetDates.Last().Add(liborTenor), bdc);

            sortedResetDates = sortedResetDates.WithAppendedValue(finalCalculableDFDate);

            var sortedRates = forwardLiborRates
                .OrderBy(x => AddTenorAdjusted(baseDate, x.Key, calendar, bdc))
                .Select(x => x.Value)
                .ToArray();

            // Assume first known libor rate applies all the way back to the base date
            if (!sortedResetDates.Contains(baseDate))
            {
                sortedResetDates = sortedResetDates.WithPrependedValue(baseDate);
                sortedRates = sortedRates.WithPrependedValue(sortedRates[0]);
            }

            var yearFractions = BuildTauVector(sortedResetDates, calendar, dcc);

            Dictionary<DateTime, double> discountFactors = new Dictionary<DateTime, double> {{baseDate, 1.0}};

            var currentDF = 1.0;
            for (var i = 0; i < sortedRates.Length; i++)
            {
                currentDF = currentDF * 1.0 / (1.0 + yearFractions[i] * sortedRates[i]);
                discountFactors.Add(sortedResetDates[i + 1], currentDF);
            }

            return new Curve<MonotoneCubicInterpolator>(discountFactors);
        }

        public static double[] BuildDiscountFactorVector(DateTime[] resetDates, ICurve discountCurve)
        {
            var P = new double[resetDates.Length - 1];

            for (var i = 0; i < resetDates.Length - 1; i++)
            {
                P[i] = discountCurve.Get(resetDates[i + 1]);
            }

            return P;
        }

        public static DateTime AddTenorAdjusted(DateTime baseDate, Tenor tenor,
            ICalendar calendar, BusinessDayConvention bdc)
        {
            return calendar.GetBusinessDay(baseDate.Add(tenor), bdc);
        }

        public static DateTime[] BuildResetDateArray(DateTime baseDate, Tenor tenorToStart, Tenor tenorStartToEnd,
            Tenor liborTenor, ICalendar calendar, BusinessDayConvention bdc)
        {
            var liborStartDateUnadjusted = baseDate.Add(tenorToStart);
            var liborEndDateUnadjusted = liborStartDateUnadjusted.Add(tenorStartToEnd);

            return BuildResetDateArray(liborStartDateUnadjusted, liborEndDateUnadjusted, liborTenor, calendar, bdc);
        }

        public static DateTime[] BuildResetDateArray(DateTime startDateUnadjusted, DateTime endDateUnadjusted,
            Tenor tenor, ICalendar calendar, BusinessDayConvention bdc)
        {
            List<DateTime> resetDates = new List<DateTime>();

            var date = startDateUnadjusted;

            do
            {
                resetDates.Add(calendar.GetBusinessDay(date, bdc));
                date = date.Add(tenor);
            } while (date <= endDateUnadjusted);

            return resetDates.ToArray();
        }
    }
}