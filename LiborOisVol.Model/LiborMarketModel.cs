﻿using System;
using LiborOisVol.Core;

namespace LiborOisVol.Model
{
    public class LiborMarketModel
    {
        private readonly double[][,] _c;
        private readonly double[][,] _a;

        private readonly double[] _f0;

        private readonly double[] _tau;

        private readonly double[] _currentDraw;
        private readonly double[][] _uncorrelatedDraws;
        private readonly double[][] _correlatedDraws;

        private readonly IUniformRandom _drawProvider;
        
        private readonly double[,] _f;

        public double[,] F => _f;

        private readonly int N;

        private readonly double[][] _rootCjj;

        public LiborMarketModel(double[][,] C, double[][,] A,
            IUniformRandom uniformRandom,
            double[] tau, double[] F0)
        {
            N = F0.Length;

            #region Validation and Bounds Checking

            if (N != tau.Length) throw new ArgumentException(
                $"FO has length {N} but tau has length {tau.Length} (they should be equal)");

            if (N != C.Length) throw new ArgumentException(
                $"FO has length {N} but C has length {C.Length} (they should be equal)");

            for (var j = 0; j < N; j++)
            {
                var nextLength = N - j;

                if (C[j].Height() != nextLength) throw new ArgumentException(
                    $"C[{j}] has height {C[j].Height()} but it should be {nextLength}");

                if (C[j].Width() != nextLength) throw new ArgumentException(
                    $"C[{j}] has width {C[j].Width()} but it should be {nextLength}");
            }

            if (N != A.Length) throw new ArgumentException(
                $"FO has length {N} but A has length {A.Length} (they should be equal)");

            for (var j = 0; j < N; j++)
            {
                var nextLength = N - j;

                if (A[j].Height() != nextLength) throw new ArgumentException(
                    $"A[{j}] has height {A[j].Height()} but it should be {nextLength}");

                if (A[j].Width() != nextLength) throw new ArgumentException(
                    $"A[{j}] has width {A[j].Width()} but it should be {nextLength}");
            }

            var requiredDrawLength = (N * (N + 1)) / 2;

            if (uniformRandom.CurrentDraw.Length != requiredDrawLength)
                throw new ArgumentException(
                    $"F0 has length {N}, so {requiredDrawLength} uniformRandom draws are required" +
                    $"to fully evolve the LIBOR curve, but the draw array has length {_currentDraw.Length}");

            #endregion Validation and Bounds Checking

            _tau = tau;

            _c = C;
            _a = A;

            _f0 = F0;

            _drawProvider = uniformRandom;
            _currentDraw = _drawProvider.CurrentDraw;

            _correlatedDraws = new double[N][];
            _uncorrelatedDraws = new double[N][];

            for (var i = 0; i < N; i++)
            {
                _uncorrelatedDraws[i] = new double[N - i];
                _correlatedDraws[i] = new double[N - i];
            }

            _f = new double[N + 1, N];

            for (var i = 0; i < N; i++)
            {
                _f[0, i] = _f0[i];
            }

            _rootCjj = new double[N][];

            for (var t = 0; t < N; t++)
            {
                _rootCjj[t] = new double[N - t];

                for (var j = t; j < N; j++)
                {
                    var cjj = _c[t][j - t, j - t];

                    var rootCjj = Math.Sqrt(cjj);

                    _rootCjj[t][j - t] = rootCjj;
                }
            }
        }

        public void Simulate()
        {
            _drawProvider.DrawNext();

            var counter = 0;
            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < N - i; j++)
                {
                    _uncorrelatedDraws[i][j] = NormalDistribution.InverseCDF(_currentDraw[counter++]);
                }

                MatrixHelper.HalfMultiply(_a[i], _uncorrelatedDraws[i], _correlatedDraws[i]);
            }
            
            for (var t = 0; t < N; t++) // Simulation timestep
            {
                // Predictor Step
                for(var j = t; j < N; j++) // LIBOR rate
                {
                    var cjj = _c[t][j - t, j - t];

                    var diffusion = _correlatedDraws[t][j - t] * _rootCjj[t][j - t];

                    var drift = -0.5 * cjj;

                    for (var k = j + 1; k < N; k++)
                    {
                        var cjk = _c[t][j-t, k-t];
                        drift -= (1.0 - 1.0 / (1.0 + _tau[k] * _f[t, k])) * cjk;
                    }

                    _f[t + 1, j] = _f[t, j] * Math.Exp(drift + diffusion);
                }

                // Corrector Step
                for (var j = t; j < N; j++) // LIBOR rate
                {
                    var cjj = _c[t][j - t, j - t];

                    var diffusion = _correlatedDraws[t][j - t] * _rootCjj[t][j - t];

                    var drift = -0.5 * cjj;

                    for (var k = j + 1; k < N; k++)
                    {
                        var cjk = _c[t][j-t, k-t];
                        drift -= (1.0 - 1.0 / (1.0 + _tau[k] * 0.5 * (_f[t, k] + _f[t+1, k]))) * cjk;
                    }

                    _f[t + 1, j] = _f[t, j] * Math.Exp(drift + diffusion);
                }
            }
        }
    }
}