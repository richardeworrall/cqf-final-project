﻿using System;
using System.Collections.Generic;
using System.Linq;
using static LiborOisVol.Model.CommonCalculations;

namespace LiborOisVol.Model.Instruments
{
    public class EuropeanSwaption : Swaption
    {
        public EuropeanSwaption(PricingParameters e,
            DateTime expiryDate, DateTime maturityDate, double strike)
            : base(e, expiryDate, maturityDate, strike)
        { }

        public override string Description => "European Swaption";

        public override IEnumerable<Cashflow> GetCashflows(
            PricingEnvironment e, IMarketModel libor)
        {
            var P = BuildDiscountFactorVector(_resetDates, e.DiscountCurve);
            var F = _resetDates.Select(d => libor.GetRate(expiryDate, d)).ToArray();
            var tau = BuildTauVector(_resetDates, e.Parameters.Calendar, e.Parameters.DCC);
            var T = BuildYearFractionVector(
                e.Parameters.PricingDate, _resetDates, e.Parameters.Calendar, e.Parameters.DCC);

            var payoff = Math.Max(SwapRate(T, tau, P, F) - _strike, 0.0);

            return new[] { new Cashflow { Amount = payoff, Date = expiryDate } };
        }
    }
}