﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Model.Instruments
{
    public class Cap : IContract
    {
        private readonly List<Caplet> _caplets;

        public string Description => "Cap";

        public Cap(PricingParameters e, DateTime startDate, DateTime maturityDate, double strike)
        {
            _caplets = GenerateCaplets(e, startDate, maturityDate, strike);
        }

        public IEnumerable<Cashflow> GetCashflows(PricingEnvironment e, IMarketModel libor)
        {
            return _caplets.SelectMany(x => x.GetCashflows(e, libor));
        }

        private List<Caplet> GenerateCaplets(PricingParameters e,
            DateTime startDate, DateTime maturityDate, double strike)
        {
            var caplets = new List<Caplet>();

            var resetDates = CommonCalculations.BuildResetDateArray(startDate, maturityDate, 
                e.LiborTenor, e.Calendar, e.BDC);

            for (var i=0; i < resetDates.Length - 1; i++)
            {
                caplets.Add(new Caplet(resetDates[i], resetDates[i + 1], e.LiborTenor, strike));
            }

            return caplets;
        }
    }
}