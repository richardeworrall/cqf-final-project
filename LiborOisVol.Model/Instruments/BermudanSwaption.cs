﻿using System;
using System.Collections.Generic;

namespace LiborOisVol.Model.Instruments
{
    public class BermudanSwaption : Swaption
    {
        public BermudanSwaption(PricingParameters e,
            DateTime expiryDate, DateTime maturityDate, double strike)
            : base(e, expiryDate, maturityDate, strike)
        { }

        public override string Description => "Bermudan Swaption";

        public override IEnumerable<Cashflow> GetCashflows(
            PricingEnvironment e, IMarketModel libor)
        {
            throw new NotImplementedException();
        }
    }
}