﻿using System;
using System.Collections.Generic;

namespace LiborOisVol.Model.Instruments
{
    public struct Cashflow
    {
        public DateTime Date;

        public double Amount;
    }

    public interface IContract
    {
        string Description { get; }
        IEnumerable<Cashflow> GetCashflows(PricingEnvironment e, IMarketModel libor);
    }
}