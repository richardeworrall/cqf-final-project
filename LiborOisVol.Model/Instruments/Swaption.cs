﻿using System;
using static LiborOisVol.Model.CommonCalculations;
using System.Collections.Generic;

namespace LiborOisVol.Model.Instruments
{
    public abstract class Swaption : IContract
    {
        protected DateTime[] _resetDates;
        protected double _strike;
        protected DateTime expiryDate;
        protected DateTime maturityDate;

        public abstract string Description { get; }

        public Swaption(PricingParameters e, 
            DateTime expiryDate, DateTime maturityDate, double strike)
        {
            this.expiryDate = expiryDate;
            this.maturityDate = maturityDate;

            _resetDates = BuildResetDateArray(expiryDate, maturityDate,
                e.LiborTenor, e.Calendar, e.BDC);
            
            _strike = strike;
        }

        public abstract IEnumerable<Cashflow> GetCashflows(
            PricingEnvironment e, IMarketModel libor);
    }
}