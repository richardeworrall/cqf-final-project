﻿using System;
using System.Collections.Generic;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Model.Instruments
{
    public class Caplet : IContract
    {
        private readonly DateTime _resetDate;
        private readonly DateTime _maturityDate;
        private readonly double _strike;

        public string Description => $"Caplet Reset={_resetDate:yyyy-MM-dd} Maturity={_maturityDate:yyyy-MM-dd} K={_strike:0.00000000}";

        public Caplet(DateTime resetDate, DateTime maturityDate, Tenor tenor, double strike)
        {
            _resetDate = resetDate;
            _maturityDate = maturityDate;
            _strike = strike;
        }

        public IEnumerable<Cashflow> GetCashflows(PricingEnvironment e, IMarketModel libor)
        {
            var forwardRate = libor.GetRate(_resetDate, _resetDate);
            
            var tau = e.Parameters.Calendar.YearFraction(
                _resetDate, _maturityDate, e.Parameters.DCC);

            var payoffAtMaturity = Math.Max(forwardRate - _strike, 0) * tau;

            return new[] {new Cashflow { Amount = payoffAtMaturity, Date = _maturityDate }};
        }
    }
}