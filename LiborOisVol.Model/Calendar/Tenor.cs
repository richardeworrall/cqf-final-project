﻿using System;
using System.Text.RegularExpressions;

namespace LiborOisVol.Model.Calendar
{
    public enum Period
    {
        Day,
        Week,
        Month,
        Year
    }

    public class Tenor
    {
        public Period Period { get; }
        public int NumPeriods { get; }

        private Tenor(int numPeriods, Period period)
        {
            Period = period;
            NumPeriods = numPeriods;
        }

        private static Tenor GetYearsAndMonthsTenor(string tenor)
        {
            var result = Regex.Match(tenor, @"(?<YEARS>\d+)Y (?<MONTHS>\d+)M", RegexOptions.IgnoreCase);

            var nYears = int.Parse(result.Groups["YEARS"].Value);

            var nMonths = int.Parse(result.Groups["MONTHS"].Value);

            return Get(nMonths + 12 * nYears, Period.Month);
        }

        public static Tenor Get(string tenor)
        {
            tenor = tenor.Trim();

            if (Regex.IsMatch(tenor, @"\d+Y \d+M", RegexOptions.IgnoreCase))
            {
                return GetYearsAndMonthsTenor(tenor);
            }

            var period = ParsePeriod(tenor);
            var numPeriods = ParseNumPeriods(tenor);
            return Get(numPeriods, period);
        }

        public static Tenor Get(int numPeriods, Period period)
        {
            return new Tenor(numPeriods, period);
        }

        private static Period ParsePeriod(string tenor)
        {
            var period = Regex.Match(tenor, @"[a-zA-Z]$", RegexOptions.IgnoreCase);
            
            if (!period.Success)
            {
                throw new ApplicationException($"Could not convert {tenor} to a Tenor");
            }

            switch (period.Value)
            {
                case "D":
                case "d":
                    return Period.Day;

                case "W":
                case "w":
                    return Period.Week;

                case "M":
                    return Period.Month;

                case "Y":
                case "y":
                    return Period.Year;

                default:
                    throw new ApplicationException($"Could not convert {tenor} to a Tenor");
            }
        }

        private static int ParseNumPeriods(string tenor)
        {
            var numPeriods = Regex.Match(tenor, @"^\d*", RegexOptions.IgnoreCase);
            if (!numPeriods.Success)
            {
                throw new ApplicationException($"Could not convert {tenor} to a Tenor");
            }

            if (!int.TryParse(numPeriods.Value, out var pLength))
            {
                throw new ApplicationException($"Could not convert {tenor} to a Tenor");
            }

            return pLength;
        }

        public override string ToString()
        {
            return $"{NumPeriods}{Period.ToString().Substring(0, 1)}";
        }
        
        public static implicit operator Tenor(string s)
        {
            return Get(s);
        }
    }

    public static class TenorExtensions
    {
        public static DateTime Add(this DateTime startDate, Tenor tenor)
        {
            switch (tenor.Period)
            {
                case Period.Day:
                    return startDate.Date.AddDays(tenor.NumPeriods);

                case Period.Week:
                    return startDate.Date.AddDays(tenor.NumPeriods * 7);

                case Period.Month:
                    return startDate.Date.AddMonths(tenor.NumPeriods);

                case Period.Year:
                    return startDate.Date.AddYears(tenor.NumPeriods);

                default:
                    throw new NotSupportedException(
                        "Tenor.AddDate only support days, months and year tenors");
            }
        }
    }
}