﻿using System;

namespace LiborOisVol.Model.Calendar
{
    public enum DayCountConvention
    {
        ActualActual,
        Actual360,
        Actual365
    }

    public interface IDayCountConvention
    {
        double YearsBetweenDates(DateTime date1, DateTime date2);
    }

    public class ActualActual : IDayCountConvention
    {
        public double YearsBetweenDates(DateTime date1, DateTime date2)
        {
            double yearsBetweenDates;
            
            if (date1.Year == date2.Year)
            {
                double daysInTheYear = Calendar.IsLeapYear(date1.Year) ? 366 : 365;
                yearsBetweenDates = Math.Abs(date2.DayOfYear - date1.DayOfYear) / daysInTheYear;
                return yearsBetweenDates;
            }

            DateTime startDate, endDate;
            if (date1 < date2)
            {
                startDate = date1;
                endDate = date2;
            }
            else
            {
                startDate = date2;
                endDate = date1;
            }

            var numberOfYearsBetween = Math.Abs(endDate.Year - startDate.Year) - 1;

            double startDaysInYear = Calendar.IsLeapYear(startDate.Year) ? 366 : 365;
            double endDaysInYear = Calendar.IsLeapYear(endDate.Year) ? 366 : 365;

            yearsBetweenDates = numberOfYearsBetween 
                                + (startDaysInYear - startDate.DayOfYear) / startDaysInYear 
                                + endDate.DayOfYear / endDaysInYear;

            return yearsBetweenDates;
        }
    }

    public class Actual360 : IDayCountConvention
    {
        public double YearsBetweenDates(DateTime startDate, DateTime endDate)
        {
            var yearsBetweenDates = (double)Math.Abs((endDate - startDate).Days) / 360;

            return yearsBetweenDates;
        }
    }

    public class Actual365 : IDayCountConvention
    {
        public double YearsBetweenDates(DateTime date1, DateTime date2)
        {
            double yearsBetweenDates;

            if (date1.Year == date2.Year)
            {
                const double daysInTheYear = 365;
                yearsBetweenDates = Math.Abs(date2.DayOfYear - date1.DayOfYear) / daysInTheYear;
                return yearsBetweenDates;
            }

            DateTime startDate, endDate;
            if (date1 < date2)
            {
                startDate = date1;
                endDate = date2;
            }
            else
            {
                startDate = date2;
                endDate = date1;
            }

            var numberOfYearsBetween = Math.Abs(endDate.Year - startDate.Year) - 1;

            double startDaysInYear = Calendar.DaysInYear(startDate.Year);
            double endDaysInYear = Calendar.DaysInYear(endDate.Year);

            yearsBetweenDates = numberOfYearsBetween
                                + (startDaysInYear - startDate.DayOfYear) / startDaysInYear
                                + endDate.DayOfYear / endDaysInYear;

            return yearsBetweenDates;
        }
    }
}