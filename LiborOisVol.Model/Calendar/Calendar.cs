﻿using System;

namespace LiborOisVol.Model.Calendar
{
    public enum BusinessDayConvention
    {
        Unadjusted,
        Following,
        Preceeding,
        ModifiedFollowing,
        ModifiedPreceeding
    }

    public interface ICalendar
    {
        DateTime GetBusinessDay(DateTime startDate, BusinessDayConvention bdc);

        double YearFraction(DateTime startDate, DateTime endDate, DayCountConvention dcc);
    }

    public abstract class Calendar : ICalendar
    {
        protected abstract bool IsBusinessDay(DateTime date);

        private DateTime GetNextBusinessDay(DateTime date)
        {
            var candidateDate = date.AddDays(1);
            var i = 0;
            while (!IsBusinessDay(candidateDate))
            {
                candidateDate = candidateDate.AddDays(1);
                i++;
                if (i > 30) throw new ApplicationException(
                    "There are more than 30 consecutive non-business days in this calendar. " +
                    "Something has gone wrong!");
            }
            return candidateDate;
        }

        private DateTime GetPreviousBusinessDay(DateTime date)
        {
            var candidateDate = date.AddDays(-1);
            var i = 0;
            while (!IsBusinessDay(candidateDate))
            {
                candidateDate = candidateDate.AddDays(-1);
                i++;
                if (i > 30) throw new ApplicationException(
                    "There are more than 30 consecutive non-business days in this calendar. " +
                    "Something has gone wrong!");
            }
            return candidateDate;
        }

        public DateTime GetBusinessDay(DateTime startDate, BusinessDayConvention bdc)
        {
            if (IsBusinessDay(startDate)) return startDate;

            DateTime candidateDate;
            switch (bdc)
            {
                case BusinessDayConvention.Following:
                    return GetNextBusinessDay(startDate);

                case BusinessDayConvention.Preceeding:
                    return GetPreviousBusinessDay(startDate);

                case BusinessDayConvention.ModifiedFollowing:

                    candidateDate = GetNextBusinessDay(startDate);
                    
                    if (candidateDate.Month == startDate.Month) return candidateDate;

                    var i = 0;
                    while (candidateDate.Month != startDate.Month)
                    {
                        candidateDate = GetPreviousBusinessDay(candidateDate);
                        i++;
                        if (i > 30) throw new NotSupportedException(
                            "Tenor.AddDate only support days, months, quarters and year tenors");
                    }

                    return candidateDate;

                case BusinessDayConvention.ModifiedPreceeding:

                    candidateDate = GetPreviousBusinessDay(startDate);
                    
                    if (candidateDate.Month == startDate.Month) return candidateDate;
                    
                    var i2 = 0;
                    while (candidateDate.Month != startDate.Month)
                    {
                        candidateDate = GetNextBusinessDay(candidateDate);
                        i2++;
                        if (i2 > 30) throw new NotSupportedException(
                            "Tenor.AddDate only support days, months, quarters and year tenors");
                    }

                    return candidateDate;

                case BusinessDayConvention.Unadjusted:
                default:
                    return startDate;
            }
        }

        public static bool IsLeapYear(int year)
        {
            if (year < 1) throw new ArgumentException(
                "Negative years are not supported in Leap Year determination.");

            if (year % 4 != 0) return false;

            if (year % 100 != 0) return true;

            return year % 400 == 0;
        }

        public static int DaysInYear(int year)
        {
            return IsLeapYear(year) ? 366 : 365;
        }

        public double YearFraction(DateTime startDate, DateTime endDate, DayCountConvention dcc)
        {
            IDayCountConvention idcc;
            switch (dcc)
            {
                case DayCountConvention.ActualActual:
                    idcc = new ActualActual();
                    break;
                case DayCountConvention.Actual360:
                    idcc = new Actual360();
                    break;
                case DayCountConvention.Actual365:
                    idcc = new Actual365();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(dcc), dcc, "Must provide a day count convention");
            }

            return idcc.YearsBetweenDates(startDate, endDate);
        }
    }
}