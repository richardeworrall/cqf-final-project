﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Model.Calendar
{
    public class LondonCalendar : Calendar
    {
        // See https://en.wikipedia.org/wiki/Public_holidays_in_the_United_Kingdom
        
        private readonly HashSet<DateTime> _specialHolidays = new HashSet<DateTime>
        {
            new DateTime(2012,06,05),   // Diamond Jubilee of Elizabeth II
            new DateTime(2012,06,04),   // Diamond Jubilee of Elizabeth II (Bank Holiday Moved)
            new DateTime(2011,04,29),   // Royal Wedding of Prince William and Catherine Middleton
            new DateTime(2002,06,03),   // Golden Jubilee of Elizabeth II
            new DateTime(2002,06,04),   // Golden Jubilee of Elizabeth II (Bank Holiday Moved)
            new DateTime(1999,12,31),   // Millenium Celebration
            new DateTime(1995,05,08),   // 50th anniversary of VE Day (Bank Holiday Moved)
            new DateTime(1981,07,29),   // Royal Wedding of Prince Charles and Lady Diana Spencer
            new DateTime(1977,06,07)    // Silver Jubilee of Elizabeth II
        };

        private readonly HashSet<DateTime> _specialNotHolidays = new HashSet<DateTime>
        {
            new DateTime(2012,05,28),   // Diamond Jubilee of Elizabeth II (Bank Holiday Moved)
            new DateTime(2002,05,27),   // Golden Jubilee of Elizabeth II (Bank Holiday Moved)
            new DateTime(1995,05,01)    // 50th anniversary of VE Day (Bank Holiday Moved)
        };

        protected override bool IsBusinessDay(DateTime date)
        {
            if (!IsWeekday(date)) return false;

            if (_specialNotHolidays.Any(x => date.Date == x.Date)) return true;
            if (_specialHolidays.Any(x => date.Date == x.Date)) return false;

            if (IsNewYearsDayHoliday(date)) return false;
            if (IsGoodFridayBankHoliday(date)) return false;
            if (IsEasterMondayBankHoliday(date)) return false;
            if (IsMayBankHolidayMonday(date)) return false;
            if (IsSpringBankHolidayMonday(date)) return false;
            if (IsAugustBankHolidayMonday(date)) return false;
            if (IsChristmasDayHoliday(date)) return false;
            if (IsBoxingDayHoliday(date)) return false;

            return true;
        }

        private static bool IsWeekday(DateTime dateTime)
        {
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    return false;
                default:
                    return true;
            }
        }

        private static DateTime GetEasterSunday(int year)
        {
            // See http://stackoverflow.com/a/2510411/21574

            var g = year % 19;
            var c = year / 100;
            var h = (c - c / 4 - (8 * c + 13) / 25 + 19 * g + 15) % 30;
            var i = h - h / 28 * (1 - h / 28 * (29 / (h + 1)) * ((21 - g) / 11));

            var day = i - (year + year / 4 + i + 2 - c + c / 4) % 7 + 28;
            var month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }

            return new DateTime(year, month, day);
        }

        private static bool IsNewYearsDayHoliday(DateTime dateTime)
        {
            var firstOfJanuary = new DateTime(dateTime.Year, 1, 1);

            DateTime newYearsDay;
            switch (firstOfJanuary.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    newYearsDay = firstOfJanuary.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    newYearsDay = firstOfJanuary.AddDays(1);
                    break;
                default:
                    newYearsDay = firstOfJanuary;
                    break;
            }

            return dateTime.Date == newYearsDay.Date;
        }

        private static bool IsGoodFridayBankHoliday(DateTime dateTime)
        {
            var easterSunday = GetEasterSunday(dateTime.Year);
            return easterSunday.AddDays(-2).Date == dateTime.Date;
        }

        private static bool IsEasterMondayBankHoliday(DateTime dateTime)
        {
            var easterSunday = GetEasterSunday(dateTime.Year);
            return easterSunday.AddDays(1).Date == dateTime.Date;
        }

        private static bool IsMayBankHolidayMonday(DateTime dateTime)
        {
            var firstDayInMay = new DateTime(dateTime.Year, 05, 01);
            var mayBankHoliday = FindNextMondayOrReturnThisIfMonday(firstDayInMay);
            return dateTime.Date == mayBankHoliday.Date;
        }

        private static bool IsSpringBankHolidayMonday(DateTime dateTime)
        {
            var lastDayInMay = new DateTime(dateTime.Year, 05, 31);

            var springBankHoliday = FindPreviousMondayOrReturnThisIfMonday(lastDayInMay);

            return dateTime.Date == springBankHoliday.Date;
        }

        private static bool IsAugustBankHolidayMonday(DateTime dateTime)
        {
            var lastDayInAugust = new DateTime(dateTime.Year, 08, 31);
            var augustBankHoliday = FindPreviousMondayOrReturnThisIfMonday(lastDayInAugust);
            return dateTime.Date == augustBankHoliday.Date;
        }

        private static bool IsChristmasDayHoliday(DateTime dateTime)
        {
            var christmasDay = new DateTime(dateTime.Year, 12, 25);

            DateTime christmasDayHolidayDay;
            switch (christmasDay.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    christmasDayHolidayDay = christmasDay.AddDays(2);
                    break;
                default:
                    christmasDayHolidayDay = christmasDay;
                    break;
            }

            return dateTime.Date == christmasDayHolidayDay.Date;
        }

        private static bool IsBoxingDayHoliday(DateTime dateTime)
        {
            var boxingDay = new DateTime(dateTime.Year, 12, 26);

            DateTime boxingDayHolidayDay;
            switch (boxingDay.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    boxingDayHolidayDay = boxingDay.AddDays(2);
                    break;
                default:
                    boxingDayHolidayDay = boxingDay;
                    break;
            }

            return dateTime.Date == boxingDayHolidayDay.Date;
        }

        private static DateTime FindNextMondayOrReturnThisIfMonday(DateTime dateTime)
        {
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return dateTime;
                case DayOfWeek.Sunday:
                    return dateTime.AddDays(1);
                default:
                    var offset = 8 - (int)dateTime.DayOfWeek;
                    return dateTime.AddDays(offset);
            }
        }

        private static DateTime FindPreviousMondayOrReturnThisIfMonday(DateTime dateTime)
        {
            switch (dateTime.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return dateTime;
                case DayOfWeek.Sunday:
                    return dateTime.AddDays(-6);
                default:
                    var offset = -1 * ((int)dateTime.DayOfWeek - 1);
                    return dateTime.AddDays(offset);
            }
        }
    }
}