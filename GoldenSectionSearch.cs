using System;
using System.Collections.Generic;

// adapted from:
// https://medium.com/datadriveninvestor/golden-section-search-method-peak-index-in-a-mountain-array-leetcode-852-a00f53ed4076

namespace GoldenSectionSearchExperiments
{
    static class GoldenSectionSearch
    {
        private static double InvPhi = 2.0 / (1.0 + Math.Sqrt(5.0));
        
        private static int FindMaxElementIdx(int minIdx, int maxIdx, IReadOnlyList<double> arr)
        {
            if (maxIdx == minIdx) return minIdx;
            if (maxIdx - minIdx == 1) return arr[minIdx] > arr[maxIdx] ? minIdx :maxIdx;
            if (maxIdx - minIdx == 2)
                return arr[maxIdx] > arr[minIdx]
                    ? arr[maxIdx] > arr[minIdx + 1] ? maxIdx : minIdx + 1
                    : arr[minIdx] > arr[minIdx + 1] ? minIdx : minIdx + 1;
            
            minIdx -= 1;
            maxIdx += 1;
            
            int calc_x1(int left, int right)
                => right - (int) Math.Round((right - left) * InvPhi);
            
            int calc_x2(int left, int right)
                => left + (int) Math.Round((right - left) * InvPhi);
            
            var l = minIdx;
            var r = maxIdx;

            var x1 = calc_x1(l, r);
            var x2 = calc_x2(l, r);

            while (x1 < x2)
            {
                if (arr[x1] < arr[x2])
                {
                    l = x1;
                    x1 = x2;
                    x2 = calc_x1(x1, r);
                }
                else
                {
                    r = x2;
                    x2 = x1;
                    x1 = calc_x2(l, x2);
                }
            }

            return x1;
        }
    }
}