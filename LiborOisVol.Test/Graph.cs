﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Windows.Forms;

namespace LiborOisVol.ConsoleUI
{
    public partial class Graph : Form
    {
        public Graph(Func<double, double> f, double xMin, double xMax, int nSteps = 10000)
        {
            InitializeComponent();
            var myModel = new PlotModel { Title = "Example 1" };
            //var s = new LineSeries();
            //s.InterpolationAlgorithm = new CanonicalSpline(0.5);
            //s.Points.Add(new DataPoint(1.0, 10.0));
            //s.Points.Add(new DataPoint(2.0, 30.0));
            //s.Points.Add(new DataPoint(3.0, 35.0));
            //s.Points.Add(new DataPoint(4.0, 19.0));
            //s.Points.Add(new DataPoint(5.0, 14.0));
            var s = new FunctionSeries(f, xMin, xMax, nSteps);
            s.Background = OxyColor.FromRgb(255, 255, 255);
            s.Color = OxyColor.FromRgb(0, 0, 255);
            myModel.Series.Add(s);
            this.plot1.Model = myModel;
        }

        public Graph(PlotModel model)
        {
            InitializeComponent();
            this.plot1.Model = model;
        }
    }
}
