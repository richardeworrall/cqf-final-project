﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiborOisVol.Model;
using System.Linq;
using LiborOisVol.Core.Sobol;
using LiborOisVol.Core;
using LiborOisVol.Model.Instruments;
using LiborOisVol.Simulation;
using static LiborOisVol.Test.TestData.TestEnvironments;

namespace LiborOisVol.Test
{
    [TestClass]
    public class LiborMarketModelTests
    {
        [TestMethod]
        public void TestLMMCapletPricing()
        {
            var env = LMMIPEnvironment;

            var capletQuotes = CapletBuilder
                .Build(LMMIPEnvironment, LMMIPCapVolCurve, LMMIPDiscountCurve)
                .Take(20)
                .ToArray();
            
            var TT = env.YearFractionsFromStartFor(capletQuotes.Select(c => c.ResetDate).ToArray());

            var volatilityModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            var correlationModel = new ABCDModelCorrelation();

            var kk = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, volatilityModel);

            var CC = MatrixBuilder.BuildCovarianceMatrices(
                TT, volatilityModel, correlationModel, kk);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var nDraws = TT.Length * (TT.Length + 1) / 2;

            var random = SobolFactory.Build((uint) nDraws, "Sobol/sobol-direction-numbers.dat");
            
            var terminalDate = capletQuotes.Last().MaturityDate;

            var resetsPlusFinalMaturity = capletQuotes.Select(x => x.ResetDate)
                                .ToArray().WithAppendedValue(terminalDate);

            var f0 = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var lmm = new LiborMarketModel(CC, AA, random, tau, f0);

            var capletContracts = capletQuotes.Select(
                (c, idx) => new Caplet(
                    c.ResetDate, c.MaturityDate, env.Parameters.LiborTenor, f0[idx]))
                    .ToArray();

            var lmmWrapper = new LiborMarketModelWrapper(
                lmm, env, capletQuotes.Select(x => x.ResetDate).ToArray(), tau, terminalDate);

            var npvTotals = capletContracts
                .Select(c => new MeanVarianceTracker(c.Description)).ToArray();

            var nSims = 1 << 16;            // 65,536 iterations
            
            for (var i = 0; i < nSims; i++)
            {
                lmm.Simulate();

                for (var j = 0; j < capletContracts.Length; j++)
                {
                    npvTotals[j].Add(lmmWrapper.NPV(capletContracts[j]));
                }
            }

            var targetPrices = capletQuotes.Select(c =>
            {
                var f = CommonCalculations.ForwardRate(
                            env.DiscountCurve, c.ResetDate, c.MaturityDate,
                            env.Parameters.Calendar, env.Parameters.DCC);

                var deltaStartReset = env.tau(env.Parameters.PricingDate, c.ResetDate);

                var deltaResetMaturity = env.tau(c.ResetDate, c.MaturityDate);

                var df = env.DiscountCurve.Get(c.MaturityDate) 
                            / env.DiscountCurve.Get(env.Parameters.PricingDate);

                var price = Black76.CapletPrice(f, c.Strike, c.Volatility, 
                                        deltaStartReset, deltaResetMaturity, df);

                return price;
            }).ToArray();

            var prices = npvTotals.Select(t => t.Mean).ToArray();

            var absoluteErrorsBps = prices.Zip(
                                        targetPrices, (a, b) => Math.Abs(a - b) * 1E4).ToArray();
            
            var relativeErrors = prices.Zip(
                                        targetPrices, (a, b) => Math.Abs((a - b) / b)).ToArray();

            Assert.IsTrue(absoluteErrorsBps.Max() < 0.25);  // i.e. within ¼ bp of the Black '76 price 
            Assert.IsTrue(relativeErrors.Max() < 0.01);     // i.e. within 1% of the Black '76 price
        }
    }
}