﻿using LiborOisVol.Core;
using LiborOisVol.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Test.TestData
{
    public static partial class TestEnvironments
    {
        public static PricingEnvironment RebonatoEnvironment
        {
            get
            {
                var env = LMMIPEnvironment;
                env.Parameters.PricingDate = RebonatoGBPDiscountCurve.FirstDate;
                env.DiscountCurve = RebonatoGBPDiscountCurve;
                env.Parameters.LiborTenor = Tenor.Get("6M");
                return env;
            }
        }

        public static DateTime RebonatoBaseDate => DateTime.Parse("2000-08-10");

        public static ICurve RebonatoFlatDiscountCurve
        {
            get
            {
                var discFacs = new Dictionary<double, double>
                {
                    {0.5,0.966736},{1,0.934579},{1.5,0.903492},{2,0.873439},{2.5,0.844385},{3,0.816298},
                    {3.5,0.789145},{4,0.762895},{4.5,0.737519},{5,0.712986},{5.5,0.68927},{6,0.666342},
                    {6.5,0.644177},{7,0.62275},{7.5,0.602035},{8,0.582009},{8.5,0.562649},{9,0.543934},
                    {9.5,0.525841},{10,0.508349},{10.5,0.49144},{11,0.475093},{11.5,0.45929},{12,0.444012}
                    ,{12.5,0.429243},{13,0.414964},{13.5,0.401161},{14,0.387817},{14.5,0.374917},{15,0.362446},
                    {15.5,0.35039},{16,0.338735},{16.5,0.327467},{17,0.316574},{17.5,0.306044},{18,0.295864},
                    {18.5,0.286022},{19,0.276508},{19.5,0.267311},{20,0.258419},{20.5,0.249823}
                };

                return new Curve<MonotoneCubicInterpolator>(
                    discFacs.ToDictionary(x =>
                    RebonatoBaseDate.AddYears((int)Math.Truncate(x.Key)).AddMonths((int)((x.Key - Math.Truncate(x.Key)) * 12)),
                    x => x.Value));
            }
        }

        public static ICurve RebonatoGBPDiscountCurve
        {
            get
            {
                var discFacs = new Dictionary<double, double>
                {
                    {0.5,0.969514},{1,0.939441},{1.5,0.909913},{2,0.881024},{2.5,0.852807},{3,0.825482},
                    {3.5,0.7991},{4,0.773438},{4.5,0.749042},{5,0.725408},{5.5,0.702527},{6,0.680361},
                    {6.5,0.659402},{7,0.639171},{7.5,0.61958},{8,0.600668},{8.5,0.582455},{9,0.564873},
                    {9.5,0.547888},{10,0.531492},{10.5,0.515651},{11,0.50036},{11.5,0.485543},{12,0.47124},
                    {12.5,0.457861},{13,0.444977},{13.5,0.432554},{14,0.420575},{14.5,0.409019},{15,0.397888},
                    {15.5,0.387341},{16,0.377196},{16.5,0.367435},{17,0.358056},{17.5,0.348978},{18,0.340292},
                    {18.5,0.331614},{19,0.323265},{19.5,0.31546},{20,0.307945},{20.5,0.300321}
                };

                return new Curve<MonotoneCubicInterpolator>(
                    discFacs.ToDictionary(x =>
                    RebonatoBaseDate.AddYears((int)Math.Truncate(x.Key)).AddMonths((int)((x.Key - Math.Truncate(x.Key)) * 12)),
                    x => x.Value));
            }
        }
    }
}
