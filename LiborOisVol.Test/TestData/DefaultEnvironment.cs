﻿using LiborOisVol.Core;
using LiborOisVol.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model.Calendar;
using LiborOisVol.Model.Quotes;

namespace LiborOisVol.Test.TestData
{
    public static partial class TestEnvironments
    {
        // From: The Libor Market Model in Practice, ch. 7 p. 71
        public static PricingEnvironment LMMIPEnvironment => new PricingEnvironment
        {
            Parameters = new PricingParameters
            {
                PricingDate = LMMIPBaseDate,
                LiborTenor = Tenor.Get("3M"),
                Calendar = new LondonCalendar(),
                DCC = DayCountConvention.Actual360,
                BDC = BusinessDayConvention.ModifiedFollowing,
            },

            DiscountCurve = LMMIPDiscountCurve
        };
        
        public static DateTime LMMIPBaseDate => DateTime.Parse("2005-01-25");

        // From: The Libor Market Model in Practice, ch. 7 p. 68
        public static ICurve LMMIPDiscountCurve => new Curve<MonotoneCubicInterpolator>(new Dictionary<DateTime, double>
        {
            { DateTime.Parse("2005-01-21"), 1 },
            { DateTime.Parse("2005-01-25"), 0.9997685 },
            { DateTime.Parse("2005-01-26"), 0.9997107 },
            { DateTime.Parse("2005-02-01"), 0.9993636 },
            { DateTime.Parse("2005-02-08"), 0.9989588 },
            { DateTime.Parse("2005-02-25"), 0.9979767 },
            { DateTime.Parse("2005-03-25"), 0.9963442 },
            { DateTime.Parse("2005-04-25"), 0.9945224 },
            { DateTime.Parse("2005-07-25"), 0.9890361 },
            { DateTime.Parse("2005-10-25"), 0.9832707 },
            { DateTime.Parse("2006-01-25"), 0.9772395 },
            { DateTime.Parse("2007-01-25"), 0.9507588 },
            { DateTime.Parse("2008-01-25"), 0.9217704 },
            { DateTime.Parse("2009-01-26"), 0.8908955 },
            { DateTime.Parse("2010-01-25"), 0.8589736 },
            { DateTime.Parse("2011-01-25"), 0.8262486 },
            { DateTime.Parse("2012-01-25"), 0.7928704 },
            { DateTime.Parse("2013-01-25"), 0.7595743 },
            { DateTime.Parse("2014-01-27"), 0.7261153 },
            { DateTime.Parse("2015-01-26"), 0.6942849 },
            { DateTime.Parse("2017-01-25"), 0.6348348 },
            { DateTime.Parse("2020-01-27"), 0.5521957 },
            { DateTime.Parse("2025-01-27"), 0.4345583 }
        });


        public static List<CapQuote> LMMIPCapData = new List<CapQuote>
        {
            new CapQuote {MaturityDate = DateTime.Parse("2006-01-25"), Volatility = 0.1641},
            new CapQuote {MaturityDate = DateTime.Parse("2007-01-25"), Volatility = 0.2137},
            new CapQuote {MaturityDate = DateTime.Parse("2008-01-25"), Volatility = 0.2235},
            new CapQuote {MaturityDate = DateTime.Parse("2009-01-26"), Volatility = 0.2188},
            new CapQuote {MaturityDate = DateTime.Parse("2010-01-25"), Volatility = 0.2127},
            new CapQuote {MaturityDate = DateTime.Parse("2011-01-25"), Volatility = 0.2068},
            new CapQuote {MaturityDate = DateTime.Parse("2012-01-25"), Volatility = 0.2012},
            new CapQuote {MaturityDate = DateTime.Parse("2013-01-25"), Volatility = 0.1958},
            new CapQuote {MaturityDate = DateTime.Parse("2014-01-27"), Volatility = 0.1905},
            new CapQuote {MaturityDate = DateTime.Parse("2015-01-26"), Volatility = 0.1859},
            new CapQuote {MaturityDate = DateTime.Parse("2017-01-25"), Volatility = 0.1806},
            new CapQuote {MaturityDate = DateTime.Parse("2020-01-27"), Volatility = 0.1699},
            new CapQuote {MaturityDate = DateTime.Parse("2025-01-27"), Volatility = 0.1567}
        };

        // From: The Libor Market Model in Practice, ch. 7 p. 68
        public static ICurve LMMIPCapVolCurve = new Curve<LinearInterpolator>(
            LMMIPCapData.ToDictionary(x => x.MaturityDate, x => x.Volatility));
    }
}