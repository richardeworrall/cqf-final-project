﻿using LiborOisVol.Core;
using LiborOisVol.Model;
using LiborOisVol.Model.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LiborOisVol.Test.TestData
{
    public static partial class TestEnvironments
    {
        public static PricingEnvironment July10Environment => new PricingEnvironment
        {
            Parameters = new PricingParameters
            {
                PricingDate = DateTime.Parse("2018-07-10"),
                LiborTenor = Tenor.Get("6M"),
                Calendar = new LondonCalendar(),
                DCC = DayCountConvention.Actual365,
                BDC = BusinessDayConvention.ModifiedFollowing,
            },
            DiscountCurve = July10_OIS_DF_Curve
        };

        public static DateTime[] July10ResetDates => 
            CommonCalculations.BuildResetDateArray(
            July10Environment.Parameters.PricingDate, July10_OIS_DF_Curve_Data.Keys.Max(), Tenor.Get("6M"),
            July10Environment.Parameters.Calendar, July10Environment.Parameters.BDC);

        public static double[] July10LiborRates => CommonCalculations.BuildForwardRateVector(July10ResetDates, July10_LIBOR_6M_PSEUDO_DF_Curve,
            July10Environment.Parameters.Calendar, July10Environment.Parameters.DCC);

        public static double[] July10OISRates => CommonCalculations.BuildForwardRateVector(July10ResetDates, July10_OIS_DF_Curve,
            July10Environment.Parameters.Calendar, July10Environment.Parameters.DCC);

        public static ICurve July10_OIS_DF_Curve => 
            new Curve<MonotoneCubicInterpolator>(July10_OIS_DF_Curve_Data);

        public static ICurve July10_LIBOR_6M_PSEUDO_DF_Curve => 
            new Curve<MonotoneCubicInterpolator>(July10_LIBOR_6M_PSEUDO_DF_Curve_Data);

        private static Dictionary<DateTime, double> July10_OIS_DF_Curve_Data =
            new Dictionary<string, double>
            {
                { "2018-07-10", 1.0 },
                { "2018-07-17", 0.999912939087112 },
                { "2018-07-24", 0.999825893332109 },
                { "2018-08-10", 0.999573401242251 },
                { "2018-09-10", 0.999019154782718 },
                { "2018-10-10", 0.998479903253042 },
                { "2018-11-12", 0.997867570670895 },
                { "2018-12-10", 0.997326522804547 },
                { "2019-01-10", 0.996733989448274 },
                { "2019-02-11", 0.996109442135047 },
                { "2019-03-11", 0.995527816301825 },
                { "2019-04-10", 0.994891545373487 },
                { "2019-05-10", 0.994253053909545 },
                { "2019-06-10", 0.993552930307434 },
                { "2019-07-10", 0.992871184892472 },
                { "2020-01-10", 0.98832472492996 },
                { "2020-07-10", 0.983278769310001 },
                { "2021-07-12", 0.972151445148976 },
                { "2022-07-11", 0.960680150128182 },
                { "2023-07-10", 0.948529412174953 },
                { "2024-07-10", 0.936296200837138 },
                { "2025-07-10", 0.923629088485158 },
                { "2026-07-10", 0.91061283515815 },
                { "2027-07-12", 0.897316281776446 },
                { "2028-07-10", 0.884101088069762 },
                { "2030-07-10", 0.857722930162215 },
                { "2033-07-11", 0.820122379593682 },
                { "2038-07-12", 0.764655036911301 },
                { "2043-07-10", 0.71807592848788 },
                { "2048-07-10", 0.677740266760036 },
                { "2058-07-10", 0.61228923943913 },
                { "2068-07-10", 0.554811624569808 },
                { "2078-07-11", 0.50840571494361 }
            }
            .ToDictionary(x => DateTime.Parse(x.Key), x => x.Value);

        private static Dictionary<DateTime, double> July10_LIBOR_6M_PSEUDO_DF_Curve_Data =
            new Dictionary<string, double>
            {
                { "2018-07-10", 1.0 },
                { "2019-01-10", 0.995817621933166 },
                { "2019-02-11", 0.994922450710697 },
                { "2019-03-11", 0.994262848296147 },
                { "2019-04-10", 0.99351206955637} ,
                { "2019-05-10", 0.992693415285604 },
                { "2019-06-10", 0.991805709499738 },
                { "2019-07-10", 0.990908661613359 },
                { "2019-10-10", 0.988190945588484 },
                { "2020-01-10", 0.985170669165436 },
                { "2020-07-10", 0.978937071853089 },
                { "2021-07-12", 0.965221671566425 },
                { "2022-07-11", 0.950682000020657 },
                { "2023-07-10", 0.935355636475462 },
                { "2024-07-10", 0.919944041782197 },
                { "2025-07-10", 0.904220024625382 },
                { "2026-07-10", 0.888263264191277 },
                { "2027-07-12", 0.872156219738994 },
                { "2028-07-10", 0.856360881485114 },
                { "2030-07-10", 0.825255042096318 },
                { "2033-07-11", 0.781442346772888 },
                { "2038-07-12", 0.716582592794813 },
                { "2043-07-10", 0.661598513587447 },
                { "2048-07-10", 0.613703552649434 },
                { "2058-07-10", 0.537360959544556 },
                { "2068-07-10", 0.473458091132757 },
                { "2078-07-11", 0.420633981124421 }
            }
            .ToDictionary(x => DateTime.Parse(x.Key), x => x.Value);

        public static ICurve July10CapVolCurve =>
            new Curve<MonotoneCubicInterpolator>(July10LIBOR6MCapVolDataByDate);
        
        public static IInterpolator July10CapVolInterpolator =>
            new MonotoneCubicInterpolator(July10LIBOR6MCapVolDataByYearFraction);

        public static Dictionary<double, double> July10LIBOR6MCapVolDataByYearFraction
            => July10LIBOR6MCapVolDataByDate.ToDictionary(
                x => July10Environment.Parameters.Calendar.YearFraction(
                            July10Environment.Parameters.PricingDate, x.Key,
                            July10Environment.Parameters.DCC),
                x => x.Value);

        public static Dictionary<DateTime, double> July10LIBOR6MCapVolDataByDate
            => July10LIBOR6MCapVolDataByTenor.ToDictionary(
                x => July10Environment.AddTenorAdjusted(
                            July10Environment.Parameters.PricingDate, x.Key),
                x => x.Value);

        private static Dictionary<Tenor, double> July10LIBOR6MCapVolDataByTenor =
            new Dictionary<Tenor, double>
            {
                { Tenor.Get("1Y"), 0.3079 },
                { Tenor.Get("2Y"), 0.3204 },
                { Tenor.Get("3Y"), 0.3551 },
                { Tenor.Get("4Y"), 0.3869 },
                { Tenor.Get("5Y"), 0.4109 },
                { Tenor.Get("6Y"), 0.4335 },
                { Tenor.Get("7Y"), 0.4439 },
                { Tenor.Get("8Y"), 0.4536 },
                { Tenor.Get("9Y"), 0.4563 },
                { Tenor.Get("10Y"), 0.4622 },
                { Tenor.Get("12Y"), 0.4609 },
                { Tenor.Get("15Y"), 0.4656 },
                { Tenor.Get("20Y"), 0.4687 },
                { Tenor.Get("25Y"), 0.4848 },
                { Tenor.Get("30Y"), 0.5081 },
            };


        public static ICurve July10_LLOY_CDS_MID_Curve =>
            new Curve<LinearInterpolator>(July10_LLOY_CDS_MID_DataByDate);
        
        public static Dictionary<DateTime, double> July10_LLOY_CDS_MID_DataByDate
            => July10_LLOY_CDS_MID.ToDictionary(
                x => July10Environment.AddTenorAdjusted(
                            July10Environment.Parameters.PricingDate, x.Key),
                x => x.Value);

        private static Dictionary<Tenor, double> July10_LLOY_CDS_MID =
            new Dictionary<Tenor, double>
        {
            { "0M", 0.001085 },
            { "6M", 0.001085 },
            { "1Y", 0.00136 },
            { "2Y", 0.002139 },
            { "3Y", 0.003226 },
            { "4Y", 0.0039965 },
            { "5Y", 0.004843 },
            { "7Y", 0.006503 },
            { "10Y", 0.0079285 }
        };

        public static ICurve July10_LLOY_CDS_BID_Curve =>
            new Curve<LinearInterpolator>(July10_LLOY_CDS_BID_DataByDate);

        public static Dictionary<DateTime, double> July10_LLOY_CDS_BID_DataByDate
            => July10_LLOY_CDS_BID.ToDictionary(
                x => July10Environment.AddTenorAdjusted(
                            July10Environment.Parameters.PricingDate, x.Key),
                x => x.Value);

        private static Dictionary<Tenor, double> July10_LLOY_CDS_BID =
            new Dictionary<Tenor, double>
        {
            { "0M", 0.000785 },
            { "6M", 0.000785 },
            { "1Y", 0.001013 },
            { "2Y", 0.00192 },
            { "3Y", 0.002976 },
            { "4Y", 0.003737 },
            { "5Y", 0.004598 },
            { "7Y", 0.006253 },
            { "10Y", 0.007609 }
        };

        public static ICurve July10_LLOY_CDS_ASK_Curve =>
            new Curve<LinearInterpolator>(July10_LLOY_CDS_ASK_DataByDate);

        public static Dictionary<DateTime, double> July10_LLOY_CDS_ASK_DataByDate
            => July10_LLOY_CDS_ASK.ToDictionary(
                x => July10Environment.AddTenorAdjusted(
                            July10Environment.Parameters.PricingDate, x.Key),
                x => x.Value);

        private static Dictionary<Tenor, double> July10_LLOY_CDS_ASK =
            new Dictionary<Tenor, double>
        {
            { "0M", 0.001385 },
            { "6M", 0.001385 },
            { "1Y", 0.001707 },
            { "2Y", 0.002358 },
            { "3Y", 0.003476 },
            { "4Y", 0.004256 },
            { "5Y", 0.005088 },
            { "7Y", 0.006753 },
            { "10Y", 0.008248 }
        };
    }
}
