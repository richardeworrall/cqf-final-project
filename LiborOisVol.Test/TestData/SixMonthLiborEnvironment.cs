﻿using LiborOisVol.Core;
using LiborOisVol.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Test.TestData
{
    public static partial class TestEnvironments
    {
        public static PricingEnvironment SixMonthLiborEnvironment => new PricingEnvironment
        {
            Parameters = SixMonthLiborPricingParameters,
            DiscountCurve = SixMonthLiborPricingParameters.BuildDiscountCurve(SixMonthLiborForwardLibors)
        };

        public static ICurve SixMonthLiborForwardCurve => 
            SixMonthLiborPricingParameters.BuildDiscountCurve(SixMonthLiborForwardLibors);

        public static PricingParameters SixMonthLiborPricingParameters => new PricingParameters
        {
            PricingDate = DateTime.Parse("2011-12-21"),
            LiborTenor = Tenor.Get("6M"),
            Calendar = new LondonCalendar(),
            DCC = DayCountConvention.Actual365,
            BDC = BusinessDayConvention.ModifiedFollowing,
        };

        public static IVolatilityModel SixMonthLiborVolatilityModel
            => new ABCDModel(a: -0.0716, b: 1.8659, c: 1.3240, d: 0.1619);

        public static Dictionary<Tenor, double> SixMonthLiborForwardLibors = new Dictionary<Tenor, double>
        {
             { "6M" , 0.0129 }, { "12M" , 0.0121 }, { "18M" , 0.0131 }, { "24M" , 0.0146 }, { "30M" , 0.0168 },
             { "36M" , 0.0198 }, { "42M" , 0.0227 }, { "48M" , 0.0251 }, { "54M" , 0.0273 }, { "60M" , 0.029 },
             { "66M" , 0.0305 }, { "72M" , 0.032 }, { "78M" , 0.0326 }, { "84M" , 0.0316 }, { "90M" , 0.0318 },
             { "96M" , 0.0338 }, { "102M" , 0.0342 }, { "108M" , 0.0328 }, { "114M" , 0.0325 }
        };

        public static Dictionary<Tenor, double> SixMonthLiborKiFactorsByTenor = new Dictionary<Tenor, double>()
        {
            { "1Y" , 0.8334 }, { "18M" , 0.6527 }, { "2Y" , 0.7886 }, { "3Y" , 0.4545 },
            { "4Y" , 0.7130 }, { "5Y" , 0.8391 }, { "6Y" , 0.8481 }, { "7Y" , 1.0192 },
            { "8Y" , 0.8483 }, { "9Y" , 0.9989 }, { "10Y" , 0.8807 }
        };

        public static ICurve SixMonthLiborKiCurve
        {
            get
            {
                var e = SixMonthLiborEnvironment;
                var availableKiByDate = SixMonthLiborKiFactorsByTenor.Select(x => new KeyValuePair<DateTime, double>(e.ResetDateAt(x.Key), x.Value)).ToDictionary(x => x.Key, x => x.Value);
                return new Curve<LinearInterpolator>(availableKiByDate);
            }
        }

        public static double[] SixMonthLiborKiVector(Tenor tenorToSimulationEnd)
        {
            var e = SixMonthLiborEnvironment;
            var resetDates = e.ResetDatesBetween("0M", tenorToSimulationEnd).WithoutLastElement();

            var ki = resetDates.Select(x => SixMonthLiborKiCurve.Get(x)).ToArray();

            return ki;
        }
    }
}