﻿// Adapted from:
// https://github.com/MicrosoftArchive/bcl/blob/master/Libraries/BigRational/BigRationalLibrary/BigRational.cs

namespace Numerics
{
    using System;
    using System.Numerics;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    [Serializable]
    public struct BigRational : IComparable, IComparable<BigRational>, IEquatable<BigRational>, ISerializable
    {
        private BigInteger _mNumerator;
        private BigInteger _mDenominator;

        private static readonly BigRational _zero = new BigRational(BigInteger.Zero);
        private static readonly BigRational _one = new BigRational(BigInteger.One);
        private static readonly BigRational _minusOne = new BigRational(BigInteger.MinusOne);

        #region Members for Internal Support

        [StructLayout(LayoutKind.Explicit)]
        private struct DoubleUlong
        {
            [FieldOffset(0)] public double dbl;
            [FieldOffset(0)] public ulong uu;
        }

        private const int DoubleMaxScale = 308;
        private static readonly BigInteger s_bnDoublePrecision = BigInteger.Pow(10, DoubleMaxScale);
        private static readonly BigInteger s_bnDoubleMaxValue = (BigInteger)double.MaxValue;
        private static readonly BigInteger s_bnDoubleMinValue = (BigInteger)double.MinValue;

        [StructLayout(LayoutKind.Explicit)]
        private struct DecimalUInt32
        {
            [FieldOffset(0)] public decimal dec;
            [FieldOffset(0)] public int flags;
        }

        private const int DecimalScaleMask = 0x00FF0000;
        private const int DecimalSignMask = unchecked((int)0x80000000);
        private const int DecimalMaxScale = 28;
        private static readonly BigInteger s_bnDecimalPrecision = BigInteger.Pow(10, DecimalMaxScale);
        private static readonly BigInteger s_bnDecimalMaxValue = (BigInteger)decimal.MaxValue;
        private static readonly BigInteger s_bnDecimalMinValue = (BigInteger)decimal.MinValue;

        #endregion Members for Internal Support

        #region Public Properties

        public static BigRational Zero => _zero;
        public static BigRational One => _one;
        public static BigRational MinusOne => _minusOne;

        public BigInteger Numerator => _mNumerator;
        public BigInteger Denominator => _mDenominator;

        public int Sign => _mNumerator.Sign;

        #endregion Public Properties

        #region Public Instance Methods

        public BigInteger GetWholePart() => BigInteger.Divide(_mNumerator, _mDenominator);
        public BigRational GetFractionPart() => new BigRational(BigInteger.Remainder(_mNumerator, _mDenominator), _mDenominator);

        public override bool Equals(object obj)
        {
            if (!(obj is BigRational)) return false;
            return Equals((BigRational) obj);
        }

        public override int GetHashCode() => (Numerator / Denominator).GetHashCode();
        
        int IComparable.CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (!(obj is BigRational)) throw new ArgumentException("Argument must be of type BigRational", nameof(obj));
            return Compare(this, (BigRational)obj);
        }

        public int CompareTo(BigRational other) => Compare(this, other);
        
        public override string ToString() => $"{Numerator:R}/{Denominator:R}";
        
        public bool Equals(BigRational other)
        {
            if (Denominator == other.Denominator) return _mNumerator == other._mNumerator;

            return _mNumerator * other.Denominator == Denominator * other._mNumerator;
        }

        #endregion Public Instance Methods

        #region Constructors

        public BigRational(BigInteger numerator)
        {
            _mNumerator = numerator;
            _mDenominator = BigInteger.One;
        }

        public BigRational(double value)
        {
            if (double.IsNaN(value)) throw new ArgumentException("Argument is not a number", nameof(value));
            if (double.IsInfinity(value)) throw new ArgumentException("Argument is infinity", nameof(value));

            SplitDoubleIntoParts(value, out var sign, out var exponent, out var significand, out var isFinite);

            if (significand == 0) { this = Zero; return; }

            _mNumerator = significand;
            _mDenominator = 1 << 52;

            if (exponent > 0) _mNumerator = _mNumerator * BigInteger.Pow(2, exponent);
            else if (exponent < 0) _mDenominator = BigInteger.Pow(2, -exponent);

            if (sign < 0) _mNumerator = BigInteger.Negate(_mNumerator);

            Simplify();
        }

        public BigRational(decimal value)
        {
            var bits = decimal.GetBits(value);
            if (bits == null || bits.Length != 4 
                || (bits[3] & ~(DecimalSignMask | DecimalScaleMask)) != 0 
                || (bits[3] & DecimalScaleMask) > 28 << 16)
            {
                throw new ArgumentException("invalid Decimal", nameof(value));
            }

            if (value == decimal.Zero) { this = Zero; return; }

            var ul = ((ulong)(uint)bits[2] << 32) | (uint) bits[1];     // (hi    << 32) | (mid)
            _mNumerator = (new BigInteger(ul) << 32) | (uint)bits[0];   // (hiMid << 32) | (low)

            var isNegative = (bits[3] & DecimalSignMask) != 0;

            if (isNegative) _mNumerator = BigInteger.Negate(_mNumerator);

            var scale = (bits[3] & DecimalScaleMask) >> 16;     // 0-28, power of 10 to divide numerator by
            _mDenominator = BigInteger.Pow(10, scale);

            Simplify();
        }

        public BigRational(BigInteger numerator, BigInteger denominator)
        {
            if (denominator.Sign == 0) throw new DivideByZeroException();

            if (numerator.Sign == 0)
            {
                _mNumerator = BigInteger.Zero;
                _mDenominator = BigInteger.One;
            }
            else if (denominator.Sign < 0)
            {
                _mNumerator = BigInteger.Negate(numerator);
                _mDenominator = BigInteger.Negate(denominator);
            }
            else
            {
                _mNumerator = numerator;
                _mDenominator = denominator;
            }

            Simplify();
        }

        public BigRational(BigInteger whole, BigInteger numerator, BigInteger denominator)
        {
            if (denominator.Sign == 0) throw new DivideByZeroException();

            if (numerator.Sign == 0 && whole.Sign == 0)
            {
                _mNumerator = BigInteger.Zero;
                _mDenominator = BigInteger.One;
            }
            else if (denominator.Sign < 0)
            {
                _mDenominator = BigInteger.Negate(denominator);
                _mNumerator = BigInteger.Negate(whole) * _mDenominator + BigInteger.Negate(numerator);
            }
            else
            {
                _mDenominator = denominator;
                _mNumerator = whole * denominator + numerator;
            }

            Simplify();
        }

        #endregion Constructors

        #region Public Static Methods

        public static BigRational Abs(BigRational r)
            => r._mNumerator.Sign < 0 ? new BigRational(BigInteger.Abs(r._mNumerator), r.Denominator) : r;

        public static BigRational Add(BigRational x, BigRational y) => x + y;
        public static BigRational Subtract(BigRational x, BigRational y) => x - y;
        public static BigRational Multiply(BigRational x, BigRational y) => x * y;
        public static BigRational Divide(BigRational dividend, BigRational divisor) => dividend / divisor;
        public static BigRational Remainder(BigRational dividend, BigRational divisor) => dividend % divisor;
        public static BigRational Negate(BigRational r) => new BigRational(BigInteger.Negate(r._mNumerator), r.Denominator);
        public static BigRational Invert(BigRational r) => new BigRational(r.Denominator, r._mNumerator);


        public static BigRational DivRem(BigRational dividend, BigRational divisor, out BigRational remainder)
        {
            // a/b / c/d  == (ad)/(bc)    ;    a/b % c/d  == (ad % bc)/bd
            // (ad) and (bc) need to be calculated for both the division and the remainder operations.

            var ad = dividend._mNumerator * divisor.Denominator;
            var bc = dividend.Denominator * divisor._mNumerator;
            var bd = dividend.Denominator * divisor.Denominator;

            remainder = new BigRational(ad % bc, bd);

            return new BigRational(ad, bc);
        }

        public static BigRational Exp(BigRational x, uint binaryDigitsAccuracy = 256)
        {
            if (x == Zero) return One;

            x = Clip(x, binaryDigitsAccuracy);

            if (Abs(x) > 1)
            {
                var k = ((int)Abs(x)) + 1;

                var q = Exp(x / k);

                return Pow(q, k);
            }

            var answer = Zero;

            var nextTerm = One;

            var min = Pow(2, (int)-binaryDigitsAccuracy);

            ulong i = 1;

            while (true)
            {
                answer += nextTerm;

                nextTerm *= x;
                nextTerm /= i++;

                if (Abs(nextTerm) < min) break;

                nextTerm = Clip(nextTerm, binaryDigitsAccuracy);
                answer = Clip(answer, binaryDigitsAccuracy);
            }

            return answer;
        }

        public static BigRational Clip(BigRational value, uint binaryDigitsAccuracy = 256)
        {
            var min = Pow(2, (int) -binaryDigitsAccuracy);

            DivRem(value, min, out var rem);

            return value - rem;
        }

        public static BigRational Log(BigRational value)
        {
            return BigInteger.Log(value.Numerator) - BigInteger.Log(value.Denominator);
        }

        public static BigRational Pow(BigRational baseValue, int exponent)
        {
            if (baseValue == Zero) return Zero;

            if (exponent >= 0)
            {
                return new BigRational(BigInteger.Pow(baseValue.Numerator, exponent),
                                    BigInteger.Pow(baseValue.Denominator, exponent));
            }

            return new BigRational(BigInteger.Pow(baseValue.Denominator, -exponent), 
                BigInteger.Pow(baseValue.Numerator, -exponent));
        }

        public static BigInteger LeastCommonDenominator(BigRational x, BigRational y)
            => x.Denominator * y.Denominator / BigInteger.GreatestCommonDivisor(x.Denominator, y.Denominator);

        public static int Compare(BigRational r1, BigRational r2)
            => BigInteger.Compare(r1._mNumerator * r2.Denominator, r2._mNumerator * r1.Denominator);

        #endregion Public Static Methods

        #region Operator Overloads

        public static bool operator ==(BigRational x, BigRational y) => Compare(x, y) == 0;
        public static bool operator !=(BigRational x, BigRational y) => Compare(x, y) != 0;
        public static bool operator <(BigRational x, BigRational y) => Compare(x, y) < 0;
        public static bool operator <=(BigRational x, BigRational y) => Compare(x, y) <= 0;
        public static bool operator >(BigRational x, BigRational y) => Compare(x, y) > 0;
        public static bool operator >=(BigRational x, BigRational y) => Compare(x, y) >= 0;

        public static BigRational operator +(BigRational r) => r;
        public static BigRational operator -(BigRational r) => new BigRational(-r._mNumerator, r.Denominator);
        public static BigRational operator ++(BigRational r) => r + One;
        public static BigRational operator --(BigRational r) => r - One;

        public static BigRational operator +(BigRational r1, BigRational r2)
            => new BigRational(r1._mNumerator * r2.Denominator + r1.Denominator * r2._mNumerator, r1.Denominator * r2.Denominator);

        public static BigRational operator -(BigRational r1, BigRational r2)
            => new BigRational(r1._mNumerator * r2.Denominator - r1.Denominator * r2._mNumerator, r1.Denominator * r2.Denominator);

        public static BigRational operator *(BigRational r1, BigRational r2)
            => new BigRational(r1._mNumerator * r2._mNumerator, r1.Denominator * r2.Denominator);

        public static BigRational operator /(BigRational r1, BigRational r2)
            => new BigRational(r1._mNumerator * r2.Denominator, r1.Denominator * r2._mNumerator);

        public static BigRational operator %(BigRational r1, BigRational r2)
            => new BigRational(r1._mNumerator * r2.Denominator % (r1.Denominator * r2._mNumerator), r1.Denominator * r2.Denominator);

        #endregion Operator Overloads

        #region Explicit conversions from BigRational

        public static explicit operator sbyte(BigRational value)
            => (sbyte) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator ushort(BigRational value) 
            => (ushort) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator uint(BigRational value) 
            => (uint) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator ulong(BigRational value) 
            => (ulong) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator byte(BigRational value)
            => (byte) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator short(BigRational value)
            => (short) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator int(BigRational value)
            => (int) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator long(BigRational value)
            => (long) BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator BigInteger(BigRational value)
            => BigInteger.Divide(value._mNumerator, value._mDenominator);

        public static explicit operator float(BigRational value)
        {
            // The Single value type represents a single-precision 32-bit number with
            // values ranging from negative 3.402823e38 to positive 3.402823e38      
            // values that do not fit into this range are returned as Infinity
            return (float)(double) value;
        }

        public static explicit operator double(BigRational value)
        {
            // The Double value type represents a double-precision 64-bit number with
            // values ranging from -1.79769313486232e308 to +1.79769313486232e308
            // values that do not fit into this range are returned as +/-Infinity

            if (SafeCastToDouble(value._mNumerator) && SafeCastToDouble(value._mDenominator))
            {
                return (double) value._mNumerator / (double)value._mDenominator;
            }

            // scale the numerator to preserve the fraction part through the integer division
            var denormalized = value._mNumerator * s_bnDoublePrecision / value._mDenominator;

            if (denormalized.IsZero)
                return value.Sign < 0 ? BitConverter.Int64BitsToDouble(unchecked((long)0x8000000000000000)) : 0d; // underflow to -+0

            double result = 0;
            var isDouble = false;
            var scale = DoubleMaxScale;

            while (scale > 0)
            {
                if (!isDouble)
                {
                    if (SafeCastToDouble(denormalized))
                    {
                        result = (double)denormalized;
                        isDouble = true;
                    }
                    else denormalized = denormalized / 10;
                }
                result = result / 10;
                scale--;
            }

            if (!isDouble) return value.Sign < 0 ? double.NegativeInfinity : double.PositiveInfinity;

            return result;
        }

        public static explicit operator decimal(BigRational value)
        {
            // The Decimal value type represents decimal numbers ranging
            // from +79,228,162,514,264,337,593,543,950,335 to -79,228,162,514,264,337,593,543,950,335
            // the binary representation of a Decimal value is of the form, ((-2^96 to 2^96) / 10^(0 to 28))

            if (SafeCastToDecimal(value._mNumerator) && SafeCastToDecimal(value._mDenominator))
            {
                return (decimal)value._mNumerator / (decimal)value._mDenominator;
            }

            // scale the numerator to preseve the fraction part through the integer division
            var denormalized = value._mNumerator * s_bnDecimalPrecision / value._mDenominator;
            if (denormalized.IsZero)
            {
                return decimal.Zero; // underflow - fraction is too small to fit in a decimal
            }
            for (var scale = DecimalMaxScale; scale >= 0; scale--)
            {
                if (!SafeCastToDecimal(denormalized))
                {
                    denormalized = denormalized / 10;
                }
                else
                {
                    var dec = new DecimalUInt32 { dec = (decimal) denormalized };
                    dec.flags = (dec.flags & ~DecimalScaleMask) | (scale << 16);
                    return dec.dec;
                }
            }

            throw new OverflowException("Value was either too large or too small for a Decimal.");
        }

        #endregion Explicit conversions from BigRational

        #region Implicit conversions to BigRational

        public static implicit operator BigRational(sbyte value) => new BigRational((BigInteger) value);
        public static implicit operator BigRational(ushort value) => new BigRational((BigInteger) value);
        public static implicit operator BigRational(uint value) => new BigRational((BigInteger) value);
        public static implicit operator BigRational(ulong value) => new BigRational((BigInteger) value);

        public static implicit operator BigRational(byte value) => new BigRational((BigInteger)value);
        public static implicit operator BigRational(short value) => new BigRational((BigInteger)value);
        public static implicit operator BigRational(int value) => new BigRational((BigInteger)value);
        public static implicit operator BigRational(long value) => new BigRational((BigInteger)value);

        public static implicit operator BigRational(BigInteger value) => new BigRational(value);
        public static implicit operator BigRational(float value) => new BigRational(value);
        public static implicit operator BigRational(double value) => new BigRational(value);

        public static implicit operator BigRational(decimal value) => new BigRational(value);

        #endregion Implicit conversions to BigRational

        #region Serialization
        
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            info.AddValue("Numerator", _mNumerator);
            info.AddValue("Denominator", _mDenominator);
        }

        private BigRational(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            _mNumerator = (BigInteger)info.GetValue("Numerator", typeof(BigInteger));
            _mDenominator = (BigInteger)info.GetValue("Denominator", typeof(BigInteger));
        }

        #endregion Serialization

        #region Instance helper methods

        private void Simplify()
        {
            // * if the numerator is {0, +1, -1} then the fraction is already reduced
            // * if the denominator is {+1} then the fraction is already reduced
            if (_mNumerator == BigInteger.Zero) _mDenominator = BigInteger.One;

            var gcd = BigInteger.GreatestCommonDivisor(_mNumerator, _mDenominator);

            if (gcd > BigInteger.One)
            {
                _mNumerator = _mNumerator / gcd;
                _mDenominator = Denominator / gcd;
            }
        }

        #endregion Instance helper methods

        #region Static helper methods

        private static bool SafeCastToDouble(BigInteger value) => s_bnDoubleMinValue <= value && value <= s_bnDoubleMaxValue;
        private static bool SafeCastToDecimal(BigInteger value) => s_bnDecimalMinValue <= value && value <= s_bnDecimalMaxValue;

        private static void SplitDoubleIntoParts(double dbl, out int sign, out int exp, out ulong man, out bool isFinite)
        {
            DoubleUlong du;
            du.uu = 0;
            du.dbl = dbl;

            sign = 1 - ((int)(du.uu >> 62) & 2);
            man = du.uu & 0x000FFFFFFFFFFFFF;
            exp = (int)(du.uu >> 52) & 0x7FF;

            if (exp == 0) // Denormalized number.
            {   
                isFinite = true;
                if (man != 0)
                    exp = -1074;
            }
            else if (exp == 0x7FF) // NaN or Infinite.
            {   
                isFinite = false;
                exp = int.MaxValue;
            }
            else
            {
                isFinite = true;
                man |= 0x0010000000000000; // mask in the implied leading 53rd significand bit
                exp -= 1075;
            }
        }

        #endregion Static helper methods
    }
}