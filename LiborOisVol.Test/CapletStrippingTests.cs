﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiborOisVol.Model;
using System.Linq;
using LiborOisVol.Test.TestData;
using System.Collections.Generic;
using LiborOisVol.Model.Calendar;

namespace LiborOisVol.Test
{
    [TestClass]
    public class CapletStrippingTests
    {
        [TestMethod]
        public void TestCapletStripping()
        {
            var env = TestEnvironments.LMMIPEnvironment;

            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment, 
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve);

            var capletVols = caplets.ToDictionary(x => x.MaturityDate, x => x);

            var targetVolByTenor = new Dictionary<Tenor, double>
            {
                { "6M ", 0.1641 },      { "9M ", 0.1641 },      { "1Y ", 0.1641 },  { "1Y 3M ", 0.2015 },
                { "1Y 6M ", 0.2189 },   { "1Y 9M ", 0.2365 },   { "2Y ", 0.255  },  { "2Y 3M ", 0.2212 },
                { "2Y 6M ", 0.2255 },   { "2Y 9M ", 0.2298 },   { "3Y ", 0.2341 },  { "3Y 3M ", 0.2097 },
                { "3Y 6M ", 0.2083 },   { "3Y 9M ", 0.2077 },   { "4Y ", 0.2051 },  { "4Y 3M ", 0.2007 },
                { "4Y 6M ", 0.1982 },   { "4Y 9M ", 0.1959 },   { "5Y ", 0.1938 },  { "6Y ", 0.1859 },
                { "7Y ", 0.1781 },      { "8Y ", 0.17 },        { "9Y ", 0.1622 },  { "10Y ", 0.157 },
                { "11Y ", 0.1652 },     { "12Y ", 0.1602 },     { "13Y ", 0.1451 }, { "14Y ", 0.138 },
                { "15Y ", 0.1315 },     { "16Y ", 0.1353 },     { "17Y ", 0.13 },   { "18Y ", 0.1243 },
                { "19Y ", 0.1184 },     { "20Y ", 0.1131 }
            };

            var errors = targetVolByTenor.Select(t =>
            {
                var targetVol = t.Value;
                var actualVol = capletVols[env.ResetDateAt(t.Key)].Volatility;
                var error = Math.Abs(actualVol - targetVol);
                return error;
            })
            .ToArray();

            foreach (var error in errors)
            {
                Assert.IsTrue(error < 0.005);
            }
        }
    }
}