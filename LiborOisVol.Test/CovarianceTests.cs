﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Model;
using LiborOisVol.Test.TestData;
using LiborOisVol.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiborOisVol.Core.Sobol;
using LiborOisVol.Core;
using MathNet.Numerics.Statistics;
using System.Diagnostics;

namespace LiborOisVol.Test
{
    [TestClass]
    public class CovarianceTests
    {
        [TestMethod]
        public void TestCovarianceMatrixConstruction()
        {
            var env = TestEnvironments.LMMIPEnvironment;

            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToArray();

            var correlationModel = new ABCDModelCorrelation();

            var model = VolatilityFitting.FitVolatilityModel(env, caplets);

            var correctionFactors = VolatilityFitting.GetCorrectionFactors(env, caplets, model);

            var TT = env.YearFractionsFromStartFor(caplets.Select(c => c.ResetDate).ToArray());

            var matrices = MatrixBuilder.BuildCovarianceMatrices(TT, model, correlationModel, correctionFactors);

            for (var i = 0; i < TT.Length; i++)
            {
                for (var j = i; j < TT.Length - i; j++)
                {
                    for (var k = i; k < TT.Length - i; k++)
                    {
                        Assert.IsTrue(matrices[i][j, k] > 0);
                        Assert.IsTrue(matrices[i][j, k] == matrices[i][k, j]);
                    }
                }
            }
        }

        [TestMethod]
        public void TestMatrixMultiplication()
        {
            var A = new double[1, 2] { { 2.0, 1.0 } };

            var B = new double[2, 3] { { 3.0, 0.0, -2.0 }, { 4.0, 2.0, 9.0 } };

            var result = MatrixHelper.Multiply(A, B);

            var expectedResult = new double[1, 3] { { 10.0, 2.0, 5.0 } };

            Assert.AreEqual(expectedResult.Width(), result.Width());

            for (var i = 0; i < expectedResult.Height(); i++)
            {
                for (var j = 0; j < expectedResult.Width(); j++)
                {
                    Assert.IsTrue(Math.Abs(result[i, j] - expectedResult[i, j]) < 1E-8);
                }
            }
        }

        [TestMethod]
        public void TestCholeskyDecomposition()
        {
            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToArray();

            var env = TestEnvironments.LMMIPEnvironment;

            var TT = env.YearFractionsFromStartFor(caplets.Select(c => c.ResetDate).ToArray());

            var correlationModel = new ABCDModelCorrelation();

            var targetCorrelations = MatrixBuilder.BuildCorrelationMatrix(TT, correlationModel);

            var A = MatrixHelper.CholeskyDecompose(targetCorrelations);

            var ATranspose = MatrixHelper.Transpose(A);

            var reconstructedMatrix = MatrixHelper.Multiply(A, ATranspose);

            for (var i = 0; i < TT.Length; i++)
            {
                for (var j = 0; j < TT.Length; j++)
                {
                    Assert.IsTrue(Math.Abs(targetCorrelations[i, j] - reconstructedMatrix[i, j]) < 1E-8);
                }
            }
        }

        [TestMethod]
        public void TestCorrelatedRandomDrawStatistics()
        {
            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToArray();

            var env = TestEnvironments.LMMIPEnvironment;

            var TT = env.YearFractionsFromStartFor(caplets.Select(c => c.ResetDate).ToArray());

            var correlationModel = new ABCDModelCorrelation();

            var targetCorrelations = MatrixBuilder.BuildCorrelationMatrix(TT, correlationModel);

            var pseudoSquareRoot = MatrixHelper.CholeskyDecompose(targetCorrelations);

            var sobol = SobolFactory.Build((uint)TT.Length, "Sobol/sobol-direction-numbers.dat");

            var N = 1 << 16;

            var results = new double[TT.Length][];

            for (var j = 0; j < TT.Length; j++)
            {
                results[j] = new double[N];
            }

            var normalDraw = new double[TT.Length];
            var correlatedDraw = new double[TT.Length];

            for (var i = 0; i < N; i++)
            {
                sobol.DrawNext();

                for (var j = 0; j < TT.Length; j++)
                {
                    normalDraw[j]
                        = NormalDistribution.InverseCDF(sobol.CurrentDrawDoubles[j]);
                }

                MatrixHelper.HalfMultiply(pseudoSquareRoot, normalDraw, correlatedDraw);

                for (var j = 0; j < TT.Length; j++)
                {
                    results[j][i] = correlatedDraw[j];
                }
            }

            for (var i = 0; i < TT.Length; i++)
            {
                var stats = new DescriptiveStatistics(results[i]);

                Assert.AreEqual(0.0, stats.Mean, 0.0001);
                Assert.AreEqual(1.0, stats.Variance, 0.01);

                for (var j = i; j < TT.Length; j++)
                {
                    var correlation = Correlation.Pearson(results[i], results[j]);

                    Assert.AreEqual(targetCorrelations[i, j], correlation, 0.01);
                }
            }
        }

        [TestMethod]
        public void TestBuildAllCorrelationRootMatrices()
        {
            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToArray();

            var env = TestEnvironments.LMMIPEnvironment;

            var TT = env.YearFractionsFromStartFor(caplets.Select(c => c.ResetDate).ToArray());

            var correlationModel = new ABCDModelCorrelation();

            var correlationMatrices = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            for (var i = 0; i < TT.Length; i++)
            {
                Assert.IsTrue(correlationMatrices[i].Height() == TT.Length - i);
                Assert.IsTrue(correlationMatrices[i].Width() == TT.Length - i);
                
                for (var j = 0; j < TT.Length - i; j++)
                {
                    for (var k = 0; k < TT.Length - i; k++)
                    {
                        if (k <= j) Assert.IsTrue(correlationMatrices[i][j, k] > 0);
                        if (k > j) Assert.IsTrue(correlationMatrices[i][j, k] == 0);
                    }
                }
            }
        }
    }
}