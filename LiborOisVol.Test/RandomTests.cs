﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiborOisVol.Core.Sobol;

namespace LiborOisVol.Test
{
    [TestClass]
    public class RandomTests
    {
        [TestMethod]
        public void TestSobolOffset()
        {
            var offsets = new uint[] { 0, 1, 3, 30, 231, 1000 };

            const uint nDimensions = 250;

            foreach (var offset in offsets)
            {
                var random = SobolFactory.Build(
                    nDimensions, "Sobol/sobol-direction-numbers.dat");

                for(var i = 0; i < offset; i++)
                {
                    random.DrawNext();
                }

                var offsetRandom = SobolFactory.Build(
                    nDimensions, "Sobol/sobol-direction-numbers.dat", offset);

                for(var d = 0; d < nDimensions; d++)
                {
                    Assert.AreEqual(
                        random.CurrentDraw[d], offsetRandom.CurrentDraw[d]);

                    Assert.AreEqual(
                        random.CurrentDrawDoubles[d], offsetRandom.CurrentDrawDoubles[d]);

                    Assert.AreEqual(offset, random.CurrentDrawNumber);
                    Assert.AreEqual(offset, offsetRandom.CurrentDrawNumber);
                }
            }
        }
    }
}