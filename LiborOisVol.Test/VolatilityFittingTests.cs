﻿using System;
using System.Linq;
using LiborOisVol.Model;
using LiborOisVol.Core.Solver;
using LiborOisVol.Test.TestData;
using MathNet.Numerics.Integration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiborOisVol.Test
{
    [TestClass]
    public class VolatilityFittingTests
    {
        [TestMethod]
        public void TestABCDIntegral1()
        {
            // comparing with:
            // http://www.wolframalpha.com/input/?i=integral+from+0+to+4.5+of+((a%2Bb(k-t))*exp(-1*c*(k-t))%2Bd)%5E2+dt+with+a%3D-0.02,b%3D0.5,c%3D1.0,d%3D0.1,k%3D4.5

            var model = new ABCDModel(-0.02, 0.5, 1.0, 0.1);

            var i1 = model.Integral_Sigma_i_Sigma_j_Definite(0, 4.5, 4.5, 4.5);

            Assert.IsTrue(Math.Abs(i1 - 0.192251) < 1E-6);
        }

        [TestMethod]
        public void TestABCDIntegral2()
        {
            // comparing with:
            // http://www.wolframalpha.com/input/?i=integral+from+7+to+8+of+((-0.02%2B0.5*(11.0-t))*exp(-1*1.0*(11.0-t))%2B0.1)*((-0.02%2B0.5*(12-t))*exp(-1*1.0*(12-t))%2B0.1)+dt

            var model = new ABCDModel(-0.02, 0.5, 1.0, 0.1);

            var i1 = model.Integral_Sigma_i_Sigma_j_Definite(7, 8, 11, 12);
            
            Assert.IsTrue(Math.Abs(i1 - 0.0192579) < 1E-6);
        }

        [TestMethod]
        public void TestABCDIntegralNumerical()
        {
            var a = -0.02;
            var b = 0.5;
            var c = 1.0;
            var d = 0.1;

            var Ti = 11.0;
            var Tj = 12.0;

            var tStart = 7.0;
            var tEnd = 8.0;

            var model = new ABCDModel(a, b, c, d);

            var closedFormResult = model.Integral_Sigma_i_Sigma_j_Definite(tStart, tEnd, Ti, Tj);
            
            Func<double,double> integrand = s =>
                model.GetInstantaneousVolatility(s, Ti) 
                        * model.GetInstantaneousVolatility(s, Tj);
            
            var numericalIntegrationResult = 
                DoubleExponentialTransformation.Integrate(integrand, tStart, tEnd, 1e-5);

            Assert.AreEqual(closedFormResult, numericalIntegrationResult, 1E-8);
        }

        [TestMethod]
        public void DemonstrateABCDModelNumericalInstability()
        {
            var a = 0.22066406549984938;
            var b = -0.012644179901837258;
            var c = 1.1196139923831816E-06;
            var d = 0.0011149761779911504;

            var Ti = 0.25;
            var Tj = 10.147222222222222;

            var tStart = 0;
            var tEnd = 0.25;

            var model = new ABCDModel(a, b, c, d);
            
            var closedFormResult = model.Integral_Sigma_i_Sigma_j_Definite(tStart, tEnd, Ti, Tj);
            
            var highPrecisionModel = new ABCDModelAP(a, b, c, d);

            var highPrecisionResult = highPrecisionModel
                .Integral_Sigma_i_Sigma_j_Definite(tStart, tEnd, Ti, Tj);
            
            Func<double, double> integrand = s =>
                model.GetInstantaneousVolatility(s, Ti)
                * model.GetInstantaneousVolatility(s, Tj);

            var numericalIntegrationResult1 = 
                DoubleExponentialTransformation.Integrate(integrand, tStart, tEnd, 1e-5);

            var numericalIntegrationResult2 = 
                SimpsonRule.IntegrateComposite(integrand, tStart, tEnd, 1000000);
            
            Assert.AreEqual(highPrecisionResult, numericalIntegrationResult1, 1E-8);
            Assert.AreEqual(numericalIntegrationResult2, numericalIntegrationResult1, 1E-8);

            // Demonstrating Numerical Instability
            Assert.AreNotEqual(closedFormResult, numericalIntegrationResult1, 1E-8);
        }

        [TestMethod]
        public void TestABCDFittingJaeckel()
        {
            var T = Enumerable.Range(9, 4).Select(x => x / 2.0).ToArray();

            var volatilities = new[] { 0.2143, 0.2067, 0.1998, 0.1935 };

            var objFun = new VolatilityFitting.ABCDObjectiveFunction(T, volatilities);

            var solver = new NelderMead(4, objFun.Evaluate, VolatilityFitting.ABCDObjectiveFunction.Constrain,
                seedGenerator: VolatilityFitting.ABCDObjectiveFunction.SeedGenerator);

            solver.Solve(out var result, out var error);

            var model = new ABCDModelAP(result[0], result[1], result[2], result[3]);

            var kk = new double[T.Length];

            for (var i = 0; i < volatilities.Length; i++)
            {
                var target = volatilities[i] * volatilities[i] * T[i];

                var modelResult = model.Integral_Sigma_i_Sigma_j_Definite(0, T[i], T[i], T[i]);

                kk[i] = Math.Sqrt(target / modelResult);
            }
            
            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Max() < 0.2);
            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Average() < 0.05);
        }

        [TestMethod]
        public void TestABCDFitting()
        {
            var T = Enumerable.Range(1, 19).Select(Convert.ToDouble).ToArray();

            var volatilities = new[]
            {
                0.180253, 0.191478, 0.186154, 0.177294, 0.167887,
                0.158123, 0.152688, 0.148709, 0.144703, 0.141259,
                0.137982, 0.134708, 0.131428, 0.128148, 0.127100,
                0.126822, 0.126539, 0.126257, 0.125970
            };

            var objFun = new VolatilityFitting.ABCDObjectiveFunction(T, volatilities);

            var solver = new NelderMead(4, objFun.Evaluate, VolatilityFitting.ABCDObjectiveFunction.Constrain,
                seedGenerator: VolatilityFitting.ABCDObjectiveFunction.SeedGenerator);

            solver.Solve(out var result, out var error);

            var model = new ABCDModel(result[0], result[1], result[2], result[3]);

            var kk = new double[T.Length];

            for (var i = 0; i < volatilities.Length; i++)
            {
                var target = volatilities[i] * volatilities[i] * T[i];
                var modelResult = model.Integral_Sigma_i_Sigma_j_Definite(0, T[i], T[i], T[i]);

                kk[i] = Math.Sqrt(target / modelResult);
            }

            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Max() < 0.2);
            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Average() < 0.05);
        }

        [TestMethod]
        public void TestCapletReconstruction()
        {
            var env = TestEnvironments.LMMIPEnvironment;

            var caplets = CapletBuilder.Build(TestEnvironments.LMMIPEnvironment,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToArray();

            var model = VolatilityFitting.FitVolatilityModel(env, caplets);

            var kk = VolatilityFitting.GetCorrectionFactors(env, caplets, model);

            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Max() < 0.3);
            Assert.IsTrue(kk.Select(u => Math.Abs(u - 1.0)).Average() < 0.05);
        }
    }
}