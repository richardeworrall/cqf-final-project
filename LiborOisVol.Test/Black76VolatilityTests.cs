﻿using System;
using LiborOisVol.Model;
using LiborOisVol.Model.Calendar;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiborOisVol.Test
{
    [TestClass]
    public class Black76VolatilityTests
    {
        [TestMethod]
        public void TestBlackCapletPrice()
        {
            var baseDate = DateTime.Parse("2005-01-21");
            var resetDate = DateTime.Parse("2006-01-25");
            var maturityDate = DateTime.Parse("2006-04-25");

            const double resetDateDiscountFactor = 0.9774658;
            const double maturityDateDiscountFactor = 0.9712884;

            const double strike = 0.02361;
            const double vol = 0.2015;

            var cal = new LondonCalendar();
            const DayCountConvention dcc = DayCountConvention.Actual360;

            var yearFractionToReset = cal.YearFraction(baseDate, resetDate, dcc);

            var yearFractionResetToMaturity = cal.YearFraction(resetDate, maturityDate, dcc);

            var forwardRate = CommonCalculations.ForwardRate(
                resetDateDiscountFactor, maturityDateDiscountFactor, yearFractionResetToMaturity);

            var capletPrice = Black76.CapletPrice(forwardRate, strike, vol, 
                yearFractionToReset, yearFractionResetToMaturity, maturityDateDiscountFactor);

            Assert.IsTrue(Math.Abs(capletPrice - 0.000733039) < 1E-5);  // i.e. within 0.1bps
        }

        [TestMethod]
        public void TestBlackCapletImpliedVol()
        {
            var baseDate = DateTime.Parse("2005-01-21");
            var resetDate = DateTime.Parse("2006-01-25");
            var maturityDate = DateTime.Parse("2006-04-25");

            const double resetDateDiscountFactor = 0.9774658;
            const double maturityDateDiscountFactor = 0.9712884;

            const double strike = 0.02361;

            const double price = 0.000733039;

            var cal = new LondonCalendar();
            const DayCountConvention dcc = DayCountConvention.Actual360;

            var yearFractionToReset = cal.YearFraction(baseDate, resetDate, dcc);

            var yearFractionResetToMaturity = cal.YearFraction(resetDate, maturityDate, dcc);

            var forwardRate = CommonCalculations.ForwardRate(
                resetDateDiscountFactor, maturityDateDiscountFactor, yearFractionResetToMaturity);

            var capletImpliedVol = Black76.CapletImpliedVol(forwardRate, strike, price, 
                yearFractionToReset, yearFractionResetToMaturity, maturityDateDiscountFactor);

            Assert.IsTrue(Math.Abs(capletImpliedVol - 0.2015) < 0.01);

            var impliedPrice = Black76.CapletPrice(forwardRate, strike, capletImpliedVol,
                yearFractionToReset, yearFractionResetToMaturity, maturityDateDiscountFactor);

            Assert.IsTrue(Math.Abs(impliedPrice - 0.000733039) < 1E-8);
        }
    }
}