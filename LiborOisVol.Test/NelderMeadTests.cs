﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Core.Solver;
using LiborOisVol.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiborOisVol.Test
{
    [TestClass]
    public class NelderMeadTests
    {
        // TODO: Test against some more of these:
        // https://en.wikipedia.org/wiki/Test_functions_for_optimization

        [TestMethod]
        public void TestSimple1DConstrained()
        {
            var price = 0.00017533283139952282;

            ObjectiveFunction f = p => Math.Abs(price - Black76.CapletPrice(0.031, 0.04, p[0], 0.5, 0.5, 0.982));

            Constrain fConst = p => { p[0] = Math.Max(p[0], 0.0); };

            var nm = new NelderMead(1, f, fConst, null, seedGenerator: SeedGenerators.Sobol);

            nm.Solve(out var result, out var bestScore);

            Assert.IsTrue(Math.Abs(bestScore) < 1E-8);
            Assert.IsTrue(Math.Abs(result[0] - 0.2873) < 1E-8);
        }

        [TestMethod]
        public void TestSimple2DQuadratic()
        {
            ObjectiveFunction f = p => 3 * (p[0] + 3) * (p[0] + 3) + 4 * (p[1] - 7) * (p[1] - 7) + 0.5;

            var nm = new NelderMead(2, f, null, null, seedGenerator: SeedGenerators.Sobol);

            nm.Solve(out var result, out var bestScore);

            Assert.AreEqual(0.5, bestScore);
            Assert.IsTrue(Math.Abs(result[0] + 3) < 1E-8);
            Assert.IsTrue(Math.Abs(result[1] - 7) < 1E-8);
        }

        [TestMethod]
        public void TestHimmelBlau()
        {
            ObjectiveFunction f = p => Math.Pow(p[0] * p[0] + p[1] - 11, 2) + Math.Pow(p[0] + p[1] * p[1] - 7, 2);

            var nm = new NelderMead(2, f, null, null, 
                seedGenerator: SeedGenerators.Sobol);

            nm.Solve(out var result, out var bestScore);

            Assert.IsTrue(Math.Abs(bestScore) < 1E-8);

            var roots = new List<Tuple<double, double>>
            {
                Tuple.Create(3.0, 2.0),
                Tuple.Create(-2.805118, 3.131312),
                Tuple.Create(-3.779310, -3.283186),
                Tuple.Create(3.584428, -1.848126)
            };

            Func<double[], Tuple<double, double>, double> distance =
                (solverResult, root) =>
                    Math.Sqrt(Math.Pow(solverResult[0] - root.Item1, 2)
                              + Math.Pow(solverResult[1] - root.Item2, 2));

            var minDistance = roots.Select(root => distance(result, root)).Min();

            Assert.IsTrue(minDistance < 1E-6);
        }

        [TestMethod]
        public void TestAckey()
        {
            ObjectiveFunction f = p =>
            {
                var x = p[0]; var y = p[1];

                return -20.0 * Math.Exp(-0.2 * Math.Sqrt(0.5 * (x * x + y * y)))
                       - Math.Exp(0.5 * (Math.Cos(2 * Math.PI * x) + Math.Cos(2 * Math.PI * y))) + Math.E + 20;
            };

            var nm = new NelderMead(2, f, null, null,
                seedGenerator: SeedGenerators.Sobol);

            nm.Solve(out var result, out var bestScore);

            Assert.IsTrue(Math.Abs(bestScore) < 1E-8);
            
            Assert.IsTrue(Math.Abs(result[0]) < 1E-8);
            Assert.IsTrue(Math.Abs(result[1]) < 1E-8);
        }
    }
}