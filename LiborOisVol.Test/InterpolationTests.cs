﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using LiborOisVol.Core;

namespace LiborOisVol.Test
{
    /// <summary>
    /// Testing my linear and monotone cubic interpolators against the alglib2 implementations.
    /// In the monotone cubic case, alglib performs flat extrapolation, whereas my implementation throws an exception
    /// As a result, interpolation values near the extremal points are different unless dummy points are added
    /// to force either a tangent of 0 at the extremal points, or tangents equal to the first and last secant gradients
    /// </summary>
    [TestClass]
    public class InterpolationTests
    {
        private static IEnumerable<double> Step(double min, double max, double stepSize)
        {
            if (stepSize <= 0) throw new ArgumentException("Stepsize should be positive");

            if (max <= min) yield break;

            var step = min;

            while (step <= max)
            {
                yield return step;
                step += stepSize;
            }
        }

        [TestMethod]
        public void TestLinearInterpolation()
        {
            var data = new Dictionary<double, double>
            {
                { 200, 5.5 },
                { 100, 3.5 },
                { 130, 4.5 },
                { 280, 2.5 },
                { 300, 0 },
                { 170, 2.0 },
                { 220, 5.5 }
            };

            // Mine
            var interpolator = new LinearInterpolator(data);
            Func<double, double> fMine = x => interpolator.Get(x);

            // ALGLIB
            alglib.spline1dbuildlinear(data.Keys.ToArray(), data.Values.ToArray(), out var spline);
            Func<double, double> fAlglib = x => alglib.spline1dcalc(spline, x);

            foreach (var diff in Step(100, 300, 1.0).Select(s => Math.Abs(fMine(s) - fAlglib(s))))
            {
                Assert.IsTrue(diff < 1E-8);
            }
        }

        [TestMethod]
        public void TestMonotoneCubicSplineInterpolationFlatEnds()
        {
            var data = new Dictionary<double, double>
            {
                { 100, 3.5 },
                { 130, 4.5 },
                { 170, 2.0 },
                { 200, 5.5 },
                { 220, 5.5 },
                { 280, 2.5 },
                { 300, 0 }
            };

            var dataExtended = data.ToArray()
                .WithPrependedValue(new KeyValuePair<double, double>(50, 3.5))
                .WithAppendedValue(new KeyValuePair<double, double>(350, 0))
                .ToDictionary(x => x.Key, x => x.Value);

            // Mine
            var interpolator = new MonotoneCubicInterpolator(dataExtended);
            Func<double, double> fMine = x => interpolator.Get(x);

            // ALGLIB
            alglib.spline1dbuildmonotone(data.Keys.ToArray(), data.Values.ToArray(), out var spline);
            Func<double, double> fAlglib = x => alglib.spline1dcalc(spline, x);
            
            foreach (var diff in Step(100, 300, 1.0).Select(s => Math.Abs(fMine(s) - fAlglib(s))))
            {
                Assert.IsTrue(diff < 1E-8);
            }
        }

        [TestMethod]
        public void TestMonotoneCubicSplineInterpolationSecantMatchedEnds()
        {
            var data = new Dictionary<double, double>
            {
                { 100, 3.5 },
                { 130, 4.5 },
                { 170, 2.0 },
                { 200, 5.5 },
                { 220, 5.5 },
                { 280, 2.5 },
                { 300, 0 }
            };

            var dataExtended = data.ToArray()
                .WithPrependedValue(new KeyValuePair<double, double>(70, 2.5))
                .WithAppendedValue(new KeyValuePair<double, double>(320, -2.5))
                .ToDictionary(x => x.Key, x => x.Value);

            // Mine
            var interpolator = new MonotoneCubicInterpolator(data);
            Func<double, double> fMine = x => interpolator.Get(x);

            // ALGLIB
            alglib.spline1dbuildmonotone(dataExtended.Keys.ToArray(), dataExtended.Values.ToArray(), out var spline);
            Func<double, double> fAlglib = x => alglib.spline1dcalc(spline, x);

            foreach (var diff in Step(100, 300, 1.0).Select(s => Math.Abs(fMine(s) - fAlglib(s))))
            {
                Assert.IsTrue(diff < 1E-8);
            }
        }
    }
}