﻿using System;
using System.Linq;
using LiborOisVol.Core;
using LiborOisVol.Core.Sobol;
using LiborOisVol.Model;
using LiborOisVol.Model.Instruments;
using LiborOisVol.Simulation;
using LiborOisVol.Test.TestData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiborOisVol.Test
{
    [TestClass]
    public class MultithreadingTests
    {
        [TestMethod]
        public void TestLMMCapletPricingMultithreaded()
        {
            var env = TestEnvironments.LMMIPEnvironment;
            var capVolCurve = TestEnvironments.LMMIPCapVolCurve;
            var discountCurve = TestEnvironments.LMMIPDiscountCurve;

            var capletQuotes = CapletBuilder
                .Build(env, capVolCurve, discountCurve)
                .Take(20)
                .ToArray();

            var resetDates = capletQuotes.Select(x => x.ResetDate).ToArray();

            var terminalDate = capletQuotes.Last().MaturityDate;
            
            var resetsPlusFinalMaturity = resetDates.WithAppendedValue(terminalDate);

            var f0 = CommonCalculations.BuildForwardRateVector(resetsPlusFinalMaturity,
                env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var contracts = capletQuotes.Select(
                    (c, idx) => new Caplet(c.ResetDate, c.MaturityDate,
                        env.Parameters.LiborTenor, f0[idx]))
                .Cast<IContract>()
                .ToArray();

            var volatilityModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            var correlationModel = new ABCDModelCorrelation();

            var kk = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, volatilityModel);

            var randomFactory = SobolFactory.GetFactory("Sobol/sobol-direction-numbers.dat");

            var modelFactory = LiborMarketModelBuilder.GetFactory(env,
                resetDates, terminalDate,
                volatilityModel, kk, correlationModel,
                env.DiscountCurve, randomFactory);

            uint nSimulations = 1 << 17;     //  131,072 iterations

            var simulation = new MonteCarloSimulation(nSimulations,
                modelFactory, contracts,
                ThreadingStrategy.MultiThreaded);
            
            var results = simulation.Run();

            var targetPrices = capletQuotes.Select(c =>
            {
                var f = CommonCalculations.ForwardRate(
                    env.DiscountCurve, c.ResetDate, c.MaturityDate,
                    env.Parameters.Calendar, env.Parameters.DCC);

                var deltaStartReset = env.tau(env.Parameters.PricingDate, c.ResetDate);

                var deltaResetMaturity = env.tau(c.ResetDate, c.MaturityDate);

                var df = env.DiscountCurve.Get(c.MaturityDate)
                         / env.DiscountCurve.Get(env.Parameters.PricingDate);

                var price = Black76.CapletPrice(f, c.Strike, c.Volatility,
                    deltaStartReset, deltaResetMaturity, df);

                return price;
            }).ToArray();

            var prices = results.Select(t => t.Mean).ToArray();

            var absoluteErrorsBps = prices.Zip(
                targetPrices, (a, b) => Math.Abs(a - b) * 1E4).ToArray();

            var relativeErrors = prices.Zip(
                targetPrices, (a, b) => Math.Abs((a - b) / b)).ToArray();

            Assert.IsTrue(absoluteErrorsBps.Max() < 0.25);  // i.e. within ¼ bp of the Black '76 price 
            Assert.IsTrue(relativeErrors.Max() < 0.01);     // i.e. within 1% of the Black '76 price
        }
    }
}