﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OxyPlot;
using LiborOisVol.Core;
using OxyPlot.Series;
using System.IO;
using OxyPlot.Axes;
using LiborOisVol.Model;
using LiborOisVol.Test.TestData;
using System.Linq;
using System.Collections.Generic;
using LiborOisVol.Core.Sobol;
using static LiborOisVol.Test.TestData.TestEnvironments;
using LiborOisVol.Core.Stats;
using LiborOisVol.Model.Calendar;
using OxyPlot.Annotations;

namespace LiborOisVol.Test
{
    [TestClass]
    public class ReportPlots
    {
        public const string TargetDir =
            @"report-plots\";

        [TestInitialize]
        public void Setup()
        {
            Directory.CreateDirectory(TargetDir);
        }
        
        public static PlotModel GetDefaultPlotModel()
        {
            var lineThickness = 0.75;

            var model = new PlotModel
            {
                LegendPosition = LegendPosition.TopLeft,
                LegendLineSpacing = 10,
                LegendFontSize = 12,
                PlotAreaBorderThickness = new OxyThickness(lineThickness)
            };

            model.Axes.Add(
                new LinearAxis
                {
                    Position = AxisPosition.Bottom,
                    //Title = "x",
                    Key = "xAxis",
                    AxisTitleDistance = 16,
                    TitleFontSize = 14,
                    FontSize = 12,
                    AxislineThickness = lineThickness,
                    MajorGridlineThickness = lineThickness,
                    MinorGridlineThickness = lineThickness,
                });

            model.Axes.Add(
                new LinearAxis
                {
                    Position = AxisPosition.Left,
                    //Title = "f(x)",
                    Key = "yAxis",
                    AxisTitleDistance = 20,
                    TitleFontSize = 14,
                    FontSize = 12,
                    AxislineThickness = lineThickness,
                    MajorGridlineThickness = lineThickness,
                    MinorGridlineThickness = lineThickness,
                });

            return model;
        }

        public static void WriteToPDF(PlotModel model, string fileName, string td = TargetDir)
        {
            using (var stream = File.Create(td + fileName + ".pdf"))
            {
                var exporter = new OxyPlot.Pdf.PdfExporter { Width = 500, Height = 350 };
                exporter.Export(model, stream);
            }
        }
        
        [TestMethod]
        public void PlotNormalCDFAndPDF()
        {
            var fileName = "normalCDFandPDF";

            var model = GetDefaultPlotModel();
            
            Func<double, double> n =
                x => NormalDistribution.PDF(x);

            Func<double, double> N =
                x => NormalDistribution.CDF(x);
            
            var nF = new FunctionSeries(n,
                -5, 5, 5000)
            {
                Title = "Normal PDF",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(255, 0, 0),
                XAxisKey = "xAxis",
                YAxisKey = "yAxis"
            };

            var NF = new FunctionSeries(N,
                -5, 5, 5000)
            {
                Title = "Normal CDF",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255),
                XAxisKey = "xAxis",
                YAxisKey = "yAxis"
            };

            model.Series.Add(nF);
            model.Series.Add(NF);

            WriteToPDF(model, fileName);
        }

        [TestMethod]
        public void PlotSteppedForwardCurve()
        {
            var fileName = "steppedForwardCurve";

            var model = GetDefaultPlotModel();

            #region Financial Calcs

            var env = TestEnvironments.LMMIPEnvironment;
            var capVolCurve = TestEnvironments.LMMIPCapVolCurve;
            var discountCurve = TestEnvironments.LMMIPDiscountCurve;

            var capletQuotes = CapletBuilder
                .Build(env, capVolCurve, discountCurve)
                .ToArray();

            var resetDates = capletQuotes.Select(x => x.ResetDate).ToArray();

            var terminalDate = capletQuotes.Last().MaturityDate;

            var resetsPlusFinalMaturity = resetDates.WithAppendedValue(terminalDate);

            var f0 = CommonCalculations.BuildForwardRateVector(resetsPlusFinalMaturity,
                env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var T = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate,
                resetDates, env.Parameters.Calendar, env.Parameters.DCC);

            #endregion Financial Calcs

            var points = new List<DataPoint>();

            for (int i = 0; i < T.Length - 1; i++)
            {
                points.Add(new DataPoint(T[i], f0[i]));
                points.Add(new DataPoint(T[i + 1], f0[i]));
            }

            var F = new LineSeries()
            {
                Title = "Forward LIBOR Rates",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255)
            };

            F.Points.AddRange(points);

            model.Series.Add(F);

            WriteToPDF(model, fileName);
        }

        private readonly Dictionary<double, double> _sampleDataPoints =
            new Dictionary<double, double>
            {
                { 0.0, 0.0 },
                //{ 1.0, 3.0 },
                { 2.0, 4.0 },
                //{ 3.0, 2.7 },
                { 4.0, 2.5 },
                //{ 5.0, 3.0 },
                { 6.0, 7.0 },
                //{ 7.0, 8.0 },
                { 8.0, 8.2 },
                //{ 9.0, 4.0 },
                { 10.0, 4.4 },
                { 10.1, 4.4 },
            };

        [TestMethod]
        public void PlotInterpolationCurves()
        {
            var fileName = "interpolationExample";

            var model = GetDefaultPlotModel();

            //model.Title = "Linear and Monotone Cubic Spline Interpolation";
            model.IsLegendVisible = true;
            model.LegendPosition = LegendPosition.LeftTop;

            var linearInterpolator = new LinearInterpolator(_sampleDataPoints);
            var monotoneInterpolator = new MonotoneCubicInterpolator(_sampleDataPoints);

            var linearSeries = new FunctionSeries(
                x => linearInterpolator.Get(x),
                0.0, 10.0, 5000)
            {
                Title = "Linear",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Dot
            };

            var monotoneSeries = new FunctionSeries(
                x => monotoneInterpolator.Get(x),
                0.0, 10.0, 5000)
            {
                Title = "Monotone Cubic Spline",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Dash
            };

            var dataSeries = new ScatterSeries
            {
                Title = "Data Points",
                Background = OxyColor.FromRgb(255, 255, 255),
                MarkerType = MarkerType.Cross,
                MarkerSize = 4,
                MarkerStroke = OxyColors.Black
            };

            dataSeries.Points.AddRange(_sampleDataPoints.Take(_sampleDataPoints.Count-1).Select(p => new ScatterPoint(p.Key, p.Value)));
            
            model.Series.Add(linearSeries);
            model.Series.Add(monotoneSeries);
            model.Series.Add(dataSeries);

            WriteToPDF(model, fileName);
        }

        [TestMethod]
        public void PlotCapAndCapletVolCurves()
        {
            var fileName = "capAndCapletVolSmooth";

            #region Financial Calcs

            var env = TestEnvironments.LMMIPEnvironment;

            var capVols = TestEnvironments.LMMIPCapData.ToDictionary(x =>
                env.Parameters.Calendar.YearFraction(env.Parameters.PricingDate, x.MaturityDate, env.Parameters.DCC),
                x => x.Volatility);

            var iCap = new MonotoneCubicInterpolator(capVols);

            Func<double, double> fCap = x => iCap.Get(x);
            
            var capletVols = CapletBuilder.Build(env,
                TestEnvironments.LMMIPCapVolCurve, TestEnvironments.LMMIPDiscountCurve).ToDictionary(x =>
                    env.Parameters.Calendar.YearFraction(env.Parameters.PricingDate, x.MaturityDate, env.Parameters.DCC),
                x => x.Volatility);

            var iCaplet = new MonotoneCubicInterpolator(capletVols);

            Func<double, double> fCaplet = x => iCaplet.Get(x);

            #endregion

            var model = GetDefaultPlotModel();

            //model.Title = "Linear and Monotone Cubic Spline Interpolation";
            model.IsLegendVisible = true;
            model.LegendPosition = LegendPosition.RightTop;

            model.Axes[0].Title = "Time To Reset Date (Years)";
            model.Axes[1].Title = "Volatility";

            // model.Axes[1].Minimum = 0.0;

            var capSeries = new FunctionSeries(fCap, iCap.LowerBound+0.01, iCap.UpperBound-0.01, n: 1000)
            {
                Title = "Cap Vols",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Solid
            };

            var capletSeries = new FunctionSeries(fCaplet, iCaplet.LowerBound+0.01, iCaplet.UpperBound-0.01, n: 1000)
            {
                Title = "Caplet Vols (Stripped)",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Dot
            };
            
            model.Series.Add(capSeries);
            model.Series.Add(capletSeries);

            void WriteToPDFCustom(PlotModel m, string f, string td = TargetDir)
            {
                using (var stream = File.Create(td + fileName + ".pdf"))
                {
                    var exporter = new OxyPlot.Pdf.PdfExporter { Width = 600, Height = 350 };
                    exporter.Export(model, stream);
                }
            }

            WriteToPDFCustom(model, fileName);
        }

        [TestMethod]
        public void PlotLinearForwardCurve()
        {
            var fileName = "linearForwardCurve";

            var model = GetDefaultPlotModel();

            #region Financial Calcs

            var env = TestEnvironments.LMMIPEnvironment;
            var capVolCurve = TestEnvironments.LMMIPCapVolCurve;
            var discountCurve = TestEnvironments.LMMIPDiscountCurve;

            var capletQuotes = CapletBuilder
                .Build(env, capVolCurve, discountCurve)
                .ToArray();

            var resetDates = capletQuotes.Select(x => x.ResetDate).ToArray();

            var terminalDate = capletQuotes.Last().MaturityDate;

            var resetsPlusFinalMaturity = resetDates.WithAppendedValue(terminalDate);

            var f0 = CommonCalculations.BuildForwardRateVector(resetsPlusFinalMaturity,
                env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var T = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate,
                resetDates, env.Parameters.Calendar, env.Parameters.DCC);

            #endregion Financial Calcs

            var interpolator = new LinearInterpolator(T, f0);

            var F = new FunctionSeries(
                x => interpolator.Get(x),
                interpolator.LowerBound, interpolator.UpperBound, 5000)
            {
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255)
            };

            model.Series.Add(F);

            WriteToPDF(model, fileName);
        }

        [TestMethod]
        public void PlotSmoothedForwardCurve()
        {
            var fileName = "smoothedForwardCurve";

            var model = GetDefaultPlotModel();

            #region Financial Calcs

            var env = TestEnvironments.LMMIPEnvironment;
            var capVolCurve = TestEnvironments.LMMIPCapVolCurve;
            var discountCurve = TestEnvironments.LMMIPDiscountCurve;

            var capletQuotes = CapletBuilder
                .Build(env, capVolCurve, discountCurve)
                .ToArray();

            var resetDates = capletQuotes.Select(x => x.ResetDate).ToArray();

            var terminalDate = capletQuotes.Last().MaturityDate;

            var resetsPlusFinalMaturity = resetDates.WithAppendedValue(terminalDate);

            var f0 = CommonCalculations.BuildForwardRateVector(resetsPlusFinalMaturity,
                env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var T = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate,
                resetDates, env.Parameters.Calendar, env.Parameters.DCC);

            #endregion Financial Calcs

            var interpolator = new MonotoneCubicInterpolator(T, f0);

            var F = new FunctionSeries(
               x => interpolator.Get(x),
               interpolator.LowerBound, interpolator.UpperBound, 5000)
            {
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255)
            };

            model.Series.Add(F);

            WriteToPDF(model, fileName);
        }

        [TestMethod]
        public void PlotInstantaneousVolatilityModel()
        {
            var filename = "abcdInstantaneousVolatility";

            var plotModel = GetDefaultPlotModel();

            plotModel.LegendPosition = LegendPosition.RightTop;

            plotModel.Axes[0].Title = "Time to Maturity (Years)";
            plotModel.Axes[1].Title = "Volatility";

            var volModel = new ABCDModel(0.0065, 0.25, 0.8769, 0.1023);

            Func<double, double> plotVol = x => volModel.GetInstantaneousVolatility(20 - x, 20);
            //_{i}
            //var d = FontEncoding.

            var s = new FunctionSeries(plotVol, 0, 20, 10000)
            {
                Title = "Instantaneous Volatility (σ_{i})",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255)
            };

            plotModel.Series.Add(s);

            WriteToPDF(plotModel, filename);
        }

        [TestMethod]
        public void PlotCorrelationModel()
        {
            var filename = "abcdCorrelation";

            var plotModel = GetDefaultPlotModel();

            plotModel.LegendPosition = LegendPosition.RightTop;

            plotModel.Axes[0].Title = "T_{i} - T_{j} (Years)";
            plotModel.Axes[1].Title = "Correlation";

            var corrModel = new ABCDModelCorrelation();

            Func<double, double> plotCorr = x => corrModel.GetCorrelation(0, x);
            //_{i}
            //var d = FontEncoding.

            var s = new FunctionSeries(plotCorr, -10, 10, 10000)
            {
                Title = "Correlation (ρ_{ij})",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColor.FromRgb(0, 0, 255)
            };

            plotModel.Series.Add(s);

            WriteToPDF(plotModel, filename);
        }

        [TestMethod]
        public void PlotSobolExample()
        {
            var filename = "sobol";

            var lineThickness = 0.75;
            var model = new PlotModel
            {
                Title = "Sobol Sequence Draws",
                LegendPosition = LegendPosition.TopLeft,
                LegendLineSpacing = 10,
                LegendFontSize = 12,
                PlotAreaBorderThickness = new OxyThickness(lineThickness),
            };
            model.Axes.Add(
            new LinearAxis
            {
                Position = AxisPosition.Bottom,
                //Title = "x",
                Key = "xAxis",
                AxisTitleDistance = 16,
                TitleFontSize = 14,
                FontSize = 12,
                AxislineThickness = lineThickness,
                MajorGridlineThickness = lineThickness,
                MinorGridlineThickness = lineThickness,
            });

            model.Axes.Add(
            new LinearAxis
            {
                Position = AxisPosition.Left,
                //Title = "f(x)",
                Key = "yAxis",
                AxisTitleDistance = 20,
                TitleFontSize = 14,
                FontSize = 12,
                AxislineThickness = lineThickness,
                MajorGridlineThickness = lineThickness,
                MinorGridlineThickness = lineThickness,
            });

            model.IsLegendVisible = false;

            model.Axes[0].Title = "Dimension 5";
            model.Axes[0].Maximum = 1.0;
            model.Axes[0].Minimum = 0.0;

            model.Axes[1].Title = "Dimension 6";
            model.Axes[1].Maximum = 1.0;
            model.Axes[1].Minimum = 0.0;

            var sobol = SobolFactory.Build(10, "Sobol/sobol-direction-numbers.dat");

            var data1 = sobol.TakeNext(10);
            var data2 = sobol.TakeNext(90);
            var data3 = sobol.TakeNext(156);

            var series1 = new ScatterSeries
            {
                MarkerType = MarkerType.Plus,
                MarkerSize = 5,
                MarkerStroke = OxyColors.Red
            };

            series1.Points.AddRange(data1.Select(p => new ScatterPoint(p[4], p[5])));

            var series2 = new ScatterSeries
            {
                MarkerType = MarkerType.Cross,
                MarkerSize = 3,
                MarkerStroke = OxyColors.Green
            };

            series2.Points.AddRange(data2.Select(p => new ScatterPoint(p[4], p[5])));

            var series3 = new ScatterSeries
            {
                MarkerType = MarkerType.Cross,
                MarkerSize = 3,
                MarkerStroke = OxyColors.Blue
            };

            series3.Points.AddRange(data3.Select(p => new ScatterPoint(p[4], p[5])));
            
            model.Series.Add(series1);
            model.Series.Add(series2);
            model.Series.Add(series3);

            void WriteToPDFCustom(PlotModel m, string fileName, string td = TargetDir)
            {
                using (var stream = File.Create(td + fileName + ".pdf"))
                {
                    var exporter = new OxyPlot.Pdf.PdfExporter { Width = 500, Height = 500 };
                    exporter.Export(m, stream);
                }
            }

            WriteToPDFCustom(model, filename);
        }

        [TestMethod]
        public void PlotPseudoRandomExample()
        {
            var filename = "pseudorandom";

            var lineThickness = 0.75;
            var model = new PlotModel
            {
                Title = "Pseudorandom Draws",
                LegendPosition = LegendPosition.TopLeft,
                LegendLineSpacing = 10,
                LegendFontSize = 12,
                PlotAreaBorderThickness = new OxyThickness(lineThickness),
            };
            model.Axes.Add(
            new LinearAxis
            {
                Position = AxisPosition.Bottom,
                //Title = "x",
                Key = "xAxis",
                AxisTitleDistance = 16,
                TitleFontSize = 14,
                FontSize = 12,
                AxislineThickness = lineThickness,
                MajorGridlineThickness = lineThickness,
                MinorGridlineThickness = lineThickness,
            });

            model.Axes.Add(
            new LinearAxis
            {
                Position = AxisPosition.Left,
                //Title = "f(x)",
                Key = "yAxis",
                AxisTitleDistance = 20,
                TitleFontSize = 14,
                FontSize = 12,
                AxislineThickness = lineThickness,
                MajorGridlineThickness = lineThickness,
                MinorGridlineThickness = lineThickness,
            });

            model.IsLegendVisible = false;

            model.Axes[0].Title = "Dimension 1";
            model.Axes[0].Maximum = 1.0;
            model.Axes[0].Minimum = 0.0;

            model.Axes[1].Title = "Dimension 2";
            model.Axes[1].Maximum = 1.0;
            model.Axes[1].Minimum = 0.0;

            IEnumerable<double[]> GetPseudoRandomEnumerable(int dimension)
            {
                var random = new Random();

                while (true)
                {
                    var arr = new double[dimension];
                    for (var i = 0; i < arr.Length; i++) arr[i] = random.NextDouble();
                    yield return arr;
                }
            }

            var pseudoRandomEnumerable = GetPseudoRandomEnumerable(2);

            var data1 = pseudoRandomEnumerable.Take(10).ToArray();
            var data2 = pseudoRandomEnumerable.Take(90).ToArray();
            var data3 = pseudoRandomEnumerable.Take(156).ToArray();

            var series1 = new ScatterSeries
            {
                MarkerType = MarkerType.Plus,
                MarkerSize = 5,
                MarkerStroke = OxyColors.Red
            };

            series1.Points.AddRange(data1.Select(p => new ScatterPoint(p[0], p[1])));

            var series2 = new ScatterSeries
            {
                MarkerType = MarkerType.Cross,
                MarkerSize = 3,
                MarkerStroke = OxyColors.Green
            };

            series2.Points.AddRange(data2.Select(p => new ScatterPoint(p[0], p[1])));

            var series3 = new ScatterSeries
            {
                MarkerType = MarkerType.Cross,
                MarkerSize = 3,
                MarkerStroke = OxyColors.Blue
            };

            series3.Points.AddRange(data3.Select(p => new ScatterPoint(p[0], p[1])));

            model.Series.Add(series1);
            model.Series.Add(series2);
            model.Series.Add(series3);

            void WriteToPDFCustom(PlotModel m, string fileName, string td = TargetDir)
            {
                using (var stream = File.Create(td + fileName + ".pdf"))
                {
                    var exporter = new OxyPlot.Pdf.PdfExporter { Width = 500, Height = 500 };
                    exporter.Export(m, stream);
                }
            }

            WriteToPDFCustom(model, filename);
        }

        [TestMethod]
        public void Plot3MExposure()
        {
            var env = LMMIPEnvironment;

            var capletQuotes = CapletBuilder
                .Build(LMMIPEnvironment, LMMIPCapVolCurve, LMMIPDiscountCurve)
                .Take(20)
                .ToArray();

            var TT = env.YearFractionsFromStartFor(capletQuotes.Select(c => c.ResetDate).ToArray());

            var volatilityModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            var correlationModel = new ABCDModelCorrelation();

            var kk = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, volatilityModel);

            var CC = MatrixBuilder.BuildCovarianceMatrices(
                TT, volatilityModel, correlationModel, kk);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var nDraws = TT.Length * (TT.Length + 1) / 2;

            var random = SobolFactory.Build((uint)nDraws, "Sobol/sobol-direction-numbers.dat");

            var terminalDate = capletQuotes.Last().MaturityDate;

            var resetsPlusFinalMaturity = capletQuotes.Select(x => x.ResetDate)
                                .ToArray().WithAppendedValue(terminalDate);

            var TTX = env.YearFractionsFromStartFor(resetsPlusFinalMaturity).WithPrependedValue(0);

            var f0 = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var P = CommonCalculations.BuildDiscountFactorVector(resetsPlusFinalMaturity, env.DiscountCurve);

            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var sr = CommonCalculations.SwapRate(TT, tau, P, f0);

            var lmm = new LiborMarketModel(CC, AA, random, tau, f0);

            var nSims = 1 << 16;            // 65,536 iterations

            var exposure = new InterestRateSwapMTMCalculator(tau, sr, Enumerable.Repeat(0.00, tau.Length).ToArray());

            var buckets = new Buckets(TTX.Length);

            for (var i = 0; i < nSims; i++)
            {
                lmm.Simulate();

                var mtm = exposure.Calculate_MTM_TimeSeries(lmm.F);

                buckets.Add(mtm);
            }

            var stats = buckets.Data.Select(b =>
            {
                var quantiles = new Quantiles(b);

                return new
                {
                    EPE = b.Select(d => Math.Max(d, 0)).Average(),
                    EE = b.Average(),
                    ENE = b.Select(d => Math.Min(d, 0)).Average(),
                    Q100 = quantiles.Get(1),
                    Q975 = quantiles.Get(0.975),
                    Q75 = quantiles.Get(0.75),
                    Q50 = quantiles.Get(0.5),
                    Q25 = quantiles.Get(0.25)
                };
            }).ToArray();



            Func<IEnumerable<double>, IEnumerable<double>, LineSeries> seriesF
            = (xx, yy) =>
            {
                var series = new LineSeries
                {
                    Background = OxyColor.FromRgb(255, 255, 255),
                    XAxisKey = "xAxis",
                    YAxisKey = "yAxis"
                };

                var dp = xx.Zip(yy, (x, y) => new DataPoint(x, y));

                series.Points.AddRange(dp);

                return series;
            };

            Func<IEnumerable<double>, LineSeries> timeseriesF
                = (yy) => seriesF(TTX, yy);

            // model.Series.Add(timeseriesF(stats.Select(b => b.EE)));

            var model1 = ReportPlots.GetDefaultPlotModel();

            model1.Series.Add(timeseriesF(stats.Select(b => b.EPE)));
            model1.Series.Add(timeseriesF(stats.Select(b => b.ENE)));

            ReportPlots.WriteToPDF(model1, "3m-exposure", TargetDir);


            var model2 = ReportPlots.GetDefaultPlotModel();

            model2.Series.Add(timeseriesF(stats.Select(b => b.Q25)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q50)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q75)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q975)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q100)));

            ReportPlots.WriteToPDF(model2, "3m-ee-distribution", TargetDir);
        }

        [TestMethod]
        public void Plot6MExposure()
        {
            var env = SixMonthLiborEnvironment;

            var volatilityModel = SixMonthLiborVolatilityModel;

            var correlationModel = new ABCDModelCorrelation();

            var kk = SixMonthLiborKiVector(Tenor.Get("66M")).WithoutFirstElement();

            var resetDates = env.ResetDatesBetween("6M", "54M");

            var TT = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate, resetDates,
                env.Parameters.Calendar, env.Parameters.DCC);

            var CC = MatrixBuilder.BuildCovarianceMatrices(TT, volatilityModel, correlationModel, kk);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var nDraws = TT.Length * (TT.Length + 1) / 2;

            var random = SobolFactory.Build((uint)nDraws, "Sobol/sobol-direction-numbers.dat");

            var terminalDate = env.AddTenorAdjusted(resetDates.Last(), env.Parameters.LiborTenor);

            var resetsPlusFinalMaturity = resetDates.ToArray().WithAppendedValue(terminalDate);

            var TTX = env.YearFractionsFromStartFor(resetsPlusFinalMaturity).WithPrependedValue(0);

            var f0 = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);

            var P = CommonCalculations.BuildDiscountFactorVector(resetsPlusFinalMaturity, env.DiscountCurve);

            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var sr = CommonCalculations.SwapRate(TT, tau, P, f0);

            var lmm = new LiborMarketModel(CC, AA, random, tau, f0);

            var nSims = 1 << 16;            // 65,536 iterations

            var exposure = new InterestRateSwapMTMCalculator(tau, sr, Enumerable.Repeat(0.00, tau.Length).ToArray());

            var buckets = new Buckets(TTX.Length);

            for (var i = 0; i < nSims; i++)
            {
                lmm.Simulate();

                var mtm = exposure.Calculate_MTM_TimeSeries(lmm.F);

                buckets.Add(mtm);
            }

            var stats = buckets.Data.Select(b =>
            {
                var quantiles = new Quantiles(b);

                return new
                {
                    EPE = b.Select(d => Math.Max(d, 0)).Average(),
                    EE = b.Average(),
                    ENE = b.Select(d => Math.Min(d, 0)).Average(),
                    Q100 = quantiles.Get(1),
                    Q975 = quantiles.Get(0.975),
                    Q75 = quantiles.Get(0.75),
                    Q50 = quantiles.Get(0.5),
                    Q25 = quantiles.Get(0.25)
                };
            }).ToArray();

            Func<IEnumerable<double>, IEnumerable<double>, LineSeries> seriesF
            = (xx, yy) =>
            {
                var series = new LineSeries
                {
                    Background = OxyColor.FromRgb(255, 255, 255),
                    XAxisKey = "xAxis",
                    YAxisKey = "yAxis"
                };

                var dp = xx.Zip(yy, (x, y) => new DataPoint(x, y));

                series.Points.AddRange(dp);

                return series;
            };

            Func<IEnumerable<double>, LineSeries> timeseriesF
                = (yy) => seriesF(TTX, yy);

            var model1 = ReportPlots.GetDefaultPlotModel();

            model1.Series.Add(timeseriesF(stats.Select(b => b.EPE)));
            model1.Series.Add(timeseriesF(stats.Select(b => b.ENE)));

            ReportPlots.WriteToPDF(model1, "6m-exposure", TargetDir);
            
            var model2 = ReportPlots.GetDefaultPlotModel();

            model2.Series.Add(timeseriesF(stats.Select(b => b.Q25)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q50)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q75)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q975)));
            model2.Series.Add(timeseriesF(stats.Select(b => b.Q100)));

            ReportPlots.WriteToPDF(model2, "6m-ee-distribution", TargetDir);

            var model3 = ReportPlots.GetDefaultPlotModel();

            model3.Series.Add(timeseriesF(stats.Select(b => b.Q25)));
            model3.Series.Add(timeseriesF(stats.Select(b => b.Q50)));
            model3.Series.Add(timeseriesF(stats.Select(b => b.Q75)));
            model3.Series.Add(timeseriesF(stats.Select(b => b.Q975)));

            ReportPlots.WriteToPDF(model3, "6m-ee-distribution-no-max", TargetDir);
        }

        [TestMethod]
        public void PlotJuly10Rates()
        {
            var model1 = GetDefaultPlotModel();

            model1.Axes[0].Minimum = 0.0;
            model1.Axes[1].Minimum = 0.0;

            var model2 = GetDefaultPlotModel();

            model2.Axes[0].Minimum = 0.0;
            model2.Axes[1].Minimum = 0.0;

            var TT = July10Environment.YearFractionsFromStartFor(July10ResetDates);

            var oisPoints = TT.Take(TT.Length - 1).Zip(
                TestEnvironments.July10OISRates, (t, r) =>
                      new DataPoint(t, r));

            var liborPoints = TT.Take(TT.Length-1).Zip(
                TestEnvironments.July10LiborRates, (t, r) =>
                    new DataPoint(t, r));

            var spreads = TestEnvironments.July10LiborRates
                                .Zip(TestEnvironments.July10OISRates,
                                (libor, ois) => libor - ois);

            var spreadPoints = TT.Take(TT.Length - 1).Zip(
                spreads, (t, r) =>
                    new DataPoint(t, r * 10_000));

            var liborRateSeries = new StairStepSeries
            {
                Title = "GBP LIBOR 6M",
                StrokeThickness = 1,
                Color = OxyColors.Red
            };

            var oisRateSeries = new StairStepSeries
            {
                Title = "GBP OIS",
                StrokeThickness = 1,
                Color = OxyColors.Blue
            };

            var spreadSeries = new StairStepSeries
            {
                Title = "LIBOR 6M - OIS Spread",
                StrokeThickness = 1,
                Color = OxyColors.Red
            };

            liborRateSeries.Points.AddRange(liborPoints);
            oisRateSeries.Points.AddRange(oisPoints);

            spreadSeries.Points.AddRange(spreadPoints);

            model1.Axes[0].Title = "Time (Years)";
            model1.Axes[1].Title = "Rate";

            model2.Axes[0].Title = "Time (Years)";
            model2.Axes[1].Title = "Spread (bps)";

            model1.Series.Add(liborRateSeries);
            model1.Series.Add(oisRateSeries);

            model1.IsLegendVisible = true;
            model1.LegendPosition = LegendPosition.RightBottom;

            model2.IsLegendVisible = true;
            model2.LegendPosition = LegendPosition.RightBottom;

            model2.Series.Add(spreadSeries);

            void WriteToPDFCustom(PlotModel m, string f, string td = TargetDir)
            {
                using (var stream = File.Create(td + f + ".pdf"))
                {
                    var exporter = new OxyPlot.Pdf.PdfExporter { Width = 600, Height = 350 };
                    exporter.Export(m, stream);
                }
            }

            WriteToPDFCustom(model1, "July10LiborOis");

            WriteToPDFCustom(model2, "July10LiborOisSpread");
        }

        [TestMethod]
        public void PlotJuly10CapAndCapletVolCurves()
        {
            var fileName = "July10CapAndCapletVol";

            #region Financial Calcs

            var env = July10Environment;
            
            var iCap = new LinearInterpolator(
                July10LIBOR6MCapVolDataByYearFraction.OrderBy(x => x.Key)
                .Take(13).ToDictionary(x => x.Key, x => x.Value));

            Func<double, double> fCap = x => iCap.Get(x);

            var capletQuotes = CapletBuilder.Build(env,
                July10CapVolCurve, July10_LIBOR_6M_PSEUDO_DF_Curve).Take(39).ToArray();

            var capletVols = capletQuotes.ToDictionary(x =>
                    env.Parameters.Calendar.YearFraction(env.Parameters.PricingDate, x.MaturityDate, env.Parameters.DCC),
                x => x.Volatility);

            var abcdModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            var ki = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, abcdModel);

            var cal = env.Parameters.Calendar;
            var dcc = env.Parameters.DCC;
            var baseDate = env.Parameters.PricingDate;

            var T = capletQuotes.Select(caplet => cal.YearFraction(baseDate, caplet.ResetDate, dcc)).ToArray();

            var modelImpliedVol = new double[capletQuotes.Length];

            for (var i = 0; i < capletQuotes.Length; i++)
            {
                var modelVol = abcdModel.Integral_Sigma_i_Sigma_j_Definite(0, T[i], T[i], T[i]);

                modelImpliedVol[i] = Math.Sqrt(modelVol / T[i]);
            }

            var capletReconstructedData = T.Zip(modelImpliedVol, (t, v) => new KeyValuePair<double, double>(t, v))
                                            .ToDictionary(x => x.Key, x => x.Value);

            var iCapletReconstructed = new MonotoneCubicInterpolator(capletReconstructedData);

            Func<double, double> fCapletReconstructed = x => iCapletReconstructed.Get(x);


            var iCaplet = new LinearInterpolator(capletVols);

            Func<double, double> fCaplet = x => iCaplet.Get(x);

            #endregion

            var model = GetDefaultPlotModel();

            //model.Title = "Linear and Monotone Cubic Spline Interpolation";
            model.IsLegendVisible = true;
            model.LegendPosition = LegendPosition.RightBottom;


            model.Axes[0].Minimum = 0.0;

            model.Axes[0].Title = "Time To Reset Date (Years)";
            model.Axes[1].Title = "Volatility";

            // model.Axes[1].Minimum = 0.0;

            var capSeries = new FunctionSeries(fCap, iCap.LowerBound + 0.01, iCap.UpperBound - 0.01, n: 1000)
            {
                Title = "Cap Vols",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Solid
            };
            
            var capletSeries = new FunctionSeries(fCaplet, iCaplet.LowerBound + 0.01, iCaplet.UpperBound - 0.01, n: 1000)
            {
                Title = "Caplet Vols (Stripped)",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Blue,
                StrokeThickness = 1,
                LineStyle = LineStyle.Dot
            };

            var capletReconstructedSeries = new FunctionSeries(
                fCapletReconstructed, 
                iCapletReconstructed.LowerBound + 0.01, iCapletReconstructed.UpperBound - 0.01, n: 1000)
            {
                Title = "Caplet Vols (Reconstructed)",
                Background = OxyColor.FromRgb(255, 255, 255),
                Color = OxyColors.Red,
                StrokeThickness = 1,
                LineStyle = LineStyle.Dot
            };

            model.Series.Add(capSeries);
            model.Series.Add(capletSeries);
            model.Series.Add(capletReconstructedSeries);

            void WriteToPDFCustom(PlotModel m, string f, string td = TargetDir)
            {
                using (var stream = File.Create(td + fileName + ".pdf"))
                {
                    var exporter = new OxyPlot.Pdf.PdfExporter { Width = 600, Height = 350 };
                    exporter.Export(model, stream);
                }
            }

            WriteToPDFCustom(model, fileName);
        }

        [TestMethod]
        public void PlotJuly10Exposure()
        {
            var env = July10Environment;

            var capletQuotes = CapletBuilder.Build(env,
                July10CapVolCurve, July10_LIBOR_6M_PSEUDO_DF_Curve).Take(39).ToArray();

            var volatilityModel = VolatilityFitting.FitVolatilityModel(env, capletQuotes);

            var kk = VolatilityFitting.GetCorrectionFactors(env, capletQuotes, volatilityModel).Take(10).ToArray();

            var correlationModel = new ABCDModelCorrelation();

            var resetDates = env.ResetDatesBetween("6M", "54M");

            var TT = CommonCalculations.BuildYearFractionVector(env.Parameters.PricingDate, resetDates,
                env.Parameters.Calendar, env.Parameters.DCC);

            var CC = MatrixBuilder.BuildCovarianceMatrices(TT, volatilityModel, correlationModel, kk);

            var AA = MatrixBuilder.BuildCorrelationRootMatrices(TT, correlationModel);

            var nDraws = TT.Length * (TT.Length + 1) / 2;

            var random = SobolFactory.Build((uint)nDraws, "Sobol/sobol-direction-numbers.dat");

            var terminalDate = env.AddTenorAdjusted(resetDates.Last(), env.Parameters.LiborTenor);

            var resetsPlusFinalMaturity = resetDates.ToArray().WithAppendedValue(terminalDate);

            var TTX = env.YearFractionsFromStartFor(resetsPlusFinalMaturity).WithPrependedValue(0);

            var f0 = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, TestEnvironments.July10_LIBOR_6M_PSEUDO_DF_Curve, env.Parameters.Calendar, env.Parameters.DCC);

            var oisRates = CommonCalculations.BuildForwardRateVector(
                resetsPlusFinalMaturity, env.DiscountCurve, env.Parameters.Calendar, env.Parameters.DCC);
            
            var liborOisSpreads = f0.Zip(oisRates, (libor, ois) => libor - ois).ToArray();

            var P = CommonCalculations.BuildDiscountFactorVector(resetsPlusFinalMaturity, env.DiscountCurve);

            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var sr = CommonCalculations.SwapRate(TT, tau, P, f0);

            var lmm = new LiborMarketModel(CC, AA, random, tau, f0);

            var nSims = 1 << 16;            // 65,536 iterations

            var exposure = new InterestRateSwapMTMCalculator(tau, sr, liborOisSpreads);

            var buckets = new Buckets(TTX.Length);

            for (var i = 0; i < nSims; i++)
            {
                lmm.Simulate();

                var mtm = exposure.Calculate_MTM_TimeSeries(lmm.F);

                buckets.Add(mtm);
            }

            var mtm_stats = buckets.Data.Select(b =>
            {
                var quantiles = new Quantiles(b);

                return new
                {
                    EPE = b.Select(d => Math.Max(d, 0)).Average(),
                    ENE = b.Select(d => Math.Min(d, 0)).Average(),
                    Q100 = b.Max(),
                    Q975 = quantiles.Get(0.975),
                    Q75 = quantiles.Get(0.75),
                    Q50 = quantiles.Get(0.5),
                    Q25 = quantiles.Get(0.25),
                    Q025 = quantiles.Get(0.025),
                    Q0 = b.Min()
                };
            }).ToArray();

            var exposure_stats = buckets.Data.Select(b =>
            {
                var ee_quantiles = new Quantiles(b.Select(d => Math.Max(d, 0)));

                return new
                {
                    Q100 = ee_quantiles.Get(1),
                    Q975 = ee_quantiles.Get(0.975),
                    Q75 = ee_quantiles.Get(0.75),
                    Q50 = ee_quantiles.Get(0.5),
                    Q25 = ee_quantiles.Get(0.25),
                    Q025 = ee_quantiles.Get(0.025),
                };
            }).ToArray();


            Func<IEnumerable<double>, IEnumerable<double>, LineSeries> seriesF
            = (xx, yy) =>
            {
                var series = new LineSeries
                {
                    Background = OxyColor.FromRgb(255, 255, 255),
                    XAxisKey = "xAxis",
                    YAxisKey = "yAxis"
                };

                var dp = xx.Zip(yy, (x, y) => new DataPoint(x, y));

                series.Points.AddRange(dp);

                return series;
            };

            Func<IEnumerable<double>, LineSeries> timeseriesF
                = (yy) => seriesF(TTX, yy);

            var model1 = ReportPlots.GetDefaultPlotModel();

            var epe = timeseriesF(mtm_stats.Select(b => b.EPE));
            var ene = timeseriesF(mtm_stats.Select(b => b.ENE));

            epe.StrokeThickness = 1;
            epe.Color = OxyColors.Blue;
            epe.Title = "EE";

            ene.StrokeThickness = 1;
            ene.Color = OxyColors.Blue;
            ene.LineStyle = LineStyle.Dot;
            ene.Title = "ENE";

            model1.Annotations.Add(
                new LineAnnotation
                {
                    Type = LineAnnotationType.Horizontal,
                    Y = 0,
                    LineStyle = LineStyle.Dash,
                    Color = OxyColors.DarkGray
                });

            model1.Series.Add(epe);
            model1.Series.Add(ene);

            model1.Axes[1].Maximum = 0.016;
            model1.Axes[1].Minimum = -0.016;

            model1.Axes[0].Title = "Time (Years)";
            model1.Axes[1].Title = "Exposure";

            model1.IsLegendVisible = true;
            model1.LegendPosition = LegendPosition.RightTop;

            ReportPlots.WriteToPDF(model1, "july10-exposure", TargetDir);

            var model2 = ReportPlots.GetDefaultPlotModel();

            var mtm_q025 = mtm_stats.Select(b => b.Q025).ToArray();
            var mtm_q25 = mtm_stats.Select(b => b.Q25).ToArray();
            var mtm_q50 = mtm_stats.Select(b => b.Q50).ToArray();
            var mtm_q75 = mtm_stats.Select(b => b.Q75).ToArray();
            var mtm_q975 = mtm_stats.Select(b => b.Q975).ToArray();

            var mtmBoxPlotSeries = new BoxPlotSeries
            {
                Title = "MTM Distribution",
                StrokeThickness = 1,
                MeanThickness = 2,
                Fill = OxyColors.LightGray
            };

            for (int i = 0; i < TTX.Length; i++)
            {
                mtmBoxPlotSeries.Items.Add(
                    new BoxPlotItem(
                        TTX[i],
                        mtm_q025[i],
                        mtm_q25[i],
                        mtm_q50[i],
                        mtm_q75[i],
                        mtm_q975[i]));
                
            }

            model2.Axes[0].Title = "Time (Years)";
            model2.Axes[1].Title = "MTM";

            model2.IsLegendVisible = true;
            model2.LegendPosition = LegendPosition.RightTop;

            model2.Series.Add(mtmBoxPlotSeries);
            
            ReportPlots.WriteToPDF(model2, "july10-mtm-distribution", TargetDir);

            var model3 = ReportPlots.GetDefaultPlotModel();

            var exposure_q025 = exposure_stats.Select(b => b.Q025).ToArray();
            var exposure_q25 = exposure_stats.Select(b => b.Q25).ToArray();
            var exposure_q50 = exposure_stats.Select(b => b.Q50).ToArray();
            var exposure_q75 = exposure_stats.Select(b => b.Q75).ToArray();
            var exposure_q975 = exposure_stats.Select(b => b.Q975).ToArray();

            var exposureBoxPlotSeries = new BoxPlotSeries
            {
                Title = "Exposure Distribution",
                StrokeThickness = 1,
                MeanThickness = 2,
                Fill = OxyColors.LightGray
            };

            for (int i = 0; i < TTX.Length; i++)
            {
                exposureBoxPlotSeries.Items.Add(
                    new BoxPlotItem(
                        TTX[i],
                        exposure_q025[i],
                        exposure_q25[i],
                        exposure_q50[i],
                        exposure_q75[i],
                        exposure_q975[i]));

            }

            model3.Axes[0].Title = "Time (Years)";
            model3.Axes[1].Title = "Exposure";

            model3.IsLegendVisible = true;
            model3.LegendPosition = LegendPosition.RightTop;

            model3.Series.Add(exposureBoxPlotSeries);

            ReportPlots.WriteToPDF(model3, "july10-exposure-distribution", TargetDir);
        }

        [TestMethod]
        public void PlotJuly10CDSQuotesAndSurvivalProbabilities()
        {
            var env = July10Environment;
            
            var resetsPlusFinalMaturity = env.ResetDatesBetween("0M", "10Y");
            
            var tau = CommonCalculations.BuildTauVector(
                resetsPlusFinalMaturity, env.Parameters.Calendar, env.Parameters.DCC);

            var rr = 0.4;

            var survivalCurve = CreditCalculations.GetSurvivalProbabilityCurve(
                rr, July10_LLOY_CDS_MID_Curve, July10_OIS_DF_Curve, resetsPlusFinalMaturity, tau);

            var bidPoints = resetsPlusFinalMaturity.Select(date =>
            {
                var yf = env.Parameters.Calendar.YearFraction(
                                env.Parameters.PricingDate,
                                date,
                                env.Parameters.DCC);
                var spread = 1E4 * July10_LLOY_CDS_BID_Curve.Get(date);
                return new DataPoint(yf, spread);   
            });

            var midPoints = resetsPlusFinalMaturity.Select(date =>
            {
                var yf = env.Parameters.Calendar.YearFraction(
                                env.Parameters.PricingDate,
                                date,
                                env.Parameters.DCC);
                var spread = 1E4 * July10_LLOY_CDS_MID_Curve.Get(date);
                return new DataPoint(yf, spread);
            });

            var askPoints = resetsPlusFinalMaturity.Select(date =>
            {
                var yf = env.Parameters.Calendar.YearFraction(
                                env.Parameters.PricingDate,
                                date,
                                env.Parameters.DCC);
                var spread = 1E4 * July10_LLOY_CDS_ASK_Curve.Get(date);
                return new DataPoint(yf, spread);
            });
            
            var bidSeries = new LineSeries
            {
                Title = "Bid",
                StrokeThickness = 1,
                Color = OxyColors.Green,
                LineStyle = LineStyle.Dash
            };

            bidSeries.Points.AddRange(bidPoints);

            var midSeries = new LineSeries
            {
                Title = "Mid",
                StrokeThickness = 1,
                Color = OxyColors.Blue,
                LineStyle = LineStyle.Dot
            };

            midSeries.Points.AddRange(midPoints);

            var askSeries = new LineSeries
            {
                Title = "Ask",
                StrokeThickness = 1,
                Color = OxyColors.Brown,
                LineStyle = LineStyle.Dash
            };

            askSeries.Points.AddRange(askPoints);

            var model1 = ReportPlots.GetDefaultPlotModel();

            model1.Axes[0].Minimum = 0.0;
            model1.Axes[1].Minimum = 0.0;

            model1.Series.Add(askSeries);
            model1.Series.Add(midSeries);
            model1.Series.Add(bidSeries);
            
            model1.Axes[0].Title = "Time (Years)";
            model1.Axes[1].Title = "CDS Spread (bps)";

            model1.IsLegendVisible = true;
            model1.LegendPosition = LegendPosition.RightBottom;

            ReportPlots.WriteToPDF(model1, "cds-spreads", TargetDir);

            var model2 = ReportPlots.GetDefaultPlotModel();

            var survivalPoints = resetsPlusFinalMaturity.Select(date =>
            {
                var yf = env.Parameters.Calendar.YearFraction(
                                env.Parameters.PricingDate,
                                date,
                                env.Parameters.DCC);
                var survivalProbability = survivalCurve.Get(date);
                return new DataPoint(yf, survivalProbability);
            });

            var survivalProbabilitySeries = new LineSeries
            {
                StrokeThickness = 1,
                Color = OxyColors.Red,
                LineStyle = LineStyle.Dot
            };

            survivalProbabilitySeries.Points.AddRange(survivalPoints);

            model2.Axes[1].Minimum = 0.8;

            model2.Axes[0].Title = "Time (Years)";
            model2.Axes[1].Title = "Survival Probability";
            
            model2.Series.Add(survivalProbabilitySeries);

            ReportPlots.WriteToPDF(model2, "survival-probabilities", TargetDir);
        }
    }
}