﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiborOisVol.Core;
using LiborOisVol.Core.Sobol;

namespace LiborOisVol.Test
{
    [TestClass]
    public class BasketOptionTests
    {
        [TestMethod]
        public void TestBasketOptionPricer()
        {
            var w = new[] { 0.2, 0.3, 0.3, 0.2 };
            var r = new[] { 0.01, 0.03, 0.02, 0.01 };
            var d = new[] { 0.04, 0.05, 0.03, 0.07 };
            var volS = new[] { 0.14, 0.20, 0.22, 0.18 };
            var volX = new[] { 0.20, 0.20, 0.22, 0.19 };

            var corrS = new[,]
                    {   { 1.0, 0.21, 0.87, -0.02 },
                        { 0.21, 1.0, -0.4, 0.77 },
                        { 0.87, -0.4, 1.0, 0.3 },
                        { -0.02, 0.77, 0.3, 1.0 } };

            var corrX = new[,]
            {   { 1.0, 0.21, 0.87, -0.02 },
                { 0.21, 1.0, -0.4, 0.77 },
                { 0.87, -0.4, 1.0, 0.3 },
                { -0.02, 0.77, 0.3, 1.0 } };

            var rho = new[] { 0.11, 0.2, -0.3, 0.1 };

            var tau = 0.8;

            var K = 1.2;

            var random = SobolFactory.Build(4, "Sobol/sobol-direction-numbers.dat");

            var option = new EuropeanQuantoBasketPerformanceCall(
                w, r, d, volX, volS, corrS, rho, tau, K, random);

            var tracker = new MeanVarianceTracker();

            var nSims = 1 << 17;

            for (var i = 0; i < nSims; i++)
            {
                var payoff = option.GetNextSimulationPayoff();

                tracker.Add(payoff);
            }

            var price = tracker.Mean;
        }

        public class EuropeanQuantoBasketPerformanceCall
        {
            private readonly double[] w;
            private readonly double[] r;
            private readonly double[] d;
            //private readonly double[] s0;
            private readonly double[] volS;
            private readonly double[] volX;
            private readonly double[,] corrS;
            private readonly double[] rho;
            private readonly double tau;
            private readonly double sqrtTau;
            private readonly IUniformRandom random;
            private readonly double K;

            private readonly double[,] A;

            private readonly double[] uncorrelatedDraws;
            private readonly double[] correlatedDraws;

            private readonly double[] currentDraw;

            private double[] wSExpected;

            private readonly int N;

            public EuropeanQuantoBasketPerformanceCall(
                double[] w,
                double[] r,
                double[] d,
                //double[] s0,
                double[] volX,
                double[] volS,
                double[,] corrS,
                double[] rho,
                double tau,
                double K,
                IUniformRandom random)
            {
                N = w.Length;

                this.w = w;
                this.r = w;
                this.d = d;
                //this.s0 = s0;
                this.volS = volS;
                this.volX = volX;
                this.corrS = corrS;
                this.rho = rho;
                this.tau = tau;
                this.K = K;
                this.random = random;

                sqrtTau = Math.Sqrt(tau);

                A = MatrixHelper.CholeskyDecompose(corrS);

                currentDraw = random.CurrentDraw;

                uncorrelatedDraws = new double[N];
                correlatedDraws = new double[N];

                BuildWSExpected();
            }

            private void BuildWSExpected()
            {
                wSExpected = new double[N];

                for (var i = 0; i < N; i++)
                {
                    var driftTerm = r[i]
                                        - d[i]
                                        - rho[i] * volS[i] * volX[i]
                                        - 0.5 * volS[i] * volS[i];

                    wSExpected[i]
                        = w[i]
                            // * s0[i] 
                            * Math.Exp(driftTerm * tau);
                }
            }

            public double GetNextSimulationPayoff()
            {
                random.DrawNext();

                for (var i = 0; i < N; i++)
                {
                    uncorrelatedDraws[i] = NormalDistribution.InverseCDF(currentDraw[i]);
                }

                MatrixHelper.HalfMultiply(A, uncorrelatedDraws, correlatedDraws);

                var basketAcc = 0.0;

                for (var i = 0; i < N; i++)
                {
                    basketAcc += wSExpected[i]
                                    * Math.Exp(volS[i] * correlatedDraws[i] * sqrtTau);
                }

                return Math.Max(basketAcc - K, 0);
            }
        }
    }
}