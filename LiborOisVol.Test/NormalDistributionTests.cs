﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using static LiborOisVol.Core.NormalDistribution;

namespace LiborOisVol.Test
{
    [TestClass]
    public class NormalDistributionTests
    {
        private const double Tolerance = 1E-8;

        [TestMethod]
        public void TestStandardNormal()
        {
            Assert.AreEqual(CDF(-2),      0.022750132,    Tolerance);
            Assert.AreEqual(CDF(-1.8),    0.035930319,    Tolerance);
            Assert.AreEqual(CDF(-1.6),    0.054799292,    Tolerance);
            Assert.AreEqual(CDF(-1.4),    0.080756659,    Tolerance);
            Assert.AreEqual(CDF(-1.2),    0.11506967,     Tolerance);
            Assert.AreEqual(CDF(-1),      0.158655254,    Tolerance);
            Assert.AreEqual(CDF(-0.8),    0.211855399,    Tolerance);
            Assert.AreEqual(CDF(-0.6),    0.274253118,    Tolerance);
            Assert.AreEqual(CDF(-0.4),    0.344578258,    Tolerance);
            Assert.AreEqual(CDF(-0.2),    0.420740291,    Tolerance);
            Assert.AreEqual(CDF(0),       0.5,            Tolerance);
            Assert.AreEqual(CDF(0.2),     0.579259709,    Tolerance);
            Assert.AreEqual(CDF(0.4),     0.655421742,    Tolerance);
            Assert.AreEqual(CDF(0.6),     0.725746882,    Tolerance);
            Assert.AreEqual(CDF(0.8),     0.788144601,    Tolerance);
            Assert.AreEqual(CDF(1),       0.841344746,    Tolerance);
            Assert.AreEqual(CDF(1.2),     0.88493033,     Tolerance);
            Assert.AreEqual(CDF(1.4),     0.919243341,    Tolerance);
            Assert.AreEqual(CDF(1.6),     0.945200708,    Tolerance);
            Assert.AreEqual(CDF(1.8),     0.964069681,    Tolerance);
            Assert.AreEqual(CDF(2),       0.977249868,    Tolerance);
        }

        [TestMethod]
        public void TestStandardInverseNormal()
        {
            Assert.AreEqual(InverseCDF(0.001),    -3.090232306,   Tolerance);
            Assert.AreEqual(InverseCDF(0.05),     -1.644853627,   Tolerance);
            Assert.AreEqual(InverseCDF(0.1),      -1.281551566,   Tolerance);
            Assert.AreEqual(InverseCDF(0.15),     -1.036433389,   Tolerance);
            Assert.AreEqual(InverseCDF(0.2),      -0.841621234,   Tolerance);
            Assert.AreEqual(InverseCDF(0.25),     -0.67448975,    Tolerance);
            Assert.AreEqual(InverseCDF(0.3),      -0.524400513,   Tolerance);
            Assert.AreEqual(InverseCDF(0.35),     -0.385320466,   Tolerance);
            Assert.AreEqual(InverseCDF(0.4),      -0.253347103,   Tolerance);
            Assert.AreEqual(InverseCDF(0.45),     -0.125661347,   Tolerance);
            Assert.AreEqual(InverseCDF(0.5),      0,              Tolerance);
            Assert.AreEqual(InverseCDF(0.55),     0.125661347,    Tolerance);
            Assert.AreEqual(InverseCDF(0.6),      0.253347103,    Tolerance);
            Assert.AreEqual(InverseCDF(0.65),     0.385320466,    Tolerance);
            Assert.AreEqual(InverseCDF(0.7),      0.524400513,    Tolerance);
            Assert.AreEqual(InverseCDF(0.75),     0.67448975,     Tolerance);
            Assert.AreEqual(InverseCDF(0.8),      0.841621234,    Tolerance);
            Assert.AreEqual(InverseCDF(0.85),     1.036433389,    Tolerance);
            Assert.AreEqual(InverseCDF(0.9),      1.281551566,    Tolerance);
            Assert.AreEqual(InverseCDF(0.95),     1.644853627,    Tolerance);
            Assert.AreEqual(InverseCDF(0.999),    3.090232306,    Tolerance);
        }
    }
}